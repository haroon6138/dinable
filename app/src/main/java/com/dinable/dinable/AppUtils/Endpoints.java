package com.dinable.dinable.AppUtils;

public class Endpoints
{
    //url's for data
//    public static String ip_server = "https://www.voltasinc.us/dinable/app_api/index.php";
//    public static String ip_server = "https://35.198.228.121/dinable/app_api/index.php";
    public static String ip_server = "http://dinable.com/app_api/index.php";

    //FCM URL
    public static String FCM_PUSH_URL = "https://fcm.googleapis.com/fcm/send";

    //OneSignal URL
    public static String OneSignal_PUSH_URL = "https://onesignal.com/api/v1/notifications";
}