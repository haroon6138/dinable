package com.dinable.dinable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Activity.chatActivity;
import com.dinable.dinable.Activity.friendsActivity;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.friendsModel;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class myFriendsListAdapter extends RecyclerView.Adapter<myFriendsListAdapter.myFriendsViewHolder>{

    private Context context;
    private List<friendsModel> friendsModelList;

    public class myFriendsViewHolder extends RecyclerView.ViewHolder {
        private TextView friendName, friendEmail, friendStatus;
        private CircularImageView friendImage;
        LinearLayout friendsMessageLayout, friendsRequestLayout;
        private ImageView btnMessage, btnAcceptRequest, btnRejectRequest;

        public myFriendsViewHolder(View view) {
            super(view);
            friendName = (TextView) view.findViewById(R.id.friendName);
            friendEmail = (TextView) view.findViewById(R.id.friendEmail);
            friendStatus = (TextView) view.findViewById(R.id.friendStatus);
            friendImage = (CircularImageView) view.findViewById(R.id.avatar);
            btnMessage = (ImageView) view.findViewById(R.id.btn_message);
            btnAcceptRequest = (ImageView) view.findViewById(R.id.btn_accept);
            btnRejectRequest = (ImageView) view.findViewById(R.id.btn_reject);
            friendsMessageLayout = (LinearLayout) view.findViewById(R.id.friendMessageLayout);
            friendsRequestLayout = (LinearLayout) view.findViewById(R.id.friendRequestLayout);

            if(friendsActivity.currentOpenedFragment.equalsIgnoreCase("myFriendsFragment")) {
                friendsMessageLayout.setVisibility(View.VISIBLE);
                friendsRequestLayout.setVisibility(View.GONE);
            } else if(friendsActivity.currentOpenedFragment.equalsIgnoreCase("friendRequestFragment")) {
                friendsMessageLayout.setVisibility(View.GONE);
                friendsRequestLayout.setVisibility(View.VISIBLE);
                friendStatus.setVisibility(View.GONE);
            }
        }
    }

    public myFriendsListAdapter(Context context, List<friendsModel> friendsModelList){
        this.context = context;
        this.friendsModelList = friendsModelList;
    }

    @NonNull
    @Override
    public myFriendsListAdapter.myFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_myfriends_list, null);
        return new myFriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myFriendsViewHolder holder, int position) {
        final friendsModel friendsClass = friendsModelList.get(position);

        holder.friendName.setText(friendsClass.getFriendName());
        holder.friendEmail.setText(friendsClass.getFriendEmail());

        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, chatActivity.class);
                intent.putExtra("userToEmail", friendsClass.getFriendEmail());
                intent.putExtra("userFromEmail", Prefs.getString("user_email",""));
                intent.putExtra("userFromName", Prefs.getString("user_name",""));
                intent.putExtra("userToName", friendsClass.getFriendName());
                intent.putExtra("userToFCM", friendsClass.getFriendFCM());
                context.startActivity(intent);
            }
        });

        holder.btnAcceptRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFriendRequestResponse("1", friendsClass.getFriendId());
                Toasty.info(context, "Request Accepted Of: " + friendsClass.getFriendName() + " - " + friendsClass.getFriendId()).show();
            }
        });

        holder.btnRejectRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFriendRequestResponse("2", friendsClass.getFriendId());
                Toasty.info(context, "Request Rejected Of: " + friendsClass.getFriendName() + " - " + friendsClass.getFriendId()).show();
            }
        });

        if(friendsActivity.currentOpenedFragment.equalsIgnoreCase("myFriendsFragment"))  {
            if(friendsClass.getFriendStatus().equals("0")){
                holder.friendStatus.setVisibility(View.VISIBLE);
                holder.btnMessage.setVisibility(View.GONE);
            } else {
                holder.friendStatus.setVisibility(View.GONE);
                holder.btnMessage.setVisibility(View.VISIBLE);
            }
        }

        try {
            if(friendsClass.getFriendImage().equals("null"))
                Picasso.get().load(R.drawable.dinablelogo).into(holder.friendImage);
            else
                Picasso.get().load(friendsClass.getFriendImage()).into(holder.friendImage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if(friendsModelList != null)
            return friendsModelList.size();
        else
            return 0;
    }


    // Method to send friend request accept or reject response
    private void sendFriendRequestResponse(final String status, final String acceptId) {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Friends", response);
//                if (response.contains("null")) {
//                    Toasty.warning(getActivity(), "Friends Not Available", Toast.LENGTH_LONG).show();
//                } else {
            }
//            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("req_key", "Restaurants");

                params.put("req_key", "request_actions");
                params.put("user_id", Prefs.getString("user_id", ""));
                params.put("req_user_id", acceptId);
                params.put("action", status);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);

    }

}
