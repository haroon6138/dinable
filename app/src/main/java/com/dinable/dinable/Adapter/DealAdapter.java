package com.dinable.dinable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.RestaurantPage;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.DealClass;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class DealAdapter extends RecyclerView.Adapter<DealAdapter.DealViewHolder>
{
    private Context context;
    private List<DealClass> dealClassList;

    class DealViewHolder extends RecyclerView.ViewHolder {
        TextView _deal_title;
        TextView _deal_desc;
        TextView _deal_price;
        RatingBar _deal_rating;
        ImageView _deal_Image;
        View _for_click;

        public DealViewHolder(View itemView)
        {
            super(itemView);
            _deal_title = (TextView) itemView.findViewById(R.id.deal_menu_title);
            _deal_desc = (TextView) itemView.findViewById(R.id.deal_menu_desc);
            _deal_price = (TextView) itemView.findViewById(R.id.deal_price);
            _deal_rating = (RatingBar) itemView.findViewById(R.id.deal_rating);
            _deal_Image = (ImageView) itemView.findViewById(R.id.deal_image);
            _for_click = (View) itemView.findViewById(R.id.deal_card_view);
        }
    }

    public DealAdapter(Context context, List<DealClass> dealClassList) {
        this.context = context;
        this.dealClassList = dealClassList;
    }

    @Override
    public void onBindViewHolder(DealViewHolder holder , int position)
    {
        final DealClass dealClass = dealClassList.get(position);

        //loading the image
        Glide.with(context)
                .load(dealClass.getDeal_image())
                .into(holder._deal_Image);
        holder._deal_title.setText(dealClass.getDeal_title());
        holder._deal_desc.setText(dealClass.getDeal_desc());
        holder._deal_price.setText("Rs."+dealClass.getDeal_price());
        holder._deal_rating.setRating(dealClass.getDeal_rating());

        holder._for_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forcl=new Intent(context, RestaurantPage.class);
                forcl.putExtra("rest_name",dealClass.getRest_name());
                forcl.putExtra("rest_id",dealClass.getDeal_res_id());
                forcl.putExtra("rest_image",dealClass.getRest_image());
                forcl.putExtra("rest_self",dealClass.getRest_self());
                context.startActivity(forcl);
                Animation.split(context);
            }
        });
    }

    @Override
    public DealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.deals_card_style, null);
        return new DealViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return dealClassList.size();
    }
}
