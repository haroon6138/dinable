package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dinable.dinable.R;
import com.dinable.dinable.classes.DealClass;
import com.dinable.dinable.classes.notifications_history_model;

import java.util.List;

public class notificationsListAdapter extends RecyclerView.Adapter<notificationsListAdapter.notificationsViewHolder> {

    private Context context;
    private List<notifications_history_model> notificationsClassList;

    public class notificationsViewHolder extends RecyclerView.ViewHolder {
        private TextView notiTitle, notiDesc, notiDateTime;

        public notificationsViewHolder(View view) {
            super(view);
            notiTitle = (TextView) view.findViewById(R.id.notiTitle);
            notiDesc = (TextView) view.findViewById(R.id.notiDescription);
            notiDateTime = (TextView) view.findViewById(R.id.notiDateTime);
        }
    }

    public notificationsListAdapter(Context context, List<notifications_history_model> notificationsClassList){
        this.context = context;
        this.notificationsClassList = notificationsClassList;
    }


    @NonNull
    @Override
    public notificationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.notifications_list_layout, null);
        return new notificationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull notificationsViewHolder holder, int position) {

        final notifications_history_model notificationsClass = notificationsClassList.get(position);

        holder.notiTitle.setText(notificationsClass.getNotification_Title());
        holder.notiDesc.setText(notificationsClass.getNotification_Description());
        holder.notiDateTime.setText(notificationsClass.getNotification_DateTime());

    }

    @Override
    public int getItemCount() {
        return notificationsClassList.size();
    }

}
