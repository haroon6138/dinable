package com.dinable.dinable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.place_orders_list_showing;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Order_class;

import java.util.List;

public class for_orders_cards_adaptor extends RecyclerView.Adapter<for_orders_cards_adaptor.MyViewHolder> {
    private Context con;
    private List<Order_class> order_classList;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView rest_name, order_no, order_date, order_status, order_tot_amnt, orderType;
        private CardView cardView;
        private ImageView ivRestaurantImage;

        public MyViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            ivRestaurantImage = view.findViewById(R.id.rest_image);
            rest_name = view.findViewById(R.id.rest_name);
            order_no = view.findViewById(R.id.theme);
            order_status = view.findViewById(R.id.timeto);
            order_tot_amnt = view.findViewById(R.id.date);
            order_date = view.findViewById(R.id.people);
            orderType = view.findViewById(R.id.orderType);
        }
    }

    public for_orders_cards_adaptor(Context c, List<Order_class> order_classList) {
        this.con = c;
        this.order_classList = order_classList;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Order_class c = order_classList.get(position);

        Glide.with(con)
                .load(c.getRest_image())
                .into(holder.ivRestaurantImage);

        holder.rest_name.setText(c.getRestaurant_name());
        holder.order_no.setText(c.getO_number());
        holder.order_status.setText(c.getO_status());
        holder.order_tot_amnt.setText("Rs. " + c.getTotal_price() + "/=");
        holder.order_date.setText(c.getO_date_time());
        holder.orderType.setText(c.getOrderType());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, place_orders_list_showing.class);
                i.putExtra("o_id", c.getO_id());
                i.putExtra("rest_name", c.getRestaurant_name());
                i.putExtra("rest_id", c.getRest_id());
                i.putExtra("datetime", c.getO_date_time());
                i.putExtra("overprice", c.getTotal_price());
                i.putExtra("ordernum", c.getO_number());
                i.putExtra("selfcheck", c.getSelf_or_not());
                i.putExtra("rest_image", c.getRest_image());
                i.putExtra("invoice_status", c.getInvoice());
                i.putExtra("o_status", c.getO_status());  //Approve  //Pending
                i.putExtra("rest_PlayerId", c.getRest_PlayerId());
                i.putExtra("orderType", c.getOrderType());
                con.startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return order_classList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.for_orders_showing_cards, parent, false);
        return new MyViewHolder(v);
    }
}