package com.dinable.dinable.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.EventsDetailActivity;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.EventRestClass;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class EventRestAdapter extends RecyclerView.Adapter<EventRestAdapter.EventViewHolder>
{
    private Context context;
    private List<EventRestClass> eventRestClassList;

    public EventRestAdapter(Context context, List<EventRestClass> eventRestClassList) {
        this.context = context;
        this.eventRestClassList = eventRestClassList;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.event_card_style, null);
        return new EventViewHolder(view);
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        TextView _evt_title, _evt_desc, _evt_date, _evt_category;
        RatingBar _evt_rating;
        ImageView _evt_Image;
        View _for_click;

        public EventViewHolder(View itemView)
        {
            super(itemView);
            _evt_title = (TextView) itemView.findViewById(R.id.evt_title);
            _evt_desc = (TextView) itemView.findViewById(R.id.evt_desc);
            _evt_date = (TextView) itemView.findViewById(R.id.evt_date);
            _evt_category = (TextView) itemView.findViewById(R.id.evt_category);
            _evt_rating = (RatingBar) itemView.findViewById(R.id.evt_rating);
            _evt_Image = (ImageView) itemView.findViewById(R.id.evt_image);
            _for_click = (View) itemView.findViewById(R.id.evt_card_view);
        }
    }

    @Override
    public void onBindViewHolder(final EventViewHolder holder , int position)
    {
        final EventRestClass eventRestClass = eventRestClassList.get(position);

        //loading the image
        Glide.with(context)
                .load(eventRestClass.getEvt_image())
                .into(holder._evt_Image);
        holder._evt_title.setText(eventRestClass.getEvt_title());
        holder._evt_desc.setText(eventRestClass.getEvt_desc());
        holder._evt_rating.setRating(eventRestClass.getEvt_rating());
        holder._evt_date.setText(eventRestClass.getEvt_date());
        holder._evt_category.setText(eventRestClass.getEvt_cat_name());

        holder._for_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // start transition
                Intent intent = new Intent(context, EventsDetailActivity.class);
                intent.putExtra("evt_id",eventRestClass.getEvt_id());
                intent.putExtra("evt_image",eventRestClass.getEvt_image());
                intent.putExtra("evt_name",eventRestClass.getEvt_title());
                intent.putExtra("evt_rating",eventRestClass.getEvt_rating());
                intent.putExtra("evt_desc",eventRestClass.getEvt_desc());
                intent.putExtra("evt_date",eventRestClass.getEvt_date());
                intent.putExtra("evt_time_from",eventRestClass.getEvt_time_from());
                intent.putExtra("evt_time_to",eventRestClass.getEvt_time_to());
                intent.putExtra("evt_per_head",eventRestClass.getEvt_per_head_charges());
                intent.putExtra("evt_category", eventRestClass.getEvt_cat_name());
                context.startActivity(intent);
                Animation.slideUp(context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventRestClassList.size();
    }

    public void searchedList(List<EventRestClass> _list)
    {
        eventRestClassList = _list;
        notifyDataSetChanged();
    }
}
