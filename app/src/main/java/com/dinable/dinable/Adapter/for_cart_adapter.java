package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.For_add_to_card_class;
import com.dinable.dinable.classes.For_addtocart_database_table;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.dmoral.toasty.Toasty;

public class for_cart_adapter extends RecyclerView.Adapter<for_cart_adapter.MyViewHolder>
        {
private Context context;
private List<For_addtocart_database_table> for_add_to_card_classes;

/**
 * View holder class
 * */
public class MyViewHolder extends RecyclerView.ViewHolder {
      ImageView menu_image,add_quantity,less_quantity;
      TextView menu_title,menu_price,menu_counter;
      Integer menu_qyt_add_value = 1;
      Database_Helper database_helper;

    public MyViewHolder(View view) {
        super(view);
        menu_image=(ImageView) view.findViewById(R.id.image_menu);
        add_quantity=(ImageView) view.findViewById(R.id.add_menu_qty);
        less_quantity=(ImageView) view.findViewById(R.id.remove_menu_qty);
        menu_title=(TextView) view.findViewById(R.id.menu_title);
        menu_price=(TextView) view.findViewById(R.id.menu_price);
        menu_counter=(TextView) view.findViewById(R.id.menu_qty_counter);
        database_helper=new Database_Helper(context);
    }
}

    public for_cart_adapter(Context c,List<For_addtocart_database_table> for_add_to_card_classes) {
        this.context=c;
        this.for_add_to_card_classes=for_add_to_card_classes;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       final For_addtocart_database_table c=for_add_to_card_classes.get(position);
        Glide.with(context)
                .load(c.getItem_image())
                .into(holder.menu_image);
        holder.menu_title.setText(c.getItem_name());
        holder.menu_price.setText(String.valueOf(Integer.parseInt(c.getItem_price())*Integer.parseInt(c.getItem_count())));
        holder.menu_counter.setText(String.valueOf(c.getItem_count()));

        holder.menu_qyt_add_value=Integer.parseInt(c.getItem_count());

        //Set On Click Listner On When User Add Menu Qunatity
        holder.add_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.menu_qyt_add_value++;
                holder.menu_price.setText(String.valueOf(Integer.parseInt(c.getItem_price())*holder.menu_qyt_add_value));
                holder.menu_counter.setText(String.valueOf(holder.menu_qyt_add_value));
                holder.database_helper.updateNote(c.getTable_row_id(),holder.menu_qyt_add_value);
            }
        });

        //Set On Click Listner On When User Minus Menu Qunatity
        holder.less_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.menu_qyt_add_value <= 1)
                {
                    holder.database_helper.deleteNote(c.getTable_row_id());
                    for_add_to_card_classes.remove(position);
                    notifyDataSetChanged();
                }
                else
                {
                    holder.menu_qyt_add_value--;
                    holder.menu_price.setText(String.valueOf(Integer.parseInt(c.getItem_price())*holder.menu_qyt_add_value));
                    holder.menu_counter.setText(String.valueOf(holder.menu_qyt_add_value));
                    holder.database_helper.updateNote(c.getTable_row_id(),holder.menu_qyt_add_value);
                }
            }
        });


    }


    @Override
    public int getItemCount() { return for_add_to_card_classes.size(); }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.for_show_carts,parent, false);
        return new MyViewHolder(v);
    }
}