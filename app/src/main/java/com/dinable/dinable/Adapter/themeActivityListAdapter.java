package com.dinable.dinable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.RestaurantPage;
import com.dinable.dinable.Activity.themesRestaurantSearch;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.ThemeForSearchClass;

import java.util.List;

public class themeActivityListAdapter extends
        RecyclerView.Adapter<themeActivityListAdapter.MyViewHolder> {

    private List<ThemeForSearchClass> restaurantsclassList;
    private Context con;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView themeName;
        private ImageView ab;
        private View forclick;

        public MyViewHolder(View view) {
            super(view);
            themeName = (TextView) view.findViewById(R.id.theme_title);
            ab = (ImageView) view.findViewById(R.id.theme_image);
            forclick = (View) view.findViewById(R.id.theme_card_view);
        }
    }

    public themeActivityListAdapter(Context c, List<ThemeForSearchClass> restaurantsclassList) {
        this.restaurantsclassList = restaurantsclassList;
        this.con = c;
    }

    @NonNull
    @Override
    public themeActivityListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_theme_list, parent, false);
        return new themeActivityListAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull themeActivityListAdapter.MyViewHolder holder, int position) {

        final ThemeForSearchClass c = restaurantsclassList.get(position);

        Glide.with(con)
                .load(c.getTheme_image())
                .into(holder.ab);

        holder.themeName.setText(c.getTheme_name());

        holder.forclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forcl = new Intent(con, themesRestaurantSearch.class);
                forcl.putExtra("themeId", c.getTheme_id());
                forcl.putExtra("themeName", c.getTheme_name());
                con.startActivity(forcl);
                Animation.split(con);
            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurantsclassList.size();
    }

}
