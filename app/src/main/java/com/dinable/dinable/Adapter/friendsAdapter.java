package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.friendsModel;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class friendsAdapter extends RecyclerView.Adapter<friendsAdapter.MyViewHolder> {

    private Context context;
    private List<friendsModel> friendsList;

    private static final String URL2 = "https://www.voltasinc.us/dinable/app_api/index.php";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView friendName, friendEmail;
        private CircularImageView friendImage;
        private Button btnSendRequest;

        public MyViewHolder(View view) {
            super(view);
            friendName = (TextView) view.findViewById(R.id.friendName);
            friendEmail = (TextView) view.findViewById(R.id.friendEmail);
            friendImage = (CircularImageView) view.findViewById(R.id.friendImage);
            btnSendRequest = (Button) view.findViewById(R.id.btn_sendRequest);
        }
    }

    public friendsAdapter(Context context, List<friendsModel> friendsList) {
        this.context = context;
        this.friendsList = friendsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_friend_request_list, null);
        return new friendsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final friendsModel friend = friendsList.get(position);

        holder.friendName.setText(friend.getFriendName());
        holder.friendEmail.setText(friend.getFriendEmail());

        holder.btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendFriendRequest(friend.getFriendId());
            }
        });

        Glide.with(context)
                .load(friend.getFriendImage())
                .into(holder.friendImage);

    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }

    private void sendFriendRequest(final String reqUserId) {

        StringRequest request = new StringRequest(Request.Method.POST, URL2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Friends", response);

                        if (response == null) {
                            Toast.makeText(context, "Couldn't fetch the menu! Please try again.", Toast.LENGTH_LONG).show();
                            return;
                        }

                        Toast.makeText(context, "Request Sent!!!", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e("Friends", "Error: " + error.getMessage());
                Toast.makeText(context, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("req_key", "friend_request_by_user");
                params.put("user_id", Prefs.getString("user_id", ""));
                params.put("req_user_id", reqUserId);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}
