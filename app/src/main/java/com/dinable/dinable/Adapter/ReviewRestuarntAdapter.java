package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.ReviewRestuarantClass;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class ReviewRestuarntAdapter extends RecyclerView.Adapter<ReviewRestuarntAdapter.ResReviewViewHolder>
{
    private Context context;
    private List<ReviewRestuarantClass> reviewRestuarantClassList;

    class ResReviewViewHolder extends RecyclerView.ViewHolder {
        TextView _u_name;
        TextView _u_review;
        RatingBar _u_rating;
        ImageView _u_Image;

        public ResReviewViewHolder(View itemView)
        {
            super(itemView);
            _u_name = (TextView) itemView.findViewById(R.id.user_name);
            _u_review = (TextView) itemView.findViewById(R.id.user_review);
            _u_rating = (RatingBar) itemView.findViewById(R.id.user_rating);
            _u_Image = (ImageView) itemView.findViewById(R.id.user_image_view);
        }
    }

    public ReviewRestuarntAdapter(Context context, List<ReviewRestuarantClass> reviewRestuarantClassList) {
        this.context = context;
        this.reviewRestuarantClassList = reviewRestuarantClassList;
    }

    @Override
    public void onBindViewHolder(ResReviewViewHolder holder , int position)
    {
        final ReviewRestuarantClass reviewRestuarantClass = reviewRestuarantClassList.get(position);

        //loading the image
        Glide.with(context)
                .load(reviewRestuarantClass.getRv_u_image())
                .into(holder._u_Image);
        holder._u_name.setText(reviewRestuarantClass.getRv_u_name());
        holder._u_review.setText(reviewRestuarantClass.getRv_u_review());
        holder._u_rating.setRating(reviewRestuarantClass.getRv_u_rating());

    }

    @Override
    public ResReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_review_restuarant_list, null);
        return new ResReviewViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return reviewRestuarantClassList.size();
    }
}
