package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.friendsModel;

import java.util.List;

public class invitedFriendsAdapter extends RecyclerView.Adapter<invitedFriendsAdapter.MyViewHolder> {

    private Context context;
    private List<friendsModel> friendsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView friendName, friendEmail, friendStatus;

        public MyViewHolder(View view) {
            super(view);
            friendName = (TextView) view.findViewById(R.id.friendName);
            friendEmail = (TextView) view.findViewById(R.id.friendEmail);
            friendStatus = (TextView) view.findViewById(R.id.friendStatus);
        }
    }

    public invitedFriendsAdapter(Context context, List<friendsModel> friendsList) {
        this.context = context;
        this.friendsList = friendsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_invited_friends, null);
        return new invitedFriendsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final friendsModel friend = friendsList.get(position);

        holder.friendName.setText(friend.getFriendName());
        holder.friendEmail.setText(friend.getFriendEmail());

        switch (friend.getFriendStatus()) {
            case "0":
                holder.friendStatus.setText("Invitation Pending");
                break;

            case "1":
                holder.friendStatus.setText("Invitation Accepted");
                break;

            case "2":
                holder.friendStatus.setText("Invitation Declined");
                break;

                default:
                    holder.friendStatus.setText("Invitation Pending");
        }
    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }
}
