package com.dinable.dinable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.RestaurantPage;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.restaurantsclass;

import java.util.List;


public class restaurantsadapter extends
        RecyclerView.Adapter<restaurantsadapter.MyViewHolder> {

    private List<restaurantsclass> restaurantsclassList;
    private Context con;
    private boolean isThemeActivity = false;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout forimagebanner;
        private TextView restname, dolar, distance, cuisines, directions, promoPoints;
        private RatingBar restrate;
        private ImageView ab;
        private View forclick;

        public MyViewHolder(View view) {
            super(view);
            //forimagebanner=(LinearLayout) view.findViewById(R.id.restaurantsbanners) ;
            restname = (TextView) view.findViewById(R.id.restaurantsname);
            dolar = (TextView) view.findViewById(R.id.cost);
            restrate = (RatingBar) view.findViewById(R.id.restaurantsratings);
            ab = (ImageView) view.findViewById(R.id.restaurantsbanners);
            distance = (TextView) view.findViewById(R.id.distance);
            forclick = (View) view.findViewById(R.id.forrest);
            cuisines = (TextView) view.findViewById(R.id.cuisines);
            directions = (TextView) view.findViewById(R.id.directions);
            promoPoints = (TextView) view.findViewById(R.id.promoPoints);
        }
    }

    public restaurantsadapter(Context c, List<restaurantsclass> restaurantsclassList, boolean isThemeActivity) {
        this.restaurantsclassList = restaurantsclassList;
        this.con = c;
        this.isThemeActivity = isThemeActivity;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final restaurantsclass c = restaurantsclassList.get(position);
        Glide.with(con)
                .load(c.getRestaurant_image())
                .into(holder.ab);
        holder.forclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forcl = new Intent(con, RestaurantPage.class);
                forcl.putExtra("rest_name", c.getRestaurant_name());
                forcl.putExtra("rest_id", c.getRestaurant_id());
                forcl.putExtra("rest_image", c.getRestaurant_image());
                forcl.putExtra("rest_self", c.getSelf_res());
                forcl.putExtra("rest_lat", c.getRestaurantLat());
                forcl.putExtra("rest_long", c.getRestaurantLong());
                con.startActivity(forcl);
                Animation.split(con);
            }
        });

        if(isThemeActivity) {
            holder.promoPoints.setVisibility(View.VISIBLE);
            if(c.getThemeAttractions().equalsIgnoreCase("null"))
                holder.promoPoints.setText("No Special Attractions");
            else
                holder.promoPoints.setText(c.getThemeAttractions());
        } else
            holder.promoPoints.setVisibility(View.GONE);

        holder.directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + c.getRestaurantLat() + "," + c.getRestaurantLong());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                con.startActivity(mapIntent);
            }
        });

        holder.restname.setText(c.getRestaurant_name());
        holder.restrate.setRating(c.getRestaurant_rating());
        holder.dolar.setText(c.getCost());
        holder.cuisines.setText(c.getCuisines());

        if (c.getDistance().equals("1"))
            holder.distance.setText(c.getDistance() + " KM");
        else
            holder.distance.setText(c.getDistance() + " KMs");

    }

    @Override
    public int getItemCount() {
        return restaurantsclassList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardsforrestaurant, parent, false);
        return new MyViewHolder(v);
    }

    public void searchedList(List<restaurantsclass> _list) {
        restaurantsclassList = _list;
        notifyDataSetChanged();
    }
}