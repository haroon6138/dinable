package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Booking_status;
import com.dinable.dinable.classes.userInvites;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class invitationsListAdapter extends RecyclerView.Adapter<invitationsListAdapter.myFriendsViewHolder> {

    private Context context;
    private List<userInvites> friendsModelList;

    public class myFriendsViewHolder extends RecyclerView.ViewHolder {
        private TextView rest_name, theme, date, timefrom, timeto, status, invitedBy;
        private ImageView rest_image;
        private ImageView btnAcceptRequest, btnRejectRequest;

        public myFriendsViewHolder(View view) {
            super(view);
            rest_image=view.findViewById(R.id.rest_image);
            rest_name=view.findViewById(R.id.rest_name);
            theme=view.findViewById(R.id.theme);
            date=view.findViewById(R.id.date);
            timefrom=view.findViewById(R.id.timefrom);
            timeto=view.findViewById(R.id.timeto);
            invitedBy = view.findViewById(R.id.people);
            btnAcceptRequest = view.findViewById(R.id.btn_accept);
            btnRejectRequest = view.findViewById(R.id.btn_reject);
        }
    }

    public invitationsListAdapter(Context context, List<userInvites> friendsModelList){
        this.context = context;
        this.friendsModelList = friendsModelList;
    }

    @NonNull
    @Override
    public myFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_invites,parent, false);
        return new invitationsListAdapter.myFriendsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull myFriendsViewHolder holder, int position) {

        final userInvites userInvites = friendsModelList.get(position);

        Glide.with(context)
                .load(userInvites.getR_header_image())
                .into(holder.rest_image);
        holder.rest_name.setText(userInvites.getRestaurant_name());

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(userInvites.getRequested_time_to());
            System.out.println(dateObj);
            System.out.println(new SimpleDateFormat("K:mm").format(dateObj));

            holder.theme.setText(userInvites.getBookingTheme());
            holder.date.setText(" "+userInvites.getDate_of_request().toString());
            holder.timefrom.setText(" ");
            holder.timeto.setText(" " + new SimpleDateFormat("hh:mm aa").format(dateObj));
            holder.invitedBy.setText(" "+userInvites.getInvitedByName()+"");

        } catch (final ParseException e) {
            e.printStackTrace();
        }
        holder.btnAcceptRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendFriendRequestResponse("1", userInvites.getInviterId(), userInvites.getBookingId());
            }
        });

        holder.btnRejectRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendFriendRequestResponse("2", userInvites.getInviterId(), userInvites.getBookingId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return friendsModelList.size();
    }

    // Method to send friend request accept or reject response
    private void sendFriendRequestResponse(final String status, final String acceptId, final String bookingId) {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Friends", response);
//                if (response.contains("null")) {
//                    Toasty.warning(getActivity(), "Friends Not Available", Toast.LENGTH_LONG).show();
//                } else {
            }
//            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("req_key", "Restaurants");

                params.put("req_key", "invite_actions");
                params.put("user_id", Prefs.getString("user_id", ""));
                params.put("req_user_id", acceptId);
                params.put("action", status);
                params.put("b_id", bookingId);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}
