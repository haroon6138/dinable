package com.dinable.dinable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.My_Bookings;
import com.dinable.dinable.Activity.RestaurantPage;
import com.dinable.dinable.Activity.bookingDetailsActivity;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Booking_status;
import com.dinable.dinable.classes.restaurantsclass;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class for_booking_cards_adaptor extends
        RecyclerView.Adapter<for_booking_cards_adaptor.MyViewHolder> {

    private List<Booking_status> bookingStatusList;
    private Context con;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView rest_name, theme, date, timefrom, timeto, noofpeople, status;
        private ImageView rest_image;
        private CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            rest_image = view.findViewById(R.id.rest_image);
            rest_name = view.findViewById(R.id.rest_name);
            theme = view.findViewById(R.id.theme);
            date = view.findViewById(R.id.date);
            timefrom = view.findViewById(R.id.timefrom);
            timeto = view.findViewById(R.id.timeto);
            noofpeople = view.findViewById(R.id.people);
            status = view.findViewById(R.id.status);
            cardView = view.findViewById(R.id.card_view);
        }
    }

    public for_booking_cards_adaptor(Context c, List<Booking_status> statusList) {
        this.bookingStatusList = statusList;
        this.con = c;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Booking_status c = bookingStatusList.get(position);

        Glide.with(con)
                .load(c.getR_header_image())
                .into(holder.rest_image);

        holder.rest_name.setText(c.getRestaurant_name());


        holder.theme.setText(c.getTheme());
        holder.date.setText(" " + c.getDate_of_request().toString());
        holder.timefrom.setText(" " + c.getRequested_time_from().toString());
        holder.timeto.setText(" " + c.getRequested_time_to().toString());
        holder.noofpeople.setText(" " + c.getNo_of_people() + "");
        if (c.getRejected().equals("0")) {
            if (c.getApprove_pending().equals("0")) {
                holder.status.setText("Pending");
            } else if (c.getApprove_pending().equals("2")) {
                holder.status.setText("Previous Booking.");
            } else {
                holder.status.setText("Approved");
            }
        } else {
            holder.status.setText("Rejected");
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(con, bookingDetailsActivity.class);
                intent.putExtra("restImage", c.getR_header_image());
                intent.putExtra("restaurantNameText", c.getRestaurant_name());
                intent.putExtra("restTheme", c.getTheme());
                intent.putExtra("bookingDate", c.getDate_of_request());
                intent.putExtra("bookingTime", c.getRequested_time_to());
                intent.putExtra("noofpeople", c.getNo_of_people());
                intent.putExtra("friendsInvited", c.getUsersInvited().toString());

                con.startActivity(intent);
            }
        });

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (c.getRejected().equals("1")) {
                    new SweetAlertDialog(con, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Rejecting Reason")
                            .setContentText(c.getRejecting_reason().toString())
                            .show();
                }
                return true;
            }
        });

    }


    @Override
    public int getItemCount() {
        return bookingStatusList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.for_showing_booking_cards, parent, false);
        return new MyViewHolder(v);
    }
}