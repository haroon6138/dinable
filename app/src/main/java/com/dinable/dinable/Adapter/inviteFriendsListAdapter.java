package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.dinable.dinable.Activity.inviteFriendsActivity;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.EventRestClass;
import com.dinable.dinable.classes.friendsModel;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class inviteFriendsListAdapter extends RecyclerView.Adapter<inviteFriendsListAdapter.myFriendsViewHolder> {

    private Context context;
    private List<friendsModel> friendsModelList;

    public class myFriendsViewHolder extends RecyclerView.ViewHolder {
        private TextView friendName, friendEmail, friendStatus;
        private CircularImageView friendImage;
        private CheckBox inviteCheckbox;

        public myFriendsViewHolder(View view) {
            super(view);
            friendName = (TextView) view.findViewById(R.id.friendName);
            friendEmail = (TextView) view.findViewById(R.id.friendEmail);
            friendImage = (CircularImageView) view.findViewById(R.id.avatar);
            inviteCheckbox = (CheckBox) view.findViewById(R.id.invite_checkbox);
        }
    }

    public inviteFriendsListAdapter(Context context, List<friendsModel> friendsModelList){
        this.context = context;
        this.friendsModelList = friendsModelList;
    }

    @NonNull
    @Override
    public myFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_invite_friends_list, null);
        return new inviteFriendsListAdapter.myFriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final myFriendsViewHolder holder, int position) {

        final friendsModel friendsClass = friendsModelList.get(position);

        holder.friendName.setText(friendsClass.getFriendName());
        holder.friendEmail.setText(friendsClass.getFriendEmail());

        try {
            if(friendsClass.getFriendImage().equals("null"))
                Picasso.get().load(R.drawable.dinablelogo).into(holder.friendImage);
            else
                Picasso.get().load(friendsClass.getFriendImage()).into(holder.friendImage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        holder.inviteCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inviteFriendsActivity.friendsSelected < Integer.valueOf(inviteFriendsActivity.noOfPeopleForBooking)) {
                    if (holder.inviteCheckbox.isChecked()) {
                        inviteFriendsActivity.invitedFriends.add(friendsClass.getFriendId() + "-" + friendsClass.getFriendName());
                        inviteFriendsActivity.friendsSelected++;
                    } else {
                        inviteFriendsActivity.invitedFriends.remove(friendsClass.getFriendId() + "-" + friendsClass.getFriendName());
                        inviteFriendsActivity.friendsSelected--;
                    }
                } else {
                    if(holder.inviteCheckbox.isChecked())
                        holder.inviteCheckbox.setChecked(false);
                        Toasty.error(context, "Max No. Of Invitees Reached!").show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(friendsModelList != null)
            return friendsModelList.size();
        else
            return 0;
    }

    public void searchedList(List<friendsModel> _list)
    {
        friendsModelList = _list;
        notifyDataSetChanged();
    }

}
