package com.dinable.dinable.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.place_orders_list_showing;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.order_menu_class;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class Booking_Reciept_adaptor extends RecyclerView.Adapter<Booking_Reciept_adaptor.Booking_RecieptViewHolder>
{
    private Context context;
    private List<order_menu_class> orderMenuClasseslist;

    String abcd="no";

    class Booking_RecieptViewHolder extends RecyclerView.ViewHolder {
        TextView _u_name;
        TextView _u_review;
        RatingBar _u_rating;
        ImageView _u_Image;
        CardView cardView;
        Button rateItem;

        public Booking_RecieptViewHolder(View itemView)
        {
            super(itemView);
            cardView=itemView.findViewById(R.id.card_view);
            _u_name = (TextView) itemView.findViewById(R.id.user_name);
            _u_review = (TextView) itemView.findViewById(R.id.user_review);
            _u_rating = (RatingBar) itemView.findViewById(R.id.user_rating);
            _u_Image = (ImageView) itemView.findViewById(R.id.user_image_view);
            rateItem = itemView.findViewById(R.id.rateItem);
        }
    }

    public Booking_Reciept_adaptor(Context context, List<order_menu_class> orderMenuClasseslist) {
        this.context = context;
        this.orderMenuClasseslist = orderMenuClasseslist;
    }

    @Override
    public void onBindViewHolder(Booking_RecieptViewHolder holder , int position)
    {
        final order_menu_class orderMenuClass = orderMenuClasseslist.get(position);

        //loading the image
        Glide.with(context)
                .load(orderMenuClass.getMn_image())
                .into(holder._u_Image);
        holder._u_review.setText(orderMenuClass.getMn_nm());
        holder._u_name.setText("x"+orderMenuClass.getMenu_qty()+"       RS."+(Integer.parseInt(orderMenuClass.getTotal_price())/Integer.parseInt(orderMenuClass.getMenu_qty())));
        holder._u_rating.setRating(orderMenuClass.getMn_rating());

         holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDialogsss(orderMenuClass.getMenu_id(),"Rate "+orderMenuClass.getMn_nm(),orderMenuClass.getMenu_id());
                return true;
            }
        });

         holder.rateItem.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 showDialogsss(orderMenuClass.getMenu_id(),"Rate "+orderMenuClass.getMn_nm(), orderMenuClass.getMenu_id());
             }
         });

    }

    @Override
    public Booking_RecieptViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_review_restuarant_list, null);
        return new Booking_RecieptViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if(orderMenuClasseslist != null)
            return orderMenuClasseslist.size();
        else
            return 0;
    }

    @SuppressLint("ResourceType")
    public void showDialogsss(final String msg, String title,final String menu_id){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.rating_dialog_menu);
        dialog.setTitle("Rate this Item");
        final RatingBar r_t=dialog.findViewById(R.id.user_ratings);
        r_t.setStepSize((float) 0.5);
        r_t.setMax(5);
        r_t.setId(1);
        r_t.setRating(2.0f);
        final EditText u_c=dialog.findViewById(R.id.user_comment);
        TextView cancel=dialog.findViewById(R.id.cancel);
        TextView submit=dialog.findViewById(R.id.submit);
        TextView titView=dialog.findViewById(R.id.rate_title);
        titView.setText(title);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestRatingss(menu_id,r_t.getRating(),u_c.getText().toString());
                dialog.dismiss();
            }
        });

        dialog.show();
    }




    public void requestRatingss(final String menu_id,final float rate,final String comment) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key","add_menu_ratting_review");
        params.put("menu_id",menu_id);
        params.put("rating",rate);
        params.put("review",comment);
        params.put("user_id",id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(context, "Response is null", Toast.LENGTH_SHORT).show();
                }else {

                    try {
                        final JSONObject object  = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if(object.getBoolean("success")) {
                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good Job!")
                                    .setContentText(object.getString("message"))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }else {
                            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response",response);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String  response  = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(context, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                }else {

                    Log.d("response",response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                try {
//                    pd.dismiss();
                }catch (Exception e){

                }
            }
        });
    }
}
