package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Menu_reviews_class;

import java.util.List;

public class for_menu_reviews_adaptor extends RecyclerView.Adapter<for_menu_reviews_adaptor.menuReviewViewHolder>
{
    private Context context;
    private List<Menu_reviews_class> menu_reviews_classList;

    class menuReviewViewHolder extends RecyclerView.ViewHolder {
        TextView _u_name;
        TextView _u_review;
        RatingBar _u_rating;
        ImageView _u_Image;

        public menuReviewViewHolder(View itemView)
        {
            super(itemView);
            _u_name = (TextView) itemView.findViewById(R.id.user_name);
            _u_review = (TextView) itemView.findViewById(R.id.user_review);
            _u_rating = (RatingBar) itemView.findViewById(R.id.user_rating);
            _u_Image = (ImageView) itemView.findViewById(R.id.user_image_view);
        }
    }

    public for_menu_reviews_adaptor(Context context, List<Menu_reviews_class> menu_reviews_classList) {
        this.context = context;
        this.menu_reviews_classList = menu_reviews_classList;
    }

    @Override
    public void onBindViewHolder(menuReviewViewHolder holder , int position)
    {
        final Menu_reviews_class menuReviewsClass = menu_reviews_classList.get(position);

        //loading the image
        Glide.with(context)
                .load(menuReviewsClass.getU_image())
                .into(holder._u_Image);
        holder._u_name.setText(menuReviewsClass.getUsr_nm());
        holder._u_review.setText(menuReviewsClass.getReview());
        holder._u_rating.setRating(menuReviewsClass.getRating());

    }

    @Override
    public menuReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_review_restuarant_list, null);
        return new menuReviewViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return menu_reviews_classList.size();
    }
}
