package com.dinable.dinable.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.MyEventClass;
import com.pkmmte.view.CircularImageView;

import java.util.List;


public class MyEventAdapter extends RecyclerView.Adapter<MyEventAdapter.MyEventViewHolder>
{
    private Context context;
    private List<MyEventClass> myEventClassList;

    class MyEventViewHolder extends RecyclerView.ViewHolder {
        TextView _evt_title, _evt_res_name, _evt_date, _evt_time, _evt_charges, _evt_u_status;
        ImageView _evt_Image;

        public MyEventViewHolder(View itemView)
        {
            super(itemView);
            _evt_title = (TextView) itemView.findViewById(R.id.evt_name);
            _evt_res_name = (TextView) itemView.findViewById(R.id.rest_name);
            _evt_date = (TextView) itemView.findViewById(R.id.ev_date);
            _evt_time = (TextView) itemView.findViewById(R.id.ev_time);
            _evt_charges = (TextView) itemView.findViewById(R.id.ev_price);
            _evt_u_status = (TextView) itemView.findViewById(R.id.ev_status);
            _evt_Image = (ImageView) itemView.findViewById(R.id.rest_image);
        }
    }

    public MyEventAdapter(Context context, List<MyEventClass> myEventClassList) {
        this.context = context;
        this.myEventClassList = myEventClassList;
    }

    @Override
    public void onBindViewHolder(MyEventViewHolder holder , int position)
    {
        final MyEventClass myEventClass = myEventClassList.get(position);

        //loading the image
        Glide.with(context)
                .load(myEventClass.getEvt_usr_image())
                .into(holder._evt_Image);
        holder._evt_title.setText(myEventClass.getEvt_usr_title());
        holder._evt_res_name.setText(myEventClass.getEvt_usr_res_name());
        holder._evt_date.setText("Date: "+myEventClass.getEvt_usr_date());
        holder._evt_time.setText("Time: "+myEventClass.getEvt_usr_time());
        holder._evt_charges.setText(myEventClass.getEvt_usr_charges());
        holder._evt_u_status.setText("Request: "+myEventClass.getEvt_usr_status());

    }

    @Override
    public MyEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_events_list, null);
        return new MyEventViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return myEventClassList.size();
    }
}
