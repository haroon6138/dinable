package com.dinable.dinable.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.media.Rating;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.bumptech.glide.Glide;
import com.dinable.dinable.Activity.For_Cusine_menu_Item_details;
import com.dinable.dinable.Activity.Restaurant_add_to_cart;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.CusineMenuClass;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.For_add_to_card_class;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import es.dmoral.toasty.Toasty;


public class formenuadapter extends RecyclerView.Adapter<formenuadapter.MyViewHolder>
{
    private Context context;
    private List<CusineMenuClass> cusineMenuClassList;
    private String show_add_to_cart,rest_id,rest_name,For_showing_checking;
    CounterFab floatingActionButton;


    /**
     * View holder class
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private View menuview;
        private TextView menudes,menuprice,menutitle;
        private ImageView menuimage,add_to_card;
        private RatingBar menurating;

        Database_Helper database_helper;

        public MyViewHolder(View view) {
            super(view);
            menuview = (View) view.findViewById(R.id.cusine_card_view);
            menutitle = (TextView) view.findViewById(R.id.cusine_menu_title);
            menudes = (TextView) view.findViewById(R.id.cs_tvdesc);
            menuprice = (TextView) view.findViewById(R.id.cusine_price);
            menurating = (RatingBar) view.findViewById(R.id.restaurantsratings);
            menuimage = (ImageView) view.findViewById(R.id.menu_image);
            add_to_card=(ImageView) view.findViewById(R.id.add_to_card);
            if(show_add_to_cart!=null){
                add_to_card.setVisibility(View.VISIBLE);
            }
            FloatingActionButton fab = new FloatingActionButton(context);
            database_helper=new Database_Helper(context);
        }


    }

    public formenuadapter(Context c, List<CusineMenuClass> cusineMenuClassList,String show_add_to_cart,String rest_id,String Rest_name,String For_showing_checking, CounterFab fab) {
        this.cusineMenuClassList = cusineMenuClassList;
        this.context=c;
        this.show_add_to_cart=show_add_to_cart;
        this.rest_id=rest_id;
        this.rest_name=Rest_name;
        this.For_showing_checking=For_showing_checking;
        this.floatingActionButton = fab;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CusineMenuClass c=cusineMenuClassList.get(position);
        holder.menudes.setText(c.getCusine_menu_desc());
        holder.menutitle.setText(c.getCusine_menu_name());
        holder.menuprice.setText("RS."+c.getCusine_menu_price());
        Glide.with(context)
                .load(c.getCusine_menu_image())
                .into(holder.menuimage);
        holder.menurating.setRating(Float.valueOf(c.getCusine_menu_rating()));


        holder.add_to_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    holder.database_helper.insert_carts(c.getCusine_menu_id(),"1",c.getCusine_menu_price(),c.getCusine_menu_name(),rest_id,c.getCusine_menu_image());
                    final Snackbar snackbar = Snackbar.make((((Activity) context).findViewById(android.R.id.content)), c.getCusine_menu_name() + "is added in your order list.", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    floatingActionButton.increase();
//                    snackbar.setAction("View Cart", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent in=new Intent(context,Restaurant_add_to_cart.class);
//                            in.putExtra("rest_name",rest_name);
//                            context.startActivity(in);
//                            Animation.slideUp(context);
//
//                        }
//
//                    });

                }catch (SQLiteConstraintException e){
                    Toasty.error(context,c.getCusine_menu_name() + "is already added in your order list.",Toast.LENGTH_SHORT).show();
                }
            }

        });
        holder.menuview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context,For_Cusine_menu_Item_details.class);
                in.putExtra("cusine_name",c.getCusine_menu_name());
                in.putExtra("cusine_image",c.getCusine_menu_image());
                in.putExtra("cusine_rest_id",rest_id);
                in.putExtra("cusine_idxyz",c.getCusine_menu_id()+"");
                in.putExtra("cusine_desc",c.getCusine_menu_desc());
                in.putExtra("cusine_rating",c.getCusine_menu_rating());
                in.putExtra("cusine_price",c.getCusine_menu_price());
                in.putExtra("show_cart",For_showing_checking);
                in.putExtra("rest_name",rest_name);
                context.startActivity(in);
                Animation.slideUp(context);
            }
        });


    }


    @Override
    public int getItemCount() { return cusineMenuClassList.size(); }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cusine_detail_card,parent, false);
        return new MyViewHolder(v);
    }

}