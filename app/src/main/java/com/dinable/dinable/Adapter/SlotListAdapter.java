package com.dinable.dinable.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dinable.dinable.R;
import com.dinable.dinable.classes.restaurantsclass;
import com.dinable.dinable.classes.slot;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class SlotListAdapter extends RecyclerView.Adapter<SlotListAdapter.MyViewHolder> {

    private Context context;
    private List<slot> slotList;
    private Activity activity;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView time, remaining, booking;

        public MyViewHolder(View view) {
            super(view);
            time = view.findViewById(R.id.Timing);
            remaining = view.findViewById(R.id.remainingBooking);
            booking = view.findViewById(R.id.currbooked);
        }
    }

    public SlotListAdapter(Context context, List<slot> slotList, Activity activity) {
        this.context = context;
        this.slotList = slotList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_slot_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final slot slotClass = slotList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("selectedTimeSlot", slotClass.getTime());
                activity.setResult(RESULT_OK, intent);
                activity.finish();
            }
        });

        final slot slotObj = slotList.get(position);
        holder.time.setText(slotObj.getTime());
        holder.remaining.setText(slotObj.getNo_of_people_remaining());
        holder.booking.setText(slotObj.getBooked());

    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }
}
