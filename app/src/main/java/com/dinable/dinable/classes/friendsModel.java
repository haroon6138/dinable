package com.dinable.dinable.classes;

public class friendsModel {

    String friendId;
    String friendName;
    String friendImage;
    String friendEmail;
    String friendStatus;
    String friendFCM;

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendImage() {
        return friendImage;
    }

    public void setFriendImage(String friendImage) {
        this.friendImage = friendImage;
    }

    public String getFriendEmail() {
        return friendEmail;
    }

    public void setFriendEmail(String friendEmail) {
        this.friendEmail = friendEmail;
    }

    public String getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(String friendStatus) {
        this.friendStatus = friendStatus;
    }

    public String getFriendFCM() {
        return friendFCM;
    }

    public void setFriendFCM(String friendFCM) {
        this.friendFCM = friendFCM;
    }
}
