package com.dinable.dinable.classes;

import android.os.Parcel;
import android.os.Parcelable;

public class For_add_to_card_class implements Parcelable {

    public For_add_to_card_class(int item_id, int item_count, String item_price, String item_name, String rest_id, String item_image) {
        this.item_id = item_id;
        this.item_count = item_count;
        Item_price = item_price;
        Item_name = item_name;
        this.rest_id = rest_id;
        this.item_image = item_image;
    }

    private int item_id;
    private int item_count;

    public int getItem_id() {
        return item_id;
    }

    public int getItem_count() {
        return item_count;
    }

    public String getItem_price() {
        return Item_price;
    }

    public String getItem_name() {
        return Item_name;
    }

    public String getRest_id() {
        return rest_id;
    }

    public String getItem_image() {
        return item_image;
    }

    private String Item_price;
    private String Item_name;
    private String rest_id;
    private String item_image;


    protected For_add_to_card_class(Parcel in) {
        this.item_id = in.readInt();
        this.item_count = in.readInt();
        this.Item_price = in.readString();
        this.Item_name = in.readString();
        this.rest_id = in.readString();
        this.item_image = in.readString();
    }

    public static final Creator<For_add_to_card_class> CREATOR = new Creator<For_add_to_card_class>() {
        @Override
        public For_add_to_card_class createFromParcel(Parcel in) {
            return new For_add_to_card_class(in);
        }

        @Override
        public For_add_to_card_class[] newArray(int size) {
            return new For_add_to_card_class[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(item_id);
        dest.writeInt(item_count);
        dest.writeString(Item_price);
        dest.writeString(Item_name);
        dest.writeString(rest_id);
        dest.writeString(item_image);
    }
}
