package com.dinable.dinable.classes;

public class notifications_history_model {

    String notification_Id;
    String notification_Title;
    String notification_Description;
    String notification_DateTime;


    public String getNotification_Id() {
        return notification_Id;
    }

    public void setNotification_Id(String notification_Id) {
        this.notification_Id = notification_Id;
    }

    public String getNotification_Title() {
        return notification_Title;
    }

    public void setNotification_Title(String notification_Title) {
        this.notification_Title = notification_Title;
    }

    public String getNotification_Description() {
        return notification_Description;
    }

    public void setNotification_Description(String notification_Description) {
        this.notification_Description = notification_Description;
    }

    public String getNotification_DateTime() {
        return notification_DateTime;
    }

    public void setNotification_DateTime(String notification_DateTime) {
        this.notification_DateTime = notification_DateTime;
    }
}
