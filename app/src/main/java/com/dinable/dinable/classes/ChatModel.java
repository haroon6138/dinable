package com.dinable.dinable.classes;

import android.text.format.DateFormat;

import java.util.Date;

    public class ChatModel {
        private String messageText;
        private String messageUser;
        private String messageTime;
        private String messageUserName;

        public ChatModel(String messageText, String messageUser, String messageUserName) {
            this.messageText = messageText;
            this.messageUser = messageUser;
            this.messageUserName = messageUserName;

            messageTime = (DateFormat.format("dd-MM-yyyy hh:mm:ss", new Date()).toString());
        }

        public ChatModel() {
        }

        public String getMessageText() {
            return messageText;
        }

        public void setMessageText(String messageText) {
            this.messageText = messageText;
        }

        public String getMessageUser() {
            return messageUser;
        }

        public void setMessageUser(String messageUser) {
            this.messageUser = messageUser;
        }

        public String getMessageTime() {
            return messageTime;
        }

        public void setMessageTime(String messageTime) {
            this.messageTime = messageTime;
        }

        public String getMessageUserName() {
            return messageUserName;
        }

        public void setMessageUserName(String messageUserName) {
            this.messageUserName = messageUserName;
        }
    }
