package com.dinable.dinable.classes;

public class restaurantsclass {
    String restaurant_id, self_res;
    float restaurant_rating;
    String cost;
    String distance;
    String cuisines;
    double restaurantLat;
    double restaurantLong;
    String themeAttractions;

    public double getRestaurantLat() {
        return restaurantLat;
    }

    public void setRestaurantLat(double restaurantLat) {
        this.restaurantLat = restaurantLat;
    }

    public double getRestaurantLong() {
        return restaurantLong;
    }

    public void setRestaurantLong(double restaurantLong) {
        this.restaurantLong = restaurantLong;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getSelf_res() {
        return self_res;
    }

    public void setSelf_res(String self_res) {
        this.self_res = self_res;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public restaurantsclass() {
    }

    String restaurant_name;

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public float getRestaurant_rating() {
        return restaurant_rating;
    }

    public void setRestaurant_rating(int restaurant_rating) {
        this.restaurant_rating = restaurant_rating;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurant_image() {
        return restaurant_image;
    }

    public void setRestaurant_image(String restaurant_image) {
        this.restaurant_image = restaurant_image;
    }

    String restaurant_image;

    public String getThemeAttractions() {
        return themeAttractions;
    }

    public void setThemeAttractions(String themeAttractions) {
        this.themeAttractions = themeAttractions;
    }
}
