package com.dinable.dinable.classes;

public class userInvites {

    String restaurant_name;
    String r_header_image;
    String no_of_people;
    String requested_time_to;
    String date_of_request;
    String invitedByName;
    String bookingId;
    String bookingTheme;
    String inviterId;

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getR_header_image() {
        return r_header_image;
    }

    public void setR_header_image(String r_header_image) {
        this.r_header_image = r_header_image;
    }

    public String getNo_of_people() {
        return no_of_people;
    }

    public void setNo_of_people(String no_of_people) {
        this.no_of_people = no_of_people;
    }

    public String getRequested_time_to() {
        return requested_time_to;
    }

    public void setRequested_time_to(String requested_time_to) {
        this.requested_time_to = requested_time_to;
    }

    public String getDate_of_request() {
        return date_of_request;
    }

    public void setDate_of_request(String date_of_request) {
        this.date_of_request = date_of_request;
    }

    public String getInvitedByName() {
        return invitedByName;
    }

    public void setInvitedByName(String invitedByName) {
        this.invitedByName = invitedByName;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingTheme() {
        return bookingTheme;
    }

    public void setBookingTheme(String bookingTheme) {
        this.bookingTheme = bookingTheme;
    }

    public String getInviterId() {
        return inviterId;
    }

    public void setInviterId(String inviterId) {
        this.inviterId = inviterId;
    }
}
