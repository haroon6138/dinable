package com.dinable.dinable.classes;

public class ReviewRestuarantClass
{
    String rv_u_name, rv_u_image, rv_u_review;
    int rv_u_rating;

    public String getRv_u_name() {
        return rv_u_name;
    }

    public void setRv_u_name(String rv_u_name) {
        this.rv_u_name = rv_u_name;
    }

    public String getRv_u_image() {
        return rv_u_image;
    }

    public void setRv_u_image(String rv_u_image) {
        this.rv_u_image = rv_u_image;
    }

    public String getRv_u_review() {
        return rv_u_review;
    }

    public void setRv_u_review(String rv_u_review) {
        this.rv_u_review = rv_u_review;
    }

    public int getRv_u_rating() {
        return rv_u_rating;
    }

    public void setRv_u_rating(int rv_u_rating) {
        this.rv_u_rating = rv_u_rating;
    }
}
