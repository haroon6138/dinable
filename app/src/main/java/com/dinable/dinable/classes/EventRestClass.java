package com.dinable.dinable.classes;

public class EventRestClass
{
    String evt_id, evt_rest_id, evt_title, evt_desc, evt_image, evt_time_from, evt_time_to, evt_per_head_charges, evt_date, evt_cat_name;
    int evt_rating;

    public String getEvt_id() {
        return evt_id;
    }

    public void setEvt_id(String evt_id) {
        this.evt_id = evt_id;
    }

    public String getEvt_rest_id() {
        return evt_rest_id;
    }

    public void setEvt_rest_id(String evt_rest_id) {
        this.evt_rest_id = evt_rest_id;
    }

    public String getEvt_title() {
        return evt_title;
    }

    public void setEvt_title(String evt_title) {
        this.evt_title = evt_title;
    }

    public String getEvt_desc() {
        return evt_desc;
    }

    public void setEvt_desc(String evt_desc) {
        this.evt_desc = evt_desc;
    }

    public String getEvt_image() {
        return evt_image;
    }

    public void setEvt_image(String evt_image) {
        this.evt_image = evt_image;
    }

    public String getEvt_date() {
        return evt_date;
    }

    public void setEvt_date(String evt_date) {
        this.evt_date = evt_date;
    }

    public String getEvt_time_from() {
        return evt_time_from;
    }

    public void setEvt_time_from(String evt_time_from) {
        this.evt_time_from = evt_time_from;
    }

    public String getEvt_time_to() {
        return evt_time_to;
    }

    public void setEvt_time_to(String evt_time_to) {
        this.evt_time_to = evt_time_to;
    }

    public String getEvt_per_head_charges() {
        return evt_per_head_charges;
    }

    public void setEvt_per_head_charges(String evt_per_head_charges) {
        this.evt_per_head_charges = evt_per_head_charges;
    }

    public int getEvt_rating() {
        return evt_rating;
    }

    public void setEvt_rating(int evt_rating) {
        this.evt_rating = evt_rating;
    }

    public String getEvt_cat_name() {
        return evt_cat_name;
    }

    public void setEvt_cat_name(String evt_cat_name) {
        this.evt_cat_name = evt_cat_name;
    }
}
