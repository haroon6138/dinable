package com.dinable.dinable.classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class Database_Helper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Dinable_app";


    public Database_Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(For_addtocart_database_table.CREATE_TABLE);
        db.execSQL(For_addtocart_database_table.CREATE_TABLE_FOR_CHECKING);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + For_addtocart_database_table.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + For_addtocart_database_table.TABLE_NAME_For_Checking);

        // Create tables again
        onCreate(db);
    }

    //for carts insert
    public long insert_carts(int item_id, String item_count, String item_price, String item_name, String rest_id, String item_image) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(For_addtocart_database_table.COLUMN_item_count, item_count);
        values.put(For_addtocart_database_table.COLUMN_item_id, item_id);
        values.put(For_addtocart_database_table.COLUMN_Item_name, item_name);
        values.put(For_addtocart_database_table.COLUMN_Item_price, item_price);
        values.put(For_addtocart_database_table.COLUMN_item_image, item_image);
        values.put(For_addtocart_database_table.COLUMN_rest_id, rest_id);

        // insert row
        long id = db.insert(For_addtocart_database_table.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }


    //for restaurants checking
    public long insert_checking(int Restaurant_id_For_Checking) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(For_addtocart_database_table.COLUMN_Restaurant_id_For_Checking, Restaurant_id_For_Checking);

        // insert row
        long id = db.insert(For_addtocart_database_table.TABLE_NAME_For_Checking, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }


    public For_addtocart_database_table getcarts(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(For_addtocart_database_table.TABLE_NAME,
                new String[]{For_addtocart_database_table.COLUMN_ID, For_addtocart_database_table.COLUMN_item_count,
                        For_addtocart_database_table.COLUMN_item_id,For_addtocart_database_table.COLUMN_item_image
                ,For_addtocart_database_table.COLUMN_Item_name,For_addtocart_database_table.COLUMN_Item_price,For_addtocart_database_table.COLUMN_rest_id},
                For_addtocart_database_table.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        For_addtocart_database_table note = new For_addtocart_database_table(
                cursor.getInt(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_item_id)),
                cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_item_count)),
                cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_Item_price)),
                cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_Item_name)),
                cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_rest_id)),
                cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_item_image)));

        // close the db connection
        cursor.close();

        return note;
    }

    //for carts list retrive
    public List<For_addtocart_database_table> getAllcarts() {
        List<For_addtocart_database_table> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + For_addtocart_database_table.TABLE_NAME ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                For_addtocart_database_table for_addtocart_database_table_obj= new For_addtocart_database_table();
                for_addtocart_database_table_obj.setTable_row_id(cursor.getInt(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_ID)));
                for_addtocart_database_table_obj.setItem_count(cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_item_count)));
                for_addtocart_database_table_obj.setItem_id(cursor.getInt(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_item_id)));
                for_addtocart_database_table_obj.setItem_name(cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_Item_name)));
                for_addtocart_database_table_obj.setItem_image(cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_item_image)));
                for_addtocart_database_table_obj.setItem_price(cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_Item_price)));
                for_addtocart_database_table_obj.setRest_id(cursor.getString(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_rest_id)));

                notes.add(for_addtocart_database_table_obj);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }


    //for restaurant checking retrive
    public List<Integer> getAllcheckings() {
        List<Integer> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + For_addtocart_database_table.TABLE_NAME_For_Checking ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
//                For_addtocart_database_table for_addtocart_database_table_obj= new For_addtocart_database_table();
//                for_addtocart_database_table_obj.setTable_row_id_For_Checking(cursor.getInt(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_ID_For_Checking)));
//                for_addtocart_database_table_obj.setRestaurant_id_For_Checking(cursor.getInt(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_Restaurant_id_For_Checking)));

                notes.add(cursor.getInt(cursor.getColumnIndex(For_addtocart_database_table.COLUMN_Restaurant_id_For_Checking)));
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }


    //for counting carts
    public int getcartsCount() {
        String countQuery = "SELECT  * FROM " + For_addtocart_database_table.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }


    //for counting checkings
    public int getcheckingCount() {
        String countQuery = "SELECT  * FROM " + For_addtocart_database_table.TABLE_NAME_For_Checking;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    //deleting all carts
    public int deleteall() {
        String countQuery = "DELETE FROM " + For_addtocart_database_table.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    //deleting all checking datas
    public int deleteallchecking() {
        String countQuery = "DELETE FROM " + For_addtocart_database_table.TABLE_NAME_For_Checking;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public void deleteNote(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(For_addtocart_database_table.TABLE_NAME, For_addtocart_database_table.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public int updateNote(int id,int addvalue) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(For_addtocart_database_table.COLUMN_item_count, addvalue);

        // updating row
        return db.update(For_addtocart_database_table.TABLE_NAME, values, For_addtocart_database_table.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});
    }

}