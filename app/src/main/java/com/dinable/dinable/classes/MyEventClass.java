package com.dinable.dinable.classes;

public class MyEventClass
{
    String evt_usr_title, evt_usr_res_name, evt_usr_image, evt_usr_date, evt_usr_time, evt_usr_charges, evt_usr_status;

    public String getEvt_usr_title() {
        return evt_usr_title;
    }

    public void setEvt_usr_title(String evt_usr_title) {
        this.evt_usr_title = evt_usr_title;
    }

    public String getEvt_usr_res_name() {
        return evt_usr_res_name;
    }

    public void setEvt_usr_res_name(String evt_usr_res_name) {
        this.evt_usr_res_name = evt_usr_res_name;
    }

    public String getEvt_usr_image() {
        return evt_usr_image;
    }

    public void setEvt_usr_image(String evt_usr_image) {
        this.evt_usr_image = evt_usr_image;
    }

    public String getEvt_usr_date() {
        return evt_usr_date;
    }

    public void setEvt_usr_date(String evt_usr_date) {
        this.evt_usr_date = evt_usr_date;
    }

    public String getEvt_usr_time() {
        return evt_usr_time;
    }

    public void setEvt_usr_time(String evt_usr_time) {
        this.evt_usr_time = evt_usr_time;
    }

    public String getEvt_usr_charges() {
        return evt_usr_charges;
    }

    public void setEvt_usr_charges(String evt_usr_charges) {
        this.evt_usr_charges = evt_usr_charges;
    }

    public String getEvt_usr_status() {
        return evt_usr_status;
    }

    public void setEvt_usr_status(String evt_usr_status) {
        this.evt_usr_status = evt_usr_status;
    }
}
