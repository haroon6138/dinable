package com.dinable.dinable.classes;

import org.json.JSONArray;

public class Booking_status {

    String restaurant_name;
    String r_header_image;
    String theme;
    String no_of_people;
    String requested_time_from;
    String requested_time_to;
    String date_of_request;
    String approve_pending;
    String rejected;
    String rejecting_reason;
    String rest_id;
    String usr_id;
    JSONArray usersInvited;

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getR_header_image() {
        return r_header_image;
    }

    public void setR_header_image(String r_header_image) {
        this.r_header_image = r_header_image;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getNo_of_people() {
        return no_of_people;
    }

    public void setNo_of_people(String no_of_people) {
        this.no_of_people = no_of_people;
    }

    public String getRequested_time_from() {
        return requested_time_from;
    }

    public void setRequested_time_from(String requested_time_from) {
        this.requested_time_from = requested_time_from;
    }

    public String getRequested_time_to() {
        return requested_time_to;
    }

    public void setRequested_time_to(String requested_time_to) {
        this.requested_time_to = requested_time_to;
    }

    public String getDate_of_request() {
        return date_of_request;
    }

    public void setDate_of_request(String date_of_request) {
        this.date_of_request = date_of_request;
    }

    public String getApprove_pending() {
        return approve_pending;
    }

    public void setApprove_pending(String approve_pending) {
        this.approve_pending = approve_pending;
    }

    public String getRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    public String getRejecting_reason() {
        return rejecting_reason;
    }

    public void setRejecting_reason(String rejecting_reason) {
        this.rejecting_reason = rejecting_reason;
    }

    public String getRest_id() {
        return rest_id;
    }

    public void setRest_id(String rest_id) {
        this.rest_id = rest_id;
    }

    public String getUsr_id() {
        return usr_id;
    }

    public void setUsr_id(String usr_id) {
        this.usr_id = usr_id;
    }

    public JSONArray getUsersInvited() {
        return usersInvited;
    }

    public void setUsersInvited(JSONArray usersInvited) {
        this.usersInvited = usersInvited;
    }
}
