package com.dinable.dinable.classes;

public class slot {

    String time;
    String no_of_people_remaining;
    String booked;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNo_of_people_remaining() {
        return no_of_people_remaining;
    }

    public void setNo_of_people_remaining(String no_of_people_remaining) {
        this.no_of_people_remaining = no_of_people_remaining;
    }

    public String getBooked() {
        return booked;
    }

    public void setBooked(String booked) {
        this.booked = booked;
    }
}
