package com.dinable.dinable.classes;

public class For_addtocart_database_table {

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
    }

    public String getItem_price() {
        return Item_price;
    }

    public void setItem_price(String item_price) {
        Item_price = item_price;
    }

    public String getItem_name() {
        return Item_name;
    }

    public void setItem_name(String item_name) {
        Item_name = item_name;
    }

    public String getRest_id() {
        return rest_id;
    }

    public void setRest_id(String rest_id) {
        this.rest_id = rest_id;
    }

    public String getItem_image() {
        return item_image;
    }

    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }

    public int getTable_row_id() {
        return table_row_id;
    }

    public void setTable_row_id(int table_row_id) {
        this.table_row_id = table_row_id;
    }

    private int table_row_id;
    private int item_id;
    private String item_count;
    private String Item_price;
    private String Item_name;


    public int getRestaurant_id_For_Checking() {
        return Restaurant_id_For_Checking;
    }

    public void setRestaurant_id_For_Checking(int restaurant_id_For_Checking) {
        Restaurant_id_For_Checking = restaurant_id_For_Checking;
    }

    private int Restaurant_id_For_Checking;

    public int getTable_row_id_For_Checking() {
        return table_row_id_For_Checking;
    }

    public void setTable_row_id_For_Checking(int table_row_id_For_Checking) {
        this.table_row_id_For_Checking = table_row_id_For_Checking;
    }

    private int table_row_id_For_Checking;

    public For_addtocart_database_table(int item_id, String item_count, String item_price, String item_name, String rest_id, String item_image) {
        this.item_id = item_id;
        this.item_count = item_count;
        Item_price = item_price;
        Item_name = item_name;
        this.rest_id = rest_id;
        this.item_image = item_image;
    }

    private String rest_id;
    private String item_image;


   public For_addtocart_database_table(){
    }

    public For_addtocart_database_table(int Restaurant_id_For_Checking){
       this.Restaurant_id_For_Checking=Restaurant_id_For_Checking;
    }

    public static final String TABLE_NAME = "restaurants_carts";

   //this three lines for checking
    public static final String TABLE_NAME_For_Checking = "For_Checking";
    public static final String COLUMN_ID_For_Checking = "id";
    public static final String COLUMN_Restaurant_id_For_Checking = "Restaurant_checking";


    public static final String COLUMN_ID = "id";
    public static final String COLUMN_item_id = "item_id";
    public static final String COLUMN_item_count = "item_count";
    public static final String COLUMN_Item_price = "Item_price";
    public static final String COLUMN_Item_name = "Item_name";
    public static final String COLUMN_rest_id = "rest_id";
    public static final String COLUMN_item_image = "item_image";


    // Create table SQL query for carts
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_item_id + " INTEGER UNIQUE,"
                    + COLUMN_item_count + " TEXT,"
                    + COLUMN_Item_price + " TEXT,"
                    + COLUMN_Item_name + " TEXT,"
                    + COLUMN_rest_id + " TEXT,"
                    + COLUMN_item_image + " TEXT"
                    + ")";


    //create table for checking process
    public static final String CREATE_TABLE_FOR_CHECKING =
            "CREATE TABLE " + TABLE_NAME_For_Checking + "("
                    + COLUMN_ID_For_Checking + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_Restaurant_id_For_Checking + " INTEGER UNIQUE"
                    + ")";

}
