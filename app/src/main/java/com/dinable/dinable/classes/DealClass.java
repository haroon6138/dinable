package com.dinable.dinable.classes;

public class DealClass
{
    String deal_id;
    String deal_res_id;
    String deal_title;
    String deal_desc;
    String deal_image;

    public String getRest_name() {
        return rest_name;
    }

    public void setRest_name(String rest_name) {
        this.rest_name = rest_name;
    }

    public String getRest_image() {
        return rest_image;
    }

    public void setRest_image(String rest_image) {
        this.rest_image = rest_image;
    }

    public String getRest_self() {
        return rest_self;
    }

    public void setRest_self(String rest_self) {
        this.rest_self = rest_self;
    }

    String rest_name;
    String rest_image;
    String rest_self;
    String deal_price;
    int deal_rating;

    public int getDeal_rating() {
        return deal_rating;
    }

    public void setDeal_rating(int deal_rating) {
        this.deal_rating = deal_rating;
    }

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public String getDeal_res_id() {
        return deal_res_id;
    }

    public void setDeal_res_id(String deal_res_id) {
        this.deal_res_id = deal_res_id;
    }

    public String getDeal_title() {
        return deal_title;
    }

    public void setDeal_title(String deal_title) {
        this.deal_title = deal_title;
    }

    public String getDeal_desc() {
        return deal_desc;
    }

    public void setDeal_desc(String deal_desc) {
        this.deal_desc = deal_desc;
    }

    public String getDeal_image() {
        return deal_image;
    }

    public void setDeal_image(String deal_image) {
        this.deal_image = deal_image;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }
}
