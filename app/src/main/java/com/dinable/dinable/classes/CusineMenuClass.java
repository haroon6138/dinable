package com.dinable.dinable.classes;

public class CusineMenuClass
{
    private int cusine_menu_id;
    private String cusine_menu_name;
    private String cusine_menu_desc;
    private String cusine_menu_image;
    private String cusine_menu_price;
    private String cusine_menu_rating;

    public CusineMenuClass()
    {}

    public int getCusine_menu_id() {
        return cusine_menu_id;
    }

    public void setCusine_menu_id(int cusine_menu_id) {
        this.cusine_menu_id = cusine_menu_id;
    }

    public String getCusine_menu_name() {
        return cusine_menu_name;
    }

    public void setCusine_menu_name(String cusine_menu_name) {
        this.cusine_menu_name = cusine_menu_name;
    }

    public String getCusine_menu_desc() {
        return cusine_menu_desc;
    }

    public void setCusine_menu_desc(String cusine_menu_desc) {
        this.cusine_menu_desc = cusine_menu_desc;
    }

    public String getCusine_menu_image() {
        return cusine_menu_image;
    }

    public void setCusine_menu_image(String cusine_menu_image) {
        this.cusine_menu_image = cusine_menu_image;
    }

    public String getCusine_menu_price() {
        return cusine_menu_price;
    }

    public void setCusine_menu_price(String cusine_menu_price) {
        this.cusine_menu_price = cusine_menu_price;
    }

    public String getCusine_menu_rating() {
        return cusine_menu_rating;
    }

    public void setCusine_menu_rating(String cusine_menu_rating) {
        this.cusine_menu_rating = cusine_menu_rating;
    }
}
