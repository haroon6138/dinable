package com.dinable.dinable.classes;


import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONParser
{
    static List<restaurantsclass> myRestaurantsclassList;
    static List<cuisineclass> mycuisineclasseslist;
    static List<CusineMenuClass> cusineMenuClassList;
    static List<DealClass> dealClassList;
    static List<EventRestClass> eventRestClassList;
    static List<Booking_status> booking_statuses_list;
    static List<Order_class> order_classList;
    static List<order_menu_class> order_menu_classList;
    static List<MyEventClass> myEventClassList;
    static List<CusineForSearchClass> cusineForSearchClassList;
    static List<ThemeForSearchClass> themeForSearchClassList;
    static List<ReviewRestuarantClass> reviewRestuarantClassList;
    static List<Menu_reviews_class> menuReviewsClasseslist;
    static List<notifications_history_model> notificationsHistoryList;
    static List<friendsModel> friendsModelList;
    static List<userInvites> invitesList;
    static List<ThemeForSearchClass> themesSearchClassList;

    // Parse Resturant Data For App Home Page
    public static List<restaurantsclass> parse_restaurants(String content)
    {
        JSONArray jsonArray = null;
        restaurantsclass myRestaurantsclass = null;
        try
        {
            jsonArray = new JSONArray(content);
            myRestaurantsclassList = new ArrayList<>();
            for(int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                myRestaurantsclass = new restaurantsclass();

                JSONArray cuisinesArray = jsonObject.getJSONArray("Cuisines");
                String cuisinesListString = "";

                myRestaurantsclass.setRestaurant_id(jsonObject.getString("restaurant_id"));
                myRestaurantsclass.setRestaurant_name(jsonObject.getString("restaurant_name") + " - " + jsonObject.getString("restaurant_address"));
                myRestaurantsclass.setRestaurant_rating(jsonObject.getInt("restaurant_ratting"));
                myRestaurantsclass.setSelf_res(jsonObject.getString("self_or_not"));
                myRestaurantsclass.setRestaurant_image(jsonObject.getString("r_header_image"));
                myRestaurantsclass.setCost(jsonObject.getString("r_per_head_cost"));
                myRestaurantsclass.setDistance(jsonObject.getString("distance"));
                myRestaurantsclass.setRestaurantLat(Double.valueOf(jsonObject.getString("restaurant_Lat").trim()));
                myRestaurantsclass.setRestaurantLong(Double.valueOf(jsonObject.getString("restaurant_Long").trim()));
                myRestaurantsclass.setThemeAttractions(jsonObject.getString("restaurant_attractions"));

                for(int j = 0; j < cuisinesArray.length(); j++) {
                    JSONObject jsonObject1 = cuisinesArray.getJSONObject(j);
                    cuisinesListString += jsonObject1.getString("csn_nm");
                    if((cuisinesArray.length() - 1) != j)
                        cuisinesListString += " - ";
                }
                myRestaurantsclass.setCuisines(cuisinesListString);

                myRestaurantsclassList.add(myRestaurantsclass);
                cuisinesListString = "";
            }
            return myRestaurantsclassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //Parse Friends List Data
    public static List<friendsModel> parseSearchResponseData(String content)
    {
        JSONObject dataObj = null;
        JSONArray jsonArray = null;
        friendsModel friendsModel = null;
        try
        {
            dataObj = new JSONObject(content);
            friendsModelList = new ArrayList<>();
            jsonArray = dataObj.getJSONArray("result");
            Log.e("list", jsonArray.toString());
            for(int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                friendsModel = new friendsModel();

                friendsModel.setFriendId(jsonObject.getString("id"));
                friendsModel.setFriendName(jsonObject.getString("name"));
                friendsModel.setFriendEmail(jsonObject.getString("email"));
                friendsModel.setFriendImage(jsonObject.getString("image"));

                friendsModelList.add(friendsModel);
            }
            return friendsModelList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }

    }


    //Parse Friends List Data
    public static List<friendsModel> parseFriendsInviteData(String content)
    {
        JSONObject dataObj = null;
        JSONArray jsonArray = null;
        friendsModel friendsModel = null;
        try
        {
            dataObj = new JSONObject(content);
            friendsModelList = new ArrayList<>();

            if(dataObj.getBoolean("success")) {
                jsonArray = dataObj.getJSONArray("myFriends");
                Log.e("list", jsonArray.toString());
                for(int i = 0; i < jsonArray.length(); i++)
                {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    friendsModel = new friendsModel();

                    if(jsonObject.optString("status").equals("1")) {
                        friendsModel.setFriendId(jsonObject.getString("id"));
                        friendsModel.setFriendName(jsonObject.getString("name"));
                        friendsModel.setFriendEmail(jsonObject.getString("email"));
                        friendsModel.setFriendImage(jsonObject.getString("image"));
                        friendsModel.setFriendStatus(jsonObject.optString("status"));
                        friendsModel.setFriendFCM(jsonObject.getString("fcm"));

                        friendsModelList.add(friendsModel);
                    } else {
                    }
                }
            } else {

            }


            return friendsModelList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }

    }


    //Parse Friends List Data
    public static List<friendsModel> parseFriendsData(String content)
    {
        JSONObject dataObj = null;
        JSONArray jsonArray = null;
        friendsModel friendsModel = null;
        try
        {
            dataObj = new JSONObject(content);
            friendsModelList = new ArrayList<>();

            if(dataObj.getBoolean("success")) {
                jsonArray = dataObj.getJSONArray("myFriends");
                Log.e("list", jsonArray.toString());
                for(int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    friendsModel = new friendsModel();

                    friendsModel.setFriendId(jsonObject.getString("id"));
                    friendsModel.setFriendName(jsonObject.getString("name"));
                    friendsModel.setFriendEmail(jsonObject.getString("email"));
                    friendsModel.setFriendImage(jsonObject.getString("image"));
                    friendsModel.setFriendStatus(jsonObject.optString("status"));
                    friendsModel.setFriendFCM(jsonObject.getString("fcm"));

                    friendsModelList.add(friendsModel);
                }
            } else {

            }


            return friendsModelList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public static List<friendsModel> parseFriendsRequestData(String content)
    {
        JSONObject dataObj = null;
        JSONArray jsonArray = null;
        friendsModel friendsModel = null;
        try
        {
            dataObj = new JSONObject(content);
            friendsModelList = new ArrayList<>();
            if(dataObj.getBoolean("success")) {
                jsonArray = dataObj.getJSONArray("friendRequests");
                Log.e("list", jsonArray.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    friendsModel = new friendsModel();

                    friendsModel.setFriendId(jsonObject.getString("id"));
                    friendsModel.setFriendName(jsonObject.getString("name"));
                    friendsModel.setFriendEmail(jsonObject.getString("email"));
                    friendsModel.setFriendImage(jsonObject.getString("image"));
                    friendsModel.setFriendStatus(jsonObject.getString("status"));

                    friendsModelList.add(friendsModel);
                }
            }
            return friendsModelList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    //Parse Cusines for Resturant Page
    public static List<cuisineclass> parse_cuisine(String content)
    {
        JSONArray jsonArray = null;
        cuisineclass cuisineclass = null;
        try
        {
            jsonArray = new JSONArray(content);
            mycuisineclasseslist = new ArrayList<>();
            for(int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                cuisineclass = new cuisineclass();


                cuisineclass.setCuisine_id(jsonObject.getInt("csn_id"));
                cuisineclass.setCuisine_name(jsonObject.getString("csn_nm"));


                mycuisineclasseslist.add(cuisineclass);
            }
            return mycuisineclasseslist;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    //Parse Cusine Deatil menu For
    public static List<CusineMenuClass> parse_cusine_details(String content)
    {
        JSONArray jsonArray = null;
        CusineMenuClass cusineMenuClassobj = null;
        try
        {
            jsonArray = new JSONArray(content);
            cusineMenuClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++ )
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                cusineMenuClassobj = new CusineMenuClass();

                cusineMenuClassobj.setCusine_menu_id(jsonObject.getInt("mn_id"));
                cusineMenuClassobj.setCusine_menu_name(jsonObject.getString("mn_nm"));
                cusineMenuClassobj.setCusine_menu_desc(jsonObject.getString("mn_dsc"));
                cusineMenuClassobj.setCusine_menu_image(jsonObject.getString("mn_img"));
                cusineMenuClassobj.setCusine_menu_price(jsonObject.getString("mn_prc"));
                cusineMenuClassobj.setCusine_menu_rating(jsonObject.getString("mn_rtg"));

                cusineMenuClassList.add(cusineMenuClassobj);
            }
            return cusineMenuClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<notifications_history_model> parseNotificationsData(String content){
        JSONObject jsonObj = null;
        JSONArray jsonArray = null;
        notifications_history_model notificationsClassobj = null;

        try {
            jsonObj = new JSONObject(content);
            jsonArray = jsonObj.getJSONArray("result");
            notificationsHistoryList = new ArrayList<>();
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                notificationsClassobj = new notifications_history_model();

                notificationsClassobj.setNotification_Id(jsonObject.getString("Noti_id"));
                notificationsClassobj.setNotification_Title(jsonObject.getString("noti_title"));
                notificationsClassobj.setNotification_Description(jsonObject.getString("noti_desc"));
                notificationsClassobj.setNotification_DateTime(jsonObject.getString("noti_date_time"));

                notificationsHistoryList.add(notificationsClassobj);
            }
            return notificationsHistoryList;

        } catch (JSONException ex){
            ex.printStackTrace();
            return null;
        }
    }

    // Parse Deals Data
    public  static List<DealClass> parse_Deals(String content)
    {
        JSONArray jsonArray = null;
        DealClass dealClassobj = null;
        try
        {
            jsonArray = new JSONArray(content);
            dealClassList = new ArrayList<>();
            for (int i = 0; i<jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                dealClassobj = new DealClass();

                dealClassobj.setDeal_id(jsonObject.getString("dl_id"));
                dealClassobj.setDeal_res_id(jsonObject.getString("dl_rst_id"));
                dealClassobj.setDeal_title(jsonObject.getString("dl_ttl"));
                dealClassobj.setDeal_desc(jsonObject.getString("dl_dsc"));
                dealClassobj.setDeal_image(jsonObject.getString("dl_img"));
                dealClassobj.setDeal_price(jsonObject.getString("dl_prc"));
                dealClassobj.setDeal_rating(jsonObject.getInt("dl_rtg"));
                dealClassobj.setRest_name(jsonObject.getString("restaurant_name"));
                dealClassobj.setRest_image(jsonObject.getString("r_header_image"));
                dealClassobj.setRest_self(jsonObject.getString("self_or_not"));

                dealClassList.add(dealClassobj);
            }
            return dealClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //Parse the Restaurant Events Details
    public static List<EventRestClass> parse_rest_event(String content)
    {
        JSONArray jsonArray = null;
        EventRestClass eventRestClassobj = null;
        try
        {
            jsonArray = new JSONArray(content);
            eventRestClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                eventRestClassobj = new EventRestClass();

                eventRestClassobj.setEvt_id(jsonObject.getString("evt_id"));
                eventRestClassobj.setEvt_rest_id(jsonObject.getString("evt_rst_id"));
                eventRestClassobj.setEvt_rating(jsonObject.getInt("evt_rtg"));
                eventRestClassobj.setEvt_title(jsonObject.getString("evt_ttl"));
                eventRestClassobj.setEvt_desc(jsonObject.getString("description"));
                eventRestClassobj.setEvt_image(jsonObject.getString("title_image"));
                eventRestClassobj.setEvt_date(jsonObject.getString("date"));
                eventRestClassobj.setEvt_time_from(jsonObject.getString("time_from"));
                eventRestClassobj.setEvt_time_to(jsonObject.getString("time_to"));
                eventRestClassobj.setEvt_per_head_charges(jsonObject.getString("per_head_charges"));
                eventRestClassobj.setEvt_cat_name(jsonObject.getString("evt_cat_nm"));

                eventRestClassList.add(eventRestClassobj);
            }
            return eventRestClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //Parse the Restaurant Events Details
    public static List<ThemeForSearchClass> parse_theme_data(String content)
    {
        JSONArray jsonArray = null;
        ThemeForSearchClass eventRestClassobj = null;
        try
        {
            jsonArray = new JSONArray(content);
            themesSearchClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                eventRestClassobj = new ThemeForSearchClass();

                eventRestClassobj.setTheme_id(jsonObject.getString("id"));
                eventRestClassobj.setTheme_name(jsonObject.getString("Name"));
                eventRestClassobj.setTheme_image(jsonObject.getString("Image"));

                themesSearchClassList.add(eventRestClassobj);
            }
            return themesSearchClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static ArrayList<String> parse_theme(String content)
    {
        JSONArray jsonArray = null;
        ArrayList<String> stringArrayList;
        try
        {
            jsonArray = new JSONArray(content);
            stringArrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String theme_res =jsonObject.getString("Name");
                stringArrayList.add(theme_res);
            }
            return stringArrayList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public static List<CusineForSearchClass> parse_cusine_For_search(String content)
    {
        JSONArray jsonArray = null;
        CusineForSearchClass cusineForSearchClass = null;
        try
        {
            jsonArray = new JSONArray(content);
            cusineForSearchClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                cusineForSearchClass = new CusineForSearchClass();

                cusineForSearchClass.setC_id(jsonObject.getString("csn_id"));
                cusineForSearchClass.setC_name(jsonObject.getString("csn_nm"));

                cusineForSearchClassList.add(cusineForSearchClass);
            }
            return cusineForSearchClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<ThemeForSearchClass> parse_theme_For_search(String content)
    {
        JSONArray jsonArray = null;
        ThemeForSearchClass themeForSearchClass = null;
        try
        {
            jsonArray = new JSONArray(content);
            themeForSearchClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                themeForSearchClass = new ThemeForSearchClass();

                themeForSearchClass.setTheme_id(jsonObject.getString("id"));
                themeForSearchClass.setTheme_name(jsonObject.getString("Name"));

                themeForSearchClassList.add(themeForSearchClass);
            }
            return themeForSearchClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<userInvites> parseInviteData(JSONArray content) {
        JSONArray jsonArray = null;
        userInvites userInvites=null;
        try
        {
            jsonArray = content;
            invitesList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                Log.e("obj", jsonObject1.toString());
                userInvites=new userInvites();

                userInvites.setBookingId(jsonObject1.getString("b_id"));
                userInvites.setBookingTheme(jsonObject1.getString("theme"));
                userInvites.setDate_of_request(jsonObject1.getString("req_date"));
                userInvites.setInvitedByName(jsonObject1.getString("name"));
                userInvites.setInviterId(jsonObject1.getString("id"));
                userInvites.setNo_of_people(jsonObject1.getString("people"));
                userInvites.setR_header_image(jsonObject1.getString("rest_image"));
                userInvites.setRequested_time_to(jsonObject1.getString("req_time"));
                userInvites.setRestaurant_name(jsonObject1.getString("rest_name"));

                invitesList.add(userInvites);
            }
            return invitesList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<Booking_status> parse_booking_status(String content)
    {
        JSONArray jsonArray = null;
        Booking_status booking_status=null;
        try
        {
            jsonArray = new JSONArray(content);
            booking_statuses_list = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONObject jsonObject1 = jsonObject.getJSONObject("booking");
                Log.e("obj", jsonObject1.toString());
                booking_status=new Booking_status();
                booking_status.setRestaurant_name(jsonObject1.getString("restaurant_name"));
                booking_status.setR_header_image(jsonObject1.getString("r_header_image"));
                booking_status.setRejecting_reason(jsonObject1.getString("rejecting_reason"));
                booking_status.setDate_of_request(jsonObject1.getString("date_of_request"));
                booking_status.setRequested_time_to(jsonObject1.getString("requested_time_to"));
                booking_status.setRequested_time_from(jsonObject1.getString("requested_time_from"));
                booking_status.setTheme(jsonObject1.getString("theme"));
                booking_status.setRejected(jsonObject1.getString("rejected"));
                booking_status.setApprove_pending(jsonObject1.getString("approve_pending"));
                booking_status.setNo_of_people(jsonObject1.getString("no_of_people"));
                booking_status.setUsr_id(jsonObject1.getString("usr_id"));
                booking_status.setRest_id(jsonObject1.getString("rest_id"));
                booking_status.setUsersInvited(jsonObject.getJSONArray("invites"));

                booking_statuses_list.add(booking_status);
            }
            return booking_statuses_list;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<Booking_status> parse_rest_booking_status(String content)
    {
        JSONArray jsonArray = null;
        Booking_status booking_status=null;
        try
        {
            jsonArray = new JSONArray(content);
            booking_statuses_list = new ArrayList<>();
            for (int i = 0; i < 1; i++)
            {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                Log.e("obj", jsonObject1.toString());
                booking_status=new Booking_status();
                booking_status.setRestaurant_name(jsonObject1.getString("restaurant_name"));
                booking_status.setR_header_image(jsonObject1.getString("r_header_image"));
                booking_status.setRejecting_reason(jsonObject1.getString("rejecting_reason"));
                booking_status.setDate_of_request(jsonObject1.getString("date_of_request"));
                booking_status.setRequested_time_to(jsonObject1.getString("requested_time_to"));
                booking_status.setRequested_time_from(jsonObject1.getString("requested_time_from"));
                booking_status.setTheme(jsonObject1.getString("theme"));
                booking_status.setRejected(jsonObject1.getString("rejected"));
                booking_status.setApprove_pending(jsonObject1.getString("approve_pending"));
                booking_status.setNo_of_people(jsonObject1.getString("no_of_people"));
                booking_status.setUsr_id(jsonObject1.getString("usr_id"));
                booking_status.setRest_id(jsonObject1.getString("rest_id"));

                booking_statuses_list.add(booking_status);
            }
            return booking_statuses_list;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }


        //for checking not delete it
//    public static List<Booking_status> parse_all_rest_booking_status(String content)
//    {
//        JSONArray jsonArray = null;
//        Booking_status booking_status=null;
//        try
//        {
//            jsonArray = new JSONArray(content);
//            booking_statuses_list = new ArrayList<>();
//            for (int i = 0; i < jsonArray.length(); i++)
//            {
//                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                booking_status=new Booking_status();
//                booking_status.setB_id(jsonObject.getInt("b_id"));
//                booking_status.setRest_id(jsonObject.getInt("rest_id"));
//                booking_status.setUsr_id(jsonObject.getInt("usr_id"));
//                booking_status.setNo_of_people(jsonObject.getInt("no_of_people"));
//                booking_status.setEvent_id(jsonObject.getInt("event_id"));
//                booking_status.setApprove_pending(jsonObject.getInt("approve_pending"));
//                booking_status.setRejected(jsonObject.getInt("rejected"));
//                booking_status.setTheme(jsonObject.getString("theme"));
//                booking_status.setDetails(jsonObject.getString("details"));
//                booking_status.setRequested_date(jsonObject.getString("requested_date"));
//                booking_status.setRequested_time_from(jsonObject.getString("requested_time_from"));
//                booking_status.setRequested_time_to(jsonObject.getString("requested_time_to"));
//                booking_status.setDate_of_request(jsonObject.getString("date_of_request"));
//                booking_status.setRejecting_reason(jsonObject.getString("rejecting_reason"));
//
//                booking_statuses_list.add(booking_status);
//            }
//            return booking_statuses_list;
//        }
//        catch (JSONException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }

    public static List<Order_class> parse_Orders(String content)
    {
        JSONArray jsonArray = null;
        Order_class order_class=null;
        try
        {
            jsonArray = new JSONArray(content);
            order_classList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                order_class=new Order_class();
                order_class.setO_id(jsonObject.getString("o_id"));
                order_class.setO_number(jsonObject.getString("o_number"));
                order_class.setO_date_time(jsonObject.getString("o_date_time"));
                order_class.setTotal_price(jsonObject.getString("total_price"));
                order_class.setRestaurant_name(jsonObject.getString("restaurant_name"));
                order_class.setSelf_or_not(jsonObject.getString("self_or_not"));
                order_class.setO_status(jsonObject.getString("o_status"));
                order_class.setRest_id(jsonObject.getString("restaurant_id"));
                order_class.setRest_image(jsonObject.getString("r_header_image"));
                order_class.setInvoice(jsonObject.getString("invoice_status"));
                order_class.setRest_PlayerId(jsonObject.getString("Player_Id"));
                order_class.setOrderType(jsonObject.getString("OrderType"));
                order_class.setRest_image(jsonObject.getString("r_header_image"));

                order_classList.add(order_class);
            }
            return order_classList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<order_menu_class> parse_Orders_menu(String content)
    {
        JSONArray jsonArray = null;
        order_menu_class order_menu_class=null;
        try
        {
            jsonArray = new JSONArray(content);
            order_menu_classList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                order_menu_class=new order_menu_class();
                order_menu_class.setMenu_id(jsonObject.getString("menu_id"));
                order_menu_class.setMenu_qty(jsonObject.getString("menu_qty"));
                order_menu_class.setTotal_price(jsonObject.getString("total_price"));
                order_menu_class.setMn_nm(jsonObject.getString("mn_nm"));
                order_menu_class.setMn_image(jsonObject.getString("mn_img"));
                order_menu_class.setMn_rating(jsonObject.getInt("mn_rtg"));

                order_menu_classList.add(order_menu_class);
            }
            return order_menu_classList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<MyEventClass> parse_my_events(String content)
    {
        JSONArray jsonArray = null;
        MyEventClass myEventClass =null;
        try
        {
            jsonArray = new JSONArray(content);
            myEventClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                myEventClass = new MyEventClass();

                myEventClass.setEvt_usr_title(jsonObject.getString("evt_ttl"));
                myEventClass.setEvt_usr_res_name(jsonObject.getString("restaurant_name"));
                myEventClass.setEvt_usr_date(jsonObject.getString("date"));
                myEventClass.setEvt_usr_time(jsonObject.getString("ev_time"));
                myEventClass.setEvt_usr_charges(jsonObject.getString("ev_charges"));
                myEventClass.setEvt_usr_status(jsonObject.getString("ev_status"));
                myEventClass.setEvt_usr_image(jsonObject.getString("title_image"));

                myEventClassList.add(myEventClass);
            }
            return myEventClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<ReviewRestuarantClass> parse_res_review(String content)
    {
        JSONArray jsonArray = null;
        ReviewRestuarantClass reviewRestuarantClass =null;
        try
        {
            jsonArray = new JSONArray(content);
            reviewRestuarantClassList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                reviewRestuarantClass = new ReviewRestuarantClass();

                reviewRestuarantClass.setRv_u_name(jsonObject.getString("usr_nm"));
                reviewRestuarantClass.setRv_u_image(jsonObject.getString("u_image"));
                reviewRestuarantClass.setRv_u_review(jsonObject.getString("review"));
                reviewRestuarantClass.setRv_u_rating(jsonObject.getInt("rating"));

                reviewRestuarantClassList.add(reviewRestuarantClass);
            }
            return reviewRestuarantClassList;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<Menu_reviews_class> parse_menu_review(String content)
    {
        JSONArray jsonArray = null;
        Menu_reviews_class menu_reviews_class =null;
        try
        {
            jsonArray = new JSONArray(content);
            menuReviewsClasseslist = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                menu_reviews_class = new Menu_reviews_class();

                menu_reviews_class.setUsr_nm(jsonObject.getString("usr_nm"));
                menu_reviews_class.setU_image(jsonObject.getString("u_image"));
                menu_reviews_class.setReview(jsonObject.getString("review"));
                menu_reviews_class.setRating(jsonObject.getInt("rating"));

                menuReviewsClasseslist.add(menu_reviews_class);
            }
            return menuReviewsClasseslist;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
