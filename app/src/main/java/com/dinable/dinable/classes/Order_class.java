package com.dinable.dinable.classes;

public class Order_class {
    private String o_id;
    private String o_number;
    private String o_date_time;
    private String total_price;
    private String restaurant_name;
    private String self_or_not;
    private String o_status;
    private String rest_id;
    private String invoice;
    private String rest_image;
    private String rest_PlayerId;
    private String orderType;
    private String restImage;

    public Order_class() { }

    public String getRest_id() {
        return rest_id;
    }

    public void setRest_id(String rest_id) {
        this.rest_id = rest_id;
    }

    public String getO_id() {
        return o_id;
    }

    public void setO_id(String o_id) {
        this.o_id = o_id;
    }

    public String getO_number() {
        return o_number;
    }

    public void setO_number(String o_number) {
        this.o_number = o_number;
    }

    public String getO_date_time() {
        return o_date_time;
    }

    public void setO_date_time(String o_date_time) {
        this.o_date_time = o_date_time;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getSelf_or_not() {
        return self_or_not;
    }

    public void setSelf_or_not(String self_or_not) {
        this.self_or_not = self_or_not;
    }

    public String getO_status() {
        return o_status;
    }

    public void setO_status(String o_status) {
        this.o_status = o_status;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getRest_image() {
        return rest_image;
    }

    public void setRest_image(String rest_image) {
        this.rest_image = rest_image;
    }

    public String getRest_PlayerId() {
        return rest_PlayerId;
    }

    public void setRest_PlayerId(String rest_PlayerId) {
        this.rest_PlayerId = rest_PlayerId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getRestImage() {
        return restImage;
    }

    public void setRestImage(String restImage) {
        this.restImage = restImage;
    }
}
