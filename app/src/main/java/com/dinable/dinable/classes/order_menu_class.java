package com.dinable.dinable.classes;

public class order_menu_class {
    String menu_id;
    String menu_qty;

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_qty() {
        return menu_qty;
    }

    public void setMenu_qty(String menu_qty) {
        this.menu_qty = menu_qty;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getMn_nm() {
        return mn_nm;
    }

    public void setMn_nm(String mn_nm) {
        this.mn_nm = mn_nm;
    }

    String total_price;
    String mn_nm;
    String mn_image;

    public String getMn_image() {
        return mn_image;
    }

    public void setMn_image(String mn_image) {
        this.mn_image = mn_image;
    }


    public int getMn_rating() {
        return mn_rating;
    }

    public void setMn_rating(int mn_rating) {
        this.mn_rating = mn_rating;
    }

    int mn_rating;

    public order_menu_class() {
    }
}
