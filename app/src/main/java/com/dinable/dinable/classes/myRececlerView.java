package com.dinable.dinable.classes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dinable.dinable.R;

public class myRececlerView extends RecyclerView.ViewHolder {

    public TextView textUser, textMessage, textDateTime;
    public RelativeLayout relativeLayout;

    public myRececlerView(View itemView) {
        super(itemView);

        textUser = itemView.findViewById(R.id.message_user);
        textMessage = itemView.findViewById(R.id.message_text);
        textDateTime = itemView.findViewById(R.id.message_time);
        relativeLayout = itemView.findViewById(R.id.viewChild);

    }
}
