package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.restaurantsadapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.restaurantsclass;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class search_result_activity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<restaurantsclass> restaurantsclassList;
    private com.dinable.dinable.Adapter.restaurantsadapter restaurantsadapter;
    double lati, longi;

    private String keyword_search, cusine_id, theme_id, dolr, rating = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search Results");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lati = Prefs.getDouble("lat", 0.0d);
        longi = Prefs.getDouble("long", 0.0d);

        keyword_search = getIntent().getStringExtra("keyword_search");
        cusine_id = getIntent().getStringExtra("cusine_id");
        theme_id = getIntent().getStringExtra("theme_id");
        rating = getIntent().getStringExtra("ratting");
        dolr = getIntent().getStringExtra("dollar");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        if (Utils.isOnline(search_result_activity.this)) {
            try {
                requestDatas(Endpoints.ip_server, keyword_search, cusine_id, theme_id);
                //Toast.makeText(search_result_activity.this,"Dollar "+dolr + " Rating "+rating+" Keyword "+keyword_search+" Cusine ID "+ cusine_id+"Theme ID"+ theme_id,Toast.LENGTH_LONG).show();

            } catch (Exception ex) {
                new SweetAlertDialog(search_result_activity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Some thing went wrong!")
                        .show();
            }

        } else {
            new SweetAlertDialog(search_result_activity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }


    public void requestDatas(String uri, final String keyword_search, final String cusine_id, final String theme_id) {
        final String id = Prefs.getString("user_id", "0");
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);

                restaurantsclassList = JSONParser.parse_restaurants(response);
                restaurantsadapter = new restaurantsadapter(search_result_activity.this, restaurantsclassList, false);
                recyclerView.setAdapter(restaurantsadapter);
                LinearLayoutManager llm = new LinearLayoutManager(search_result_activity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(search_result_activity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "search_filter_date");
                params.put("keyword_search", keyword_search);
                params.put("cs_id", cusine_id);
                params.put("th_id", theme_id);
                params.put("ratting", rating);
                params.put("dollar", dolr);
                params.put("lat", lati + "");
                params.put("long", longi + "");
                Log.e("search Params", params.toString());
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
    }
}
