package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Contact_us extends AppCompatActivity {
    private EditText nametxt,emailtxt,phonetxt,msgtxt;
    private Button btn_submit;
    private SweetAlertDialog pd;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDateandTime = sdf.format(new Date());
    List<String> for_subjects;
    Spinner spinner_subject;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        getWindow().setBackgroundDrawableResource(R.drawable.contactus1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Contact Us");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gettingvalues();

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isOnline(Contact_us.this))
                {
                    if(nametxt.getText().toString().isEmpty() || phonetxt.getText().toString().isEmpty() || msgtxt.getText().toString().isEmpty())
                    {
                        new SweetAlertDialog(Contact_us.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Fields are Required!")
                                .show();

                    }
                    else
                    {
                        //call the method
                        contact_form();
                    }
                }
                else
                {
                    new SweetAlertDialog(Contact_us.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Internet Not Found!")
                            .show();
                }
            }
        });
    }


    private void gettingvalues(){
        for_subjects=new ArrayList<>();
        for_subjects.add("Choose the Subject");
        for_subjects.add("feedback");
        for_subjects.add("complains");
        for_subjects.add("call back request");
        for_subjects.add("Event request");
        for_subjects.add("others");

        spinner_subject=findViewById(R.id.subject);
        spinner_subject.setAdapter(new ArrayAdapter<>(Contact_us.this, android.R.layout.simple_spinner_dropdown_item, for_subjects));
        spinner_subject.setSelection(0);


        nametxt=findViewById(R.id.nametxt);
        emailtxt=findViewById(R.id.emailtxt);
        phonetxt=findViewById(R.id.phonetxt);
        msgtxt=findViewById(R.id.msgtxt);
        btn_submit=findViewById(R.id.btn_submit);
    }

    private void contact_form()
    {
        final String u_email = Prefs.getString("user_email", "xyz@gmail.com");

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key","contact_us_email");
        params.put("u_email",u_email);
        params.put("subject",spinner_subject.getSelectedItem().toString());
        params.put("message",msgtxt.getText().toString());
        params.put("name",nametxt.getText().toString());
        params.put("phone",phonetxt.getText().toString());
        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onStart()
            {
                super.onStart();
                pd = Utils.showProgress(Contact_us.this);
                pd.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(Contact_us.this, "Response is null", Toast.LENGTH_SHORT).show();
                }else {

                    try {
                        JSONObject object  = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if(object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(Contact_us.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good Job!")
                                    .setContentText(object.getString("message"))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            Animation.split(Contact_us.this);
                                            finish();
                                        }
                                    })
                                    .show();

                        }else {
                            new SweetAlertDialog(Contact_us.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response",response);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String  response  = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(Contact_us.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                }else {

                    Log.d("response",response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }



    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
    }
}
