package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventsDetailActivity extends AppCompatActivity {

    ImageView imageView;
    TextView tv_title, tv_desc, tv_date, tv_time_from, tv_time_to, tv_per_head, tv_evt_cat;
    RatingBar ratingBar;
    Button join_btn;
    private SweetAlertDialog pd;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Event Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageView = findViewById(R.id.evt_image);
        tv_title = findViewById(R.id.evt_title);
        ratingBar = findViewById(R.id.evt_rating);
        tv_desc = findViewById(R.id.evt_desc);
        tv_date = findViewById(R.id.evt_date);
        tv_time_from = findViewById(R.id.evt_time_from);
        tv_time_to = findViewById(R.id.evt_time_to);
        tv_per_head = findViewById(R.id.evt_per_head);
        tv_evt_cat = findViewById(R.id.evt_category);
        join_btn = findViewById(R.id.btn_join_evt);

        Glide.with(EventsDetailActivity.this)
                .load(getIntent().getStringExtra("evt_image"))
                .into(imageView);
        tv_title.setText(getIntent().getStringExtra("evt_name"));
        tv_desc.setText(getIntent().getStringExtra("evt_desc"));
        tv_evt_cat.setText("Category: " + getIntent().getStringExtra("evt_category"));
        tv_date.setText("Date: " + getIntent().getStringExtra("evt_date"));
        tv_time_from.setText("Time From: " + getIntent().getStringExtra("evt_time_from"));
        tv_time_to.setText("Time To: " + getIntent().getStringExtra("evt_time_to"));
        tv_per_head.setText("Per Head Charges: Rs." + getIntent().getStringExtra("evt_per_head"));
        ratingBar.setRating(getIntent().getIntExtra("evt_rating", 1));

        join_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isOnline(EventsDetailActivity.this)) {
                    join_event();
                } else {
                    new SweetAlertDialog(EventsDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Internet Not Found!")
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(EventsDetailActivity.this);
    }

    // Declear the Join Event Function
    private void join_event() {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "join_evt_res");
        params.put("usr_id", id);
        params.put("evt_id", getIntent().getStringExtra("evt_id"));
        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(EventsDetailActivity.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(EventsDetailActivity.this, "Error occurred while communicating with the server.", Toast.LENGTH_LONG).show();
                } else {

                    try {
                        JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(EventsDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Your request to join the event has been sent, please be patient while the request is reviewed.")
                                    .show();
                        } else {
                            new SweetAlertDialog(EventsDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure!")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(EventsDetailActivity.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }
}
