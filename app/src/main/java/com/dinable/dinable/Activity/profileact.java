package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.FCM.SharedPrefManager;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class profileact extends AppCompatActivity {

    EditText name,email,phone,pass;
    CircularImageView imageView;
    private String photoPath = "";
    Button update_btn, logout_btn;
    private SweetAlertDialog pd;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profileact);

        getWindow().setBackgroundDrawableResource(R.drawable.back2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //take values from textbox
        name = findViewById(R.id.nametxt);
        email = findViewById(R.id.emailtxt);
        phone = findViewById(R.id.phonetxt);
        pass = findViewById(R.id.passtxt);
        imageView = findViewById(R.id.avatar);
        update_btn = findViewById(R.id.btn_update);
        logout_btn = findViewById(R.id.btn_logout);

        name.setText(Prefs.getString("user_name","User"));
        email.setText(Prefs.getString("user_email","abc@gmail.com"));
        phone.setText(Prefs.getString("user_phone","0123-456789"));
        pass.setText(Prefs.getString("user_password","**********"));

        // Set Email Eidt Text Read Only so that user can only read email
        email.setEnabled(false);

        //Set user image im image view if exist
        if(Prefs.getString("user_image","").isEmpty()){
            Picasso.get().load(R.drawable.ic_person_black_24dp).into(imageView);
        }else{
            Picasso.get().load(Prefs.getString("user_image","")).into(imageView);
        }
//        Glide.with(profileact.this)
//                .load(Prefs.getString("user_image","image"))
//                .into(imageView);



        //set on Click Listner on User Image View
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(profileact.this) // Activity or Fragment
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Select Image") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .single() // single mode
                        .start();
            }
        });

        //Set OnClickListener on logout button
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(profileact.this, MainActivity.class);
                Prefs.putBoolean("loginSuccess",false);
                SharedPrefManager.getInstance(profileact.this).logoutToken();
                startActivity(i);
                Toasty.success(profileact.this, "Successfully Logout", Toast.LENGTH_SHORT).show();
                //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                Animation.fade(profileact.this);
            }
        });

        //Set On Click Listner on update Button
        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isOnline(profileact.this))
                {
                    if(name.getText().toString().isEmpty() || email.getText().toString().isEmpty() || phone.getText().toString().isEmpty() || pass.getText().toString().isEmpty())
                    {
                        new SweetAlertDialog(profileact.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Fields are Required!")
                                .show();

                    }
                    else
                    {
                        // Initialize the update methode
                        update(name.getText().toString(),phone.getText().toString(),pass.getText().toString());

                        // update Prefs Values
                        Prefs.putString("user_name",name.getText().toString());
                        Prefs.putString("user_phone",phone.getText().toString());
                    }
                }
                else
                {
                    new SweetAlertDialog(profileact.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Internet Not Found!")
                            .show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ImagePicker.shouldHandle(requestCode, resultCode, data))
        {
            Image image = ImagePicker.getFirstImageOrNull(data);
            photoPath = image.getPath();
            Bitmap bmImg = BitmapFactory.decodeFile(photoPath);
            imageView.setImageBitmap(bmImg);
            //name.setText(photoPath);
        }

    }

    // Declear the update Function
    private void update(String name,String phone, String pass)
    {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key","update_user_detail");
        params.put("u_id",id);
        params.put("u_name",name);
        params.put("u_pass",pass);
        params.put("u_phone",phone);
        try {
            params.put("u_image",new File(photoPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onStart()
            {
                super.onStart();
                pd = Utils.showProgress(profileact.this);
                pd.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(profileact.this, "Response is null", Toast.LENGTH_SHORT).show();
                }else {

                    try {
                        JSONObject object  = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if(object.getBoolean("success")) {
                            Toasty.success(profileact.this,object.getString("message"),Toast.LENGTH_LONG).show();
                        }else {
                            new SweetAlertDialog(profileact.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response",response);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String  response  = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(profileact.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                }else {

                    Log.d("response",response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(profileact.this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
