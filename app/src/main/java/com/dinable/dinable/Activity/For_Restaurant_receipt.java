package com.dinable.dinable.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.For_addtocart_database_table;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.stream.Collectors;
import java.util.zip.Inflater;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class For_Restaurant_receipt extends AppCompatActivity implements RatingDialogListener {

    private List<For_addtocart_database_table> for_add;
    private Database_Helper database_helper;
    private List<Integer> addprices, menu_ids, menu_count, mn_total;
    private TextView addpr, rest, datetime;
    int total;
    //Date date;
    private SweetAlertDialog pd;
    Random random = new Random();

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDateandTime = sdf.format(new Date());
    String yes, comments, dropLatLong = "", dropLocationName = "";

    private base_adaptor baseAdaptor;
    private ListView listView;

    public For_Restaurant_receipt() throws ParseException {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for__restaurant_receipt);
        //database sqlite
        database_helper = new Database_Helper(this);
        for_add = database_helper.getAllcarts();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("rest_name"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //comments
        if (getIntent().getStringExtra("order_comment").isEmpty()) {
            comments = "no value coming";
        } else {
            comments = getIntent().getStringExtra("order_comment");
        }

        //DropLatLong
        if (getIntent().hasExtra("dropLatLong"))
            dropLatLong = getIntent().getStringExtra("dropLatLong");

        //DropLocationName
        if (getIntent().hasExtra("dropLocationName"))
            dropLocationName = getIntent().getStringExtra("dropLocationName");


        rest = (TextView) findViewById(R.id.restaurant_name);
        rest.setText(getIntent().getStringExtra("rest_name"));
        addpr = (TextView) findViewById(R.id.totalprice);

        datetime = (TextView) findViewById(R.id.datetime);
        //date=new Date();
        datetime.setText(currentDateandTime);

        addprices = new ArrayList<>();
        menu_ids = new ArrayList<>();
        menu_count = new ArrayList<>();
        mn_total = new ArrayList<>();
        //View linearLayout =  findViewById(R.id.receipts);
        //LinearLayout layout = (LinearLayout) findViewById(R.id.info);

        listView = findViewById(R.id.recipt_list);
        baseAdaptor = new base_adaptor(this, for_add);
        listView.setAdapter(baseAdaptor);


        for (int i = 0; i < for_add.size(); i++) {
            menu_ids.add(for_add.get(i).getItem_id());
            menu_count.add(Integer.parseInt(for_add.get(i).getItem_count()));
//            TextView valueTV = new TextView(this);
//            valueTV.setText(for_add.get(i).getItem_name()+"                    "+for_add.get(i).getItem_price()+"X"+for_add.get(i).getItem_count()+"                    "+Integer.parseInt(for_add.get(i).getItem_price())*Integer.parseInt(for_add.get(i).getItem_count()));


            mn_total.add(Integer.parseInt(for_add.get(i).getItem_price()) * Integer.parseInt(for_add.get(i).getItem_count()));
            addprices.add(Integer.parseInt(for_add.get(i).getItem_price()) * Integer.parseInt(for_add.get(i).getItem_count()));
//            valueTV.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT));
//
//            ((LinearLayout) linearLayout).addView(valueTV);
        }

        if (addprices.size() > 0) {
            for (int i = 0; i < addprices.size(); i++) {
                total += addprices.get(i);
            }
            addpr.setText("RS. " + String.valueOf(total) + "/=");
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.forcheckout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.order) {
            if (for_add.size() > 0) {
                //book_restaurant(for_add.get(0).getRest_id(),String.valueOf(random.nextInt(3757388)),menu_ids,menu_count,for_add.size(),total,mn_total);
                if (getIntent().getStringExtra("rest_type").equals("self")) {
                    book_restaurant(for_add.get(0).getRest_id(), String.valueOf(random.nextInt(3757388)), menu_ids, menu_count, for_add.size(), total, mn_total, comments);
                } else if (getIntent().getStringExtra("rest_type").equals("booking")) {
                    if (Prefs.getString("order_num", "null").equals("null")) {
                        book_restaurant(for_add.get(0).getRest_id(), String.valueOf(random.nextInt(3757388)), menu_ids, menu_count, for_add.size(), total, mn_total, comments);
                    } else {
                        String o_id = Prefs.getString("order_num", "");
                        book_restaurant_addmore(o_id, menu_ids, menu_count, for_add.size(), mn_total, total, comments);
                        //Toast.makeText(For_Restaurant_receipt.this,menu_ids.get(1)+" "+menu_count.get(1)+" "+mn_total.get(1),Toast.LENGTH_SHORT).show();
                        //new checking clear prefs    or on checkout of booking rest all clear
                    }
                }
            }
        }


        return super.onOptionsItemSelected(item);
    }


    //sending order notes
    private void book_restaurant(final String rest_id, String o_num, List<Integer> menu_id, List<Integer> menu_coun, int size, int total, List<Integer> mntotal, String comment) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "cus_order");
        params.put("usr_id", id);
        params.put("res_id", rest_id);
        params.put("o_num", o_num);
        params.put("menu_id", menu_id);
        params.put("menu_qty", menu_coun);
        params.put("total", size);
        params.put("total_price", total);
        params.put("mn_total_price", mntotal);
        params.put("o_date_time", currentDateandTime);
        params.put("additional_coment", comment);

        params.put("o_type", Prefs.getString("orderType", "Dine-In"));
        params.put("dropLatLong", dropLatLong);
        params.put("dropAddress", dropLocationName);

        Log.e("Params", params.toString());

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(For_Restaurant_receipt.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Order successful. Please be patient while it is being reviewed.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            //here checking resturant is self or booking
                                            if (getIntent().getStringExtra("rest_type").equals("self")) {
                                                try {
                                                    requestDatatocheckout(Endpoints.ip_server, object.getString("last_id"));
                                                    if (database_helper.getcheckingCount() > 0) {
                                                        if (database_helper.getcartsCount() > 0) {
                                                            database_helper.deleteall();
                                                            database_helper.deleteallchecking();
                                                        }
                                                    }
                                                } catch (Exception e) {

                                                }

                                            } else if (getIntent().getStringExtra("rest_type").equals("booking")) {
                                                if (database_helper.getcheckingCount() > 0) {
                                                    if (database_helper.getcartsCount() > 0) {
                                                        database_helper.deleteall();
                                                        try {
                                                            Prefs.putString("order_num", object.getString("last_id"));
                                                            Prefs.putString("order_rest_id", rest_id);
                                                            finish();
                                                        } catch (Exception e) {

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    })
                                    .show();

                        } else {
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure!")
                                    .setContentText("Error occurred while creating your order, please try again.")
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Unable to Connect Server.", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }

    //sending more orders notes
    private void book_restaurant_addmore(String o_id, List<Integer> menu_id, List<Integer> menu_coun, int size, List<Integer> mntotal, int total, String comment) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "cus_more_order");
        params.put("menu_id", menu_id);
        params.put("menu_qty", menu_coun);
        params.put("total", size);
        params.put("mn_total_price", mntotal);
        params.put("o_id", o_id);
        params.put("tot_total", total);
        params.put("u_id", id);
        params.put("additional_coment", comment);


        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(For_Restaurant_receipt.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Your order has been successfully updated.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            if (getIntent().getStringExtra("rest_type").equals("booking")) {
                                                if (database_helper.getcheckingCount() > 0) {
                                                    if (database_helper.getcartsCount() > 0) {
                                                        database_helper.deleteall();
                                                    }
                                                }
                                            }

                                            finish();
                                        }
                                    })
                                    .show();

                        } else {
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure!")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Unable to Connect Server.", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }


    //for rating
    private void showDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(2)
                .setTitle("Rate this Restaurant")
                .setDescription("How was your experience with us?")
                .setCommentInputEnabled(true)
                .setStarColor(R.color.yellow)
                .setNoteDescriptionTextColor(R.color.black)
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.colorPrimary)
                .setCommentTextColor(R.color.white)
                .setCommentBackgroundColor(R.color.black)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(this) // only if listener is implemented by fragmen
                .show();
    }

    @Override
    public void onPositiveButtonClicked(int rate, String comment) {
        if (for_add.size() > 0) {
            requestRating(Endpoints.ip_server, for_add.get(0).getRest_id(), rate, comment, yes);
            //Toast.makeText(this, ""+for_add.get(0).getRest_id()+"  "+  rate+"  "+ comment, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNegativeButtonClicked() {
        finish();
    }

    @Override
    public void onNeutralButtonClicked() {
        finish();
    }


    //chekout self
    public void requestDatatocheckout(String uri, final String o_id) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "order_to_checkout");
        params.put("o_id", o_id);
        params.put("u_id", id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
//                            showDialog();
//                            yes="yes";
                            finish();
                        } else {
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }


    //rating sending
    public void requestRating(String uri, final String rest_id, final int rate, final String comment, final String yes) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "user_review_for_res");
        params.put("res_id", rest_id);
        params.put("rating", rate);
        params.put("review", comment);
        params.put("u_id", id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Thank you for your kind feedback.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            if (yes.equals("yes")) {
                                                finish();
                                            }
                                        }
                                    })
                                    .show();
                        } else {
                            new SweetAlertDialog(For_Restaurant_receipt.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(For_Restaurant_receipt.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }

    //showing recipt adaptor
    class base_adaptor extends BaseAdapter {
        Context con;
        List<For_addtocart_database_table> for_addtocart_database_tableList;

        public base_adaptor(Context c, List<For_addtocart_database_table> for_addtocart_database_tableList) {
            this.con = c;
            this.for_addtocart_database_tableList = for_addtocart_database_tableList;
        }

        @Override
        public int getCount() {
            return for_addtocart_database_tableList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return for_addtocart_database_tableList.get(position).getItem_id();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(con);
            View view = inflater.inflate(R.layout.layout_reciept_grid, null);
            TextView item_name = view.findViewById(R.id.item_name);
            TextView item_quantity = view.findViewById(R.id.item_quantity);
            TextView item_total = view.findViewById(R.id.item_total);
            item_name.setText(for_addtocart_database_tableList.get(position).getItem_name().toString());
            item_quantity.setText(for_addtocart_database_tableList.get(position).getItem_price() + "X" + for_addtocart_database_tableList.get(position).getItem_count());
            item_total.setText("" + Integer.parseInt(for_addtocart_database_tableList.get(position).getItem_price()) * Integer.parseInt(for_addtocart_database_tableList.get(position).getItem_count()));
            return view;
        }
    }

}
