package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.invitationsListAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.userInvites;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class invitesActivity extends AppCompatActivity {

    private RecyclerView recyclerViewforInvites;
    List<userInvites> userInvitesList;
    TextView noInvitesText;
    private invitationsListAdapter invitationsListAdapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invites);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Your Invitations");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerViewforInvites = findViewById(R.id.recyclerviewforinvites);
        noInvitesText = findViewById(R.id.noInvitesTextView);

        if(Utils.isOnline(invitesActivity.this)) {
            try {
                //calling booking status method
                getInvitesData();
            } catch (Exception ex) {
                new SweetAlertDialog(invitesActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Something went wrong!")
                        .show();
            }
        } else {
            new SweetAlertDialog(invitesActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }

    private void getInvitesData() {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                Log.e("User invites", response);
                JSONObject jsonObject = null;
                JSONArray jsonArray = null;
                try {
                    jsonObject = new JSONObject(response);
                    jsonArray = jsonObject.getJSONArray("invitesToMe");
                    if (jsonArray == null) {
                        recyclerViewforInvites.setVisibility(View.GONE);
                        noInvitesText.setVisibility(View.VISIBLE);
                    } else {
                        recyclerViewforInvites.setVisibility(View.VISIBLE);
                        noInvitesText.setVisibility(View.GONE);

                        userInvitesList = JSONParser.parseInviteData(jsonArray);
                        invitationsListAdapter = new invitationsListAdapter(invitesActivity.this, userInvitesList);
                        recyclerViewforInvites.setAdapter(invitationsListAdapter);
                        LinearLayoutManager llm = new LinearLayoutManager(invitesActivity.this);
                        llm.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerViewforInvites.setLayoutManager(llm);
                    }
                } catch(JSONException ex) {

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(invitesActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "get_user_invites");
                params.put("user_id", Prefs.getString("user_id", "0"));
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

}
