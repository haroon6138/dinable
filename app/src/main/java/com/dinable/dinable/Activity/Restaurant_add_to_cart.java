package com.dinable.dinable.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dinable.dinable.Adapter.for_cart_adapter;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.For_add_to_card_class;
import com.dinable.dinable.classes.For_addtocart_database_table;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Restaurant_add_to_cart extends AppCompatActivity {

    private List<For_addtocart_database_table> for_add_to_card_classes;
    private for_cart_adapter for_cart_adapter;
    private RecyclerView recyclerViewforcart;
    private Database_Helper database_helper;
    public EditText add_comments;
    String orderType = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_add_to_cart);
        try {
            //here assigning to list all save carts from sqlite

            database_helper = new Database_Helper(this);
            for_add_to_card_classes = database_helper.getAllcarts();
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle(getIntent().getStringExtra("rest_name"));
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            orderType = Prefs.getString("orderType", "");

            add_comments = (EditText) findViewById(R.id.add_comments);

            for_cart_adapter = new for_cart_adapter(this, for_add_to_card_classes);
            recyclerViewforcart = (RecyclerView) findViewById(R.id.recyclerview);
            recyclerViewforcart.setAdapter(for_cart_adapter);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewforcart.setLayoutManager(llm);
        } catch (Exception e) {

        }
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.forproceedmanu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.checkout) {
            if (database_helper.getcartsCount() > 0) {
                //here getting all save carts from sqlite then sending to receipt activity

                Intent in = null;

                if(orderType.equals("Dine-In"))
                    in = new Intent(Restaurant_add_to_cart.this,For_Restaurant_receipt.class);

                if(orderType.equals("Delivery"))
                    in = new Intent(Restaurant_add_to_cart.this, selectDropLocationActivity.class);

                in.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
                in.putExtra("rest_type", getIntent().getStringExtra("rest_type"));
                in.putExtra("order_comment", add_comments.getText().toString());
                startActivity(in);
                Animation.zoom(Restaurant_add_to_cart.this);
                finish();
            } else {
                Toasty.error(Restaurant_add_to_cart.this, "Nothing to Proceed.", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
