package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.dinable.dinable.Fragments.AboutFragment;
import com.dinable.dinable.Fragments.ReviewsFragment;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutandReviews extends AppCompatActivity {
    private TabLayout tabLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutand_reviews);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("rest_name"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout=(TabLayout) findViewById(R.id.tabLayout) ;

        tabLayout.setTabTextColors(ContextCompat.getColor(this, android.R.color.white),
                ContextCompat.getColor(this, R.color.black));
        tabLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.colorwhitebackground));

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }
        tabLayout.setupWithViewPager(viewPager);
    }
    private void setupViewPager(ViewPager viewPager) {
        Adapterforaboutfragment adapter = new Adapterforaboutfragment(getSupportFragmentManager());
        adapter.addFragment(new AboutFragment(), "About",getIntent().getStringExtra("rest_id"));
        adapter.addFragment(new ReviewsFragment(), "Reviews",getIntent().getStringExtra("rest_id"));
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animation.windmill(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.windmill(this);
        return super.onSupportNavigateUp();
    }


    static class Adapterforaboutfragment extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapterforaboutfragment(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title,String rest_id) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
            Bundle data = new Bundle();//create bundle instance
            data.putString("rest_id", rest_id);//put string to pass with a key value
            fragment.setArguments(data);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }
}
