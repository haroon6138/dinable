package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.restaurantsadapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.restaurantsclass;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class themesRestaurantSearch extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<restaurantsclass> restaurantsclassList;
    private com.dinable.dinable.Adapter.restaurantsadapter restaurantsadapter;
    double lati, longi;
    String themeId = "", themeName = "";


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes_restaurant_search);

        if(getIntent().hasExtra("themeId"))
            themeId = getIntent().getStringExtra("themeId");
        if(getIntent().hasExtra("themeName"))
            themeName = getIntent().getStringExtra("themeName");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(themeName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lati = Prefs.getDouble("lat", 0.0d);
        longi = Prefs.getDouble("long", 0.0d);

        recyclerView = findViewById(R.id.recyclerview);

        requestDatas(Endpoints.ip_server, "", "", themeId);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    public void requestDatas(String uri, final String keyword_search, final String cusine_id, final String theme_id) {
        final String id = Prefs.getString("user_id", "0");
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);

                    restaurantsclassList = JSONParser.parse_restaurants(response);
                    restaurantsadapter = new restaurantsadapter(themesRestaurantSearch.this, restaurantsclassList, true);
                    recyclerView.setAdapter(restaurantsadapter);
                    LinearLayoutManager llm = new LinearLayoutManager(themesRestaurantSearch.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                    restaurantsadapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(themesRestaurantSearch.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "search_filter_date");
                params.put("keyword_search", keyword_search);
                params.put("cs_id", cusine_id);
                params.put("th_id", theme_id);
                params.put("ratting", "");
                params.put("dollar", "");
                params.put("lat", lati + "");
                params.put("long", longi + "");
                Log.e("search Params", params.toString());
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
}
