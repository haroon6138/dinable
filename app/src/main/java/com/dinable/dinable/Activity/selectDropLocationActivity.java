package com.dinable.dinable.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class selectDropLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    public Location mCurrentLocation;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int LOC_REQ_CODE = 1;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    Button btnSetDropoff;


    String currentLat = "";
    String currentLong = "";
    String currentLocationName = "";
    String currentLocationDetails = "";
    String rest_type, rest_name, orderComment;

    public static TextView dropLocationName, dropLocationDetails;

    View mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_drop_location);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        rest_type = getIntent().getStringExtra("rest_name");
        rest_name = getIntent().getStringExtra("rest_type");
        orderComment = getIntent().getStringExtra("order_comment");

        dropLocationName = findViewById(R.id.dropLocationName);
        dropLocationDetails = findViewById(R.id.dropLocationDetails);

        btnSetDropoff = (Button) findViewById(R.id.setDropoff);
        btnSetDropoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(selectDropLocationActivity.this,For_Restaurant_receipt.class);
                in.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
                in.putExtra("rest_type", getIntent().getStringExtra("rest_type"));
                in.putExtra("order_comment", getIntent().getStringExtra("order_comment"));
                in.putExtra("dropLocationName", dropLocationName.getText().toString());
                in.putExtra("dropLatLong", currentLat + "," + currentLong);
                startActivity(in);
                Animation.zoom(selectDropLocationActivity.this);
                finish();

            }
        });


        dropLocationName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchPickupAddressAutocomplete();
            }
        });

        dropLocationDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchPickupAddressAutocomplete();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e("Map", "Map Ready");

        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.setOnCameraIdleListener(this);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        LatLng userLoc = new LatLng(Prefs.getDouble("lat", 0.0d), Prefs.getDouble("long", 0.0d));

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(userLoc, 16);

        Geocoder geocoder = new Geocoder(selectDropLocationActivity.this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(Prefs.getDouble("lat", 0.0d), Prefs.getDouble("long", 0.0d), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Log.e("address", addresses.toString());

            String AddressLong = addresses.get(0).getAddressLine(0).split("-")[0];
            String AddressShort = "";
//            String AddressShort = addresses.get(0).getAddressLine(0).split("-")[1] + ", " + addresses.get(0).getAddressLine(0).split("-")[2];

            dropLocationName.setText(AddressLong);
            dropLocationDetails.setText(AddressShort);

            currentLat = String.valueOf(Prefs.getDouble("lat", 0.0d));
            currentLong = String.valueOf(Prefs.getDouble("long", 0.0d));
            currentLocationName = AddressLong;
            currentLocationDetails = AddressShort;

            Log.e("City Name", AddressLong + " , " + AddressShort );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//
        mMap.animateCamera(cameraUpdate);

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//            layoutParams.setMargins(0, 0, 30, 310);
        }
    }

    private void setCameraToNewLocation(double lat, double lng, String address) {
        LatLng userLoc = new LatLng(lat, lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(userLoc, 16);

        dropLocationName.setText(address);
        dropLocationDetails.setText("");

        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onCameraIdle() {
        Log.e("Location Idle", mMap.getCameraPosition().toString());
        getAddressFromLatLong(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
    }

    protected void getAddressFromLatLong(double lat, double longi) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, longi, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Log.e("address", addresses.toString());
            Log.e("countryCode", addresses.get(0).getCountryCode());

            String AddressLong = addresses.get(0).getAddressLine(0).split("-")[0];
            String AddressShort = "";
//            String AddressShort = addresses.get(0).getAddressLine(0).split("-")[1];

            dropLocationName.setText(AddressLong);
            dropLocationDetails.setText(AddressShort);

//            ph.setCurrentPickupLat(String.valueOf(lat));
//            ph.setCurrentPickupLong(String.valueOf(longi));
//            ph.setCurrentPickupLocationName(AddressLong);
//            ph.setCurrentPickupLocationDetails(AddressShort);

            currentLat = String.valueOf(lat);
            currentLong = String.valueOf(longi);
            currentLocationName = AddressLong;
            currentLocationDetails = AddressShort;


            Log.e("City Name", AddressLong + " , " + AddressShort );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LOC_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Place", "Place: " + place.getName());
                Log.i("Place", PlaceAutocomplete.getPlace(this, data).getAddress().toString());

                setCameraToNewLocation(PlaceAutocomplete.getPlace(this, data).getLatLng().latitude, PlaceAutocomplete.getPlace(this, data).getLatLng().longitude, PlaceAutocomplete.getPlace(this, data).getAddress().toString());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    protected void launchPickupAddressAutocomplete() {
        try {
            AutocompleteFilter.Builder filterBuilder = new AutocompleteFilter.Builder();
            filterBuilder.setCountry("PK");
            filterBuilder.setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT);

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(filterBuilder.build())
                            .build(this);
            startActivityForResult(intent, LOC_REQ_CODE);
        } catch (Exception e) {
            Log.e("DropLocationActivity", e.getStackTrace().toString());
        }
    }
}
