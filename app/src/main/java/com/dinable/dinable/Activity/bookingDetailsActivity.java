package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Adapter.for_booking_cards_adaptor;
import com.dinable.dinable.Adapter.invitedFriendsAdapter;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.friendsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class bookingDetailsActivity extends AppCompatActivity {

    private TextView rest_name, theme, date, timefrom, timeto, noofpeople, status;
    private ImageView rest_image;
    JSONArray invitedFriendsArray;
    private RecyclerView recyclerViewforinviteeslist;
    private List<friendsModel> invitedFriendsList;
    private invitedFriendsAdapter invitedFriendsAdapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Booking Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rest_image = findViewById(R.id.restaurantsbanners);
        rest_name = findViewById(R.id.restaurantNameText);
        theme = findViewById(R.id.theme);
        date = findViewById(R.id.date);
        timefrom = findViewById(R.id.timefrom);
        timeto = findViewById(R.id.timeto);
        noofpeople = findViewById(R.id.people);
        status = findViewById(R.id.status);

        recyclerViewforinviteeslist = findViewById(R.id.recyclerviewforinviteeslist);
        invitedFriendsList = new ArrayList<>();

        Glide.with(bookingDetailsActivity.this)
                .load(getIntent().getStringExtra("restImage"))
                .into(rest_image);

        rest_name.setText(getIntent().getStringExtra("restaurantNameText"));
        theme.setText(getIntent().getStringExtra("restTheme"));
        date.setText(getIntent().getStringExtra("bookingDate"));
        timeto.setText(getIntent().getStringExtra("bookingTime"));
        timefrom.setText("");
        noofpeople.setText(getIntent().getStringExtra("noofpeople"));
        status.setText("");
        invitedFriendsList = parseInvitedFriendsList(getIntent().getStringExtra("friendsInvited"));

        invitedFriendsAdapter = new invitedFriendsAdapter(bookingDetailsActivity.this, invitedFriendsList);
        recyclerViewforinviteeslist.setAdapter(invitedFriendsAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(bookingDetailsActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewforinviteeslist.setLayoutManager(llm);
    }

    private List<friendsModel> parseInvitedFriendsList(String content) {
        try {
            invitedFriendsArray = new JSONArray(content);
            for (int i = 0; i < invitedFriendsArray.length(); i++) {
                JSONObject jsonObject = invitedFriendsArray.getJSONObject(i);
                friendsModel friendsModel = new friendsModel();

                friendsModel.setFriendId("");
                friendsModel.setFriendName(jsonObject.getString("usr_nm"));
                friendsModel.setFriendEmail(jsonObject.getString("usr_em"));
                friendsModel.setFriendStatus(jsonObject.getString("Status"));
                friendsModel.setFriendFCM("");
                friendsModel.setFriendImage("");

                invitedFriendsList.add(friendsModel);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return invitedFriendsList;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

}
