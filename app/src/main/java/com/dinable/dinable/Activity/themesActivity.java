package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.EventRestAdapter;
import com.dinable.dinable.Adapter.themeActivityListAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.ThemeForSearchClass;
import com.dinable.dinable.classes.restaurantsclass;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class themesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<ThemeForSearchClass> themeForSearchClassList;
    private themeActivityListAdapter adapter;

    private com.dinable.dinable.Adapter.restaurantsadapter restaurantsadapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Themes");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recyclerview);

        load_theme_data();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void load_theme_data() {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("null")) {
                    Toasty.warning(themesActivity.this, "We could not find Themes.", Toast.LENGTH_LONG).show();
                } else {
                    themeForSearchClassList = JSONParser.parse_theme_data(response);
                    adapter = new themeActivityListAdapter(themesActivity.this, themeForSearchClassList);
                    recyclerView.setAdapter(adapter);
                    LinearLayoutManager llm = new LinearLayoutManager(themesActivity.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error", error.getMessage());
                        new SweetAlertDialog(themesActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Unable to Load Category!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "get_all_theme");
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
}
