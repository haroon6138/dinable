package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.DealAdapter;
import com.dinable.dinable.Adapter.notificationsListAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.DealClass;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.notifications_history_model;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class notifications_History extends AppCompatActivity {


    List<notifications_history_model> notificationsClassList;
    RecyclerView recyclerView;
    private notificationsListAdapter adapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications__history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Notifications");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView=(RecyclerView) findViewById(R.id.recylcerView);

        if(Utils.isOnline(notifications_History.this))
        {
            requestData(Endpoints.ip_server);
        }
        else
        {
            new SweetAlertDialog(notifications_History.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Connected!")
                    .show();
        }
    }

    private void requestData(String uri)
    {
        StringRequest request = new StringRequest(Request.Method.POST,uri, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    if (!jsonObject.getBoolean("success"))
                    {
                        Toasty.warning(notifications_History.this, "No Notifications Available.", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        notificationsClassList = JSONParser.parseNotificationsData(response);
                        adapter = new notificationsListAdapter(notifications_History.this, notificationsClassList);
                        recyclerView.setAdapter(adapter);
                        LinearLayoutManager llm = new LinearLayoutManager(notifications_History.this);
                        llm.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(llm);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(notifications_History.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "get_user_all_notification");
                params.put("user_id", Prefs.getString("user_id", ""));
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(notifications_History.this);
    }

}
