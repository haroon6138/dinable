package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dinable.dinable.Adapter.restaurantsadapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.FCM.SharedPrefManager;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.restaurantsclass;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String username, useremail;
    private TextView usernameview, useremailview;
    CircularImageView imageView;
    private RecyclerView recyclerView;
    private List<restaurantsclass> restaurantsclassList;
    private List<restaurantsclass> templist;
    private restaurantsadapter restaurantsadapter;
    //private Database_Helper database_helper;
    EditText for_search;
    double lati, longi;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        username = Prefs.getString("user_name", "");
        useremail = Prefs.getString("user_email", "");
        lati = Prefs.getDouble("lat", 0.0d);
        longi = Prefs.getDouble("long", 0.0d);

        // database_helper=new Database_Helper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Search Work
        for_search = (EditText) findViewById(R.id.et_for_search);
        for_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                serachres(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        usernameview = (TextView) headerView.findViewById(R.id.tv_name);
        useremailview = (TextView) headerView.findViewById(R.id.tv_email);
        imageView = headerView.findViewById(R.id.avatar);

        //Set user image im image view if exist
        //Picasso.get().load(Prefs.getString("user_image","http://autopartsdiscountcoupons.com/dinable/images/administrator-male.png")).into(imageView);
        Glide.with(Home.this)
                .load(Prefs.getString("user_image", "image"))
                .into(imageView);

        if (username != null && useremail != null) {
            usernameview.setText(username);
            useremailview.setText(useremail);
        }

        if (Utils.isOnline(Home.this)) {
            try {
                requestData(Endpoints.ip_server);
            } catch (Exception ex) {
                new SweetAlertDialog(Home.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Some thing went wrong!")
                        .show();
            }

        } else {
            new SweetAlertDialog(Home.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }


    }


    public void requestData(String uri) {
        final String id = Prefs.getString("user_id", "0");
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Restaurants", response);
                restaurantsclassList = JSONParser.parse_restaurants(response);
                restaurantsadapter = new restaurantsadapter(Home.this, restaurantsclassList, false);
                recyclerView.setAdapter(restaurantsadapter);
                LinearLayoutManager llm = new LinearLayoutManager(Home.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(Home.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("req_key", "Restaurants");

                params.put("req_key", "Resturants_Nearby");
                params.put("lat", lati + "");
                params.put("long", longi + "");

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            moveTaskToBack(true);
            //android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.filter) {
            Intent in = new Intent(Home.this, Search_activity.class);
            startActivity(in);
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_orders) {
            Intent i = new Intent(Home.this, My_orders.class);
            i.putExtra("isComingFromRestaurantPage", false);
            this.startActivity(i);
            Animation.slideUp(this);
        } else if (id == R.id.nav_event) {
            startActivity(new Intent(Home.this, EventRestActivity.class));
            Animation.slideUp(this);
        } else if (id == R.id.nav_my_event) {
            startActivity(new Intent(Home.this, MyEvent.class));
            Animation.zoom(this);
        } else if (id == R.id.nav_themes) {
            startActivity(new Intent(Home.this, themesActivity.class));
            Animation.slideUp(this);
        } else if (id == R.id.nav_bookings) {
            startActivity(new Intent(Home.this, My_Bookings.class));
            Animation.slideUp(this);
        } else if (id == R.id.nav_invites) {
            startActivity(new Intent(Home.this, invitesActivity.class));
            Animation.slideUp(this);
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(Home.this, profileact.class));
            Animation.zoom(Home.this);
        } else if (id == R.id.nav_friends) {
            Intent i = new Intent(Home.this, friendsActivity.class);
            startActivity(i);
            //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            Animation.fade(Home.this);
        } else if (id == R.id.nav_contact) {
            startActivity(new Intent(Home.this, Contact_us.class));
            Animation.zoom(Home.this);
        } else if (id == R.id.nav_notifications) {
            startActivity(new Intent(Home.this, notifications_History.class));
            Animation.zoom(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void serachres(String title) {
        templist = new ArrayList<>();
        if (templist != null)
            for (restaurantsclass list : restaurantsclassList) {
                if (list != null)
                    if (list.getRestaurant_name().toLowerCase().contains(title)) {
                        templist.add(list);
                    }
            }
        restaurantsadapter.searchedList(templist);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
