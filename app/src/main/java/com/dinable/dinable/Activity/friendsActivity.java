package com.dinable.dinable.Activity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.dinable.dinable.Fragments.addFriendFragment;
import com.dinable.dinable.Fragments.friendRequestFragment;
import com.dinable.dinable.Fragments.myFriendsFragment;
import com.dinable.dinable.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class friendsActivity extends AppCompatActivity implements addFriendFragment.OnFragmentInteractionListener, friendRequestFragment.OnFragmentInteractionListener, myFriendsFragment.OnFragmentInteractionListener {

    public static String currentOpenedFragment = "";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_myFriends:
                    currentOpenedFragment = "myFriendsFragment";
                    fragment = new myFriendsFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_addFriend:
                    currentOpenedFragment = "addFriendFragment";
                    fragment = new addFriendFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_friendRequest:
                    currentOpenedFragment = "friendRequestFragment";
                    fragment = new friendRequestFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        currentOpenedFragment = "myFriendsFragment";
        Fragment fragment = new myFriendsFragment();
        loadFragment(fragment);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
