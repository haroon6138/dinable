package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.MyEventAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.MyEventClass;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyEvent extends AppCompatActivity {

    List<MyEventClass> myEventClassList;
    RecyclerView recyclerView;
    private MyEventAdapter adapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView=(RecyclerView) findViewById(R.id.recylcerView);

        if(Utils.isOnline(MyEvent.this))
        {
            requestData(Endpoints.ip_server);
        }
        else
        {
            new SweetAlertDialog(MyEvent.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }

    private void requestData(String uri)
    {
        final String id = Prefs.getString("user_id", "0");
        StringRequest request = new StringRequest(Request.Method.POST,uri, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                if (response.contains("null"))
                {
                    Toasty.warning(MyEvent.this, "No Events Available.", Toast.LENGTH_LONG).show();
                }
                else
                {
                    myEventClassList = JSONParser.parse_my_events(response);
                    adapter = new MyEventAdapter(MyEvent.this, myEventClassList);
                    recyclerView.setAdapter(adapter);
                    LinearLayoutManager llm = new LinearLayoutManager(MyEvent.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(MyEvent.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "user_my_events");
                params.put("u_id", id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        Animation.inAndOut(MyEvent.this);
    }
}
