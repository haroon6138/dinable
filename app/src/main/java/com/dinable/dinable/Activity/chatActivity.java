package com.dinable.dinable.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.myRececlerView;
import com.dinable.dinable.classes.ChatModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class chatActivity extends AppCompatActivity {

    private FirebaseRecyclerAdapter<ChatModel, myRececlerView> adapter;
    FirebaseRecyclerOptions<ChatModel> options;

    EditText messageText;
    ImageView submitButton;
    RecyclerView recyclerView;

    //Firebase dependencies
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Query query;

    String fromUserEmail= "";
    String fromUserName= "";
    String toUserEmail = "";
    String toUserName = "";
    String toUserFCMId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();

        if(intent.getExtras() != null) {
            fromUserEmail = intent.getStringExtra("userFromEmail");
            toUserEmail = intent.getStringExtra("userToEmail");
            fromUserName = intent.getStringExtra("userFromName");
            toUserName = intent.getStringExtra("userToName");
            toUserFCMId = intent.getStringExtra("userToFCM");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(toUserName);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));

        messageText = findViewById(R.id.emojicon_edit_text);
        submitButton = findViewById(R.id.submit_button);
        recyclerView = findViewById(R.id.list_of_message);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Chats");

        query = databaseReference.orderByChild("messageTime");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                displayChatMessage();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        displayChatMessage();

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendChatMessage();
                messageText.setText("");
                messageText.requestFocus();
            }
        });
    }

    private void sendChatMessage() {
        ChatModel chat = new ChatModel(messageText.getText().toString(), fromUserEmail, fromUserName);

        databaseReference.push()
                .setValue(chat);
        adapter.notifyDataSetChanged();

        sendFCMPush(fromUserName);

//        displayChatMessage();
    }

    private void sendFCMPush(String userName) {
        final String Legacy_SERVER_KEY = "AAAAIlq1NiY:APA91bGWz7KVIk_E0eedHuJAzYh_mkRVUsGz676cAXwpFQl7B0KPcfc06GlVJ-x44XNkWXMdQb7CPvMv0bXN3flLXF_ztwEqkVqSw_v6FaCeEYJiTDEVOCzvzLSkeBL8LHrvdgdV86g_";
        String msg = userName + " Sent you a new message";
        String title = "Dinable";
        String token = toUserFCMId;

        JSONObject obj = null;
        JSONObject objData = null;
        JSONObject dataobjData = null;

        try {
            obj = new JSONObject();
            objData = new JSONObject();

            objData.put("body", msg);
            objData.put("title", title);
            objData.put("sound", "default");
            objData.put("icon", "icon_name"); //   icon_name image must be there in drawable
            objData.put("tag", token);
            objData.put("priority", "high");

            dataobjData = new JSONObject();
            dataobjData.put("text", msg);
            dataobjData.put("title", title);

            obj.put("to", token);
            //obj.put("priority", "high");

            obj.put("notification", objData);
            obj.put("data", dataobjData);
            Log.e("!_@rj@_@@_PASS:>", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, Endpoints.FCM_PUSH_URL, obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("!_@@_SUCESS", response + "");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("!_@@_Errors--", error + "");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "key=" + Legacy_SERVER_KEY);
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 1000 * 60;// 60 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);

    }

    private void displayChatMessage() {

        options = new FirebaseRecyclerOptions.Builder<ChatModel>()
                .setQuery(query, ChatModel.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<ChatModel, myRececlerView>(options)
        {
            @NonNull
            @Override
            public myRececlerView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(getBaseContext()).inflate(R.layout.list_item, parent, false);
                return new myRececlerView(itemView);
            }

            @Override
            protected void onBindViewHolder(@NonNull myRececlerView holder, int position, @NonNull ChatModel model) {
                if(model.getMessageUser().contains(fromUserEmail) || model.getMessageUser().contains(toUserEmail)) {

                    if(model.getMessageUser().contains(fromUserEmail)){
                        holder.textMessage.setGravity(Gravity.END);
                        holder.textUser.setGravity(Gravity.END);
                        holder.textDateTime.setGravity(Gravity.END);
                    }

                    holder.textMessage.setText(model.getMessageText());
                    holder.textUser.setText(model.getMessageUserName());
                    holder.textDateTime.setText(model.getMessageTime() + "");
                } else {
                    holder.relativeLayout.setVisibility(View.GONE);
                }
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }

}
