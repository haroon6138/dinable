package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.for_booking_cards_adaptor;
import com.dinable.dinable.Adapter.restaurantsadapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Booking_status;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.restaurantsclass;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class My_Bookings extends AppCompatActivity {

    private List<Booking_status> bookingStatusList;
    final String id = Prefs.getString("user_id", "0");
    private RecyclerView recyclerViewforbookingcards;
    private for_booking_cards_adaptor for_booking_cards_adaptor;
    private TextView noBookingsText;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__bookings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Bookings");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        noBookingsText = findViewById(R.id.textView);
        recyclerViewforbookingcards = findViewById(R.id.recyclerviewforbooking);

        //calling bookings status
        if (Utils.isOnline(My_Bookings.this)) {
            try {
                requestbookingStatus(Endpoints.ip_server, id);
            } catch (Exception ex) {
                new SweetAlertDialog(My_Bookings.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Something went wrong!")
                        .show();
            }
        } else {
            new SweetAlertDialog(My_Bookings.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }


    public void requestbookingStatus(String uri, final String User_id) {
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Bookings", response);
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonArray.length() == 0) {
//                    Toasty.warning(My_Bookings.this, "There are no bookings.", Toast.LENGTH_LONG).show();
                    recyclerViewforbookingcards = findViewById(R.id.recyclerviewforbooking);
                    recyclerViewforbookingcards.setVisibility(View.GONE);
                    noBookingsText.setVisibility(View.VISIBLE);
                } else {
                    Log.e("Response", response);

                    recyclerViewforbookingcards = findViewById(R.id.recyclerviewforbooking);
                    bookingStatusList = JSONParser.parse_booking_status(response);
                    for_booking_cards_adaptor = new for_booking_cards_adaptor(My_Bookings.this, bookingStatusList);
                    recyclerViewforbookingcards.setAdapter(for_booking_cards_adaptor);
                    LinearLayoutManager llm = new LinearLayoutManager(My_Bookings.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerViewforbookingcards.setLayoutManager(llm);
//                    for(int i=0;i<bookingStatusList.size();i++){
//                        Toast.makeText(My_Bookings.this, ""+bookingStatusList.get(i).getUsr_id(), Toast.LENGTH_SHORT).show();
//                    }
                    //requestData(Endpoints.ip_server);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(My_Bookings.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "all_resturant_booking_status");
                params.put("usr_id", User_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(My_Bookings.this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

//    public void requestData(String uri) {
//        final String id = Prefs.getString("user_id", "0");
//        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                if (response.contains("null")) {
//                    Toasty.warning(My_Bookings.this, "Restaurants not available.", Toast.LENGTH_LONG).show();
//                } else {
//                    restaurantsclassList = JSONParser.parse_restaurants(response);
//                    recyclerViewforbookingcards =findViewById(R.id.recyclerviewforbooking);
//                    for_booking_cards_adaptor=new for_booking_cards_adaptor(My_Bookings.this,bookingStatusList,restaurantsclassList);
//                    recyclerViewforbookingcards.setAdapter(for_booking_cards_adaptor);
//                }
//
//
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        new SweetAlertDialog(My_Bookings.this, SweetAlertDialog.ERROR_TYPE)
//                                .setTitleText("Oops...")
//                                .setContentText("Some thing went wrong!")
//                                .show();
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("req_key", "Restaurants");
//                return params;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(this);
//        queue.add(request);
//    }
}
