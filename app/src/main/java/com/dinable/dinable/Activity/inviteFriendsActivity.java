package com.dinable.dinable.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.inviteFriendsListAdapter;
import com.dinable.dinable.Adapter.myFriendsListAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.EventRestClass;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.friendsModel;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class inviteFriendsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    List<friendsModel> friendsModelList;
    private List<friendsModel> templist;
    private inviteFriendsListAdapter friendsListAdapter;
    private SearchView simpleSearchView;
    public static ArrayList<String> invitedFriends;
    Button btnInviteFriends;
    public static String noOfPeopleForBooking = "";
    public static int friendsSelected = 0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Invite Friends");
        setSupportActionBar(toolbar);

        if(getIntent().getExtras() != null)
            noOfPeopleForBooking = getIntent().getStringExtra("noOfPeopleForBooking");

        recyclerView = findViewById(R.id.friendsList);
        simpleSearchView = findViewById(R.id.simpleSearchView); // initiate a search view
        friendsModelList = new ArrayList<>();
        invitedFriends = new ArrayList<>();
        btnInviteFriends = findViewById(R.id.btn_inviteFriends);
        btnInviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Invitees", Arrays.deepToString(invitedFriends.toArray()));
                Intent returnIntent = new Intent();
                returnIntent.putExtra("inviteesArray", invitedFriends);
                setResult(Activity.RESULT_OK, returnIntent);
                noOfPeopleForBooking = "";
                finish();
            }
        });

        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
// do something on text submit
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // do something when text changes
                if(newText.length() >= 2) {
                    Log.e("Text Changed", newText);
                    serachres(newText);
                }

                if(newText.length() == 0) {
                    friendsListAdapter.searchedList(friendsModelList);
//                    friendsListAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
        getFriendsData();
    }

    private void serachres(String title)
    {
        templist = new ArrayList<>();
        if(templist != null)
            for (friendsModel list : friendsModelList)
            {
                if(list != null)
                    if(list.getFriendName().toLowerCase().contains(title) || list.getFriendEmail().toLowerCase().contains(title))
                    {
                        templist.add(list);
                    }
            }
        friendsListAdapter.searchedList(templist);
    }

    private void getFriendsData() {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Friends", response);
//                if (response.contains("null")) {
//                    Toasty.warning(getActivity(), "Friends Not Available", Toast.LENGTH_LONG).show();
//                } else {
                friendsModelList = JSONParser.parseFriendsInviteData(response);
                friendsListAdapter = new inviteFriendsListAdapter(inviteFriendsActivity.this, friendsModelList);
                recyclerView.setAdapter(friendsListAdapter);
                LinearLayoutManager llm = new LinearLayoutManager(inviteFriendsActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);
            }
//            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(inviteFriendsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("req_key", "Restaurants");

                params.put("req_key", "get_user_requests");
                params.put("user_id", Prefs.getString("user_id", ""));

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(inviteFriendsActivity.this);
        queue.add(request);
    }
}
