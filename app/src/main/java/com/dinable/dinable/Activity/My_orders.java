package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.for_orders_cards_adaptor;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.Order_class;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class My_orders extends AppCompatActivity {
    private RecyclerView recyclerView;
    private TextView noOrdersTextView;
    private for_orders_cards_adaptor for_orders_cards_adaptor;
    private List<Order_class> order_classList;

    String rest_id = "";
    String orderUrl = "";
    Intent intent;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Orders");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerviewfororders);
        noOrdersTextView = findViewById(R.id.noOrdersText);
        noOrdersTextView.setVisibility(View.GONE);

        intent = getIntent();
        if (intent.getBooleanExtra("isComingFromRestaurantPage", false)) {
            rest_id = intent.getStringExtra("rest_id");
            orderUrl = "user_order_DETAIL_Per_Restaurant";
            toolbar.setTitle(intent.getStringExtra("restName"));
        } else {
            orderUrl = "user_order_DETAIL";
        }

        if (Utils.isOnline(My_orders.this)) {
            try {
                requestData(Endpoints.ip_server);
            } catch (Exception ex) {
                new SweetAlertDialog(My_orders.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Something went wrong!")
                        .show();
            }

        } else {
            new SweetAlertDialog(My_orders.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }



        /*
         *
         * for_orders_cards_adaptor    // for adaptor ready
         * for_orders_showing_cards   //for xml cards layout ready
         * Order_class               //for data class ready
         *
         *
         * */


    }

    public void requestData(String uri) {
        final String id = Prefs.getString("user_id", "0");
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response);
                Log.e("orderURL", orderUrl);

                if (!response.equalsIgnoreCase("null")) {
                    order_classList = JSONParser.parse_Orders(response);
                    for_orders_cards_adaptor = new for_orders_cards_adaptor(My_orders.this, order_classList);
                    recyclerView.setAdapter(for_orders_cards_adaptor);
                    LinearLayoutManager llm = new LinearLayoutManager(My_orders.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                    recyclerView.setVisibility(View.VISIBLE);
                    noOrdersTextView.setVisibility(View.GONE);
                } else {
                    Toasty.error(My_orders.this, "No Orders for this restaurant yet.").show();
                    recyclerView.setVisibility(View.GONE);
                    noOrdersTextView.setVisibility(View.VISIBLE);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(My_orders.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", orderUrl);
                params.put("usr_id", id);
                params.put("r_id", rest_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(My_orders.this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isOnline(My_orders.this)) {
            try {
                requestData(Endpoints.ip_server);
            } catch (Exception ex) {
                new SweetAlertDialog(My_orders.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Something went wrong!")
                        .show();
            }

        } else {
            new SweetAlertDialog(My_orders.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }
}


