package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.EventRestAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.EventRestClass;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.restaurantsclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventRestActivity extends AppCompatActivity {

    private List<EventRestClass> eventRestClassList;
    private List<EventRestClass> templist;
    RecyclerView recyclerView;
    private EventRestAdapter adapter;
    EditText for_search;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_rest);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Restaurant Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Search Work
        for_search = (EditText) findViewById(R.id.et_for_search);
        for_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                serachres(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recylcerView);

        if (Utils.isOnline(EventRestActivity.this)) {
            requestData(Endpoints.ip_server);
        } else {
            new SweetAlertDialog(EventRestActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }

    private void requestData(String uri) {
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("null")) {
                    Toasty.warning(EventRestActivity.this, "No Themes Available.", Toast.LENGTH_LONG).show();
                } else {
                    eventRestClassList = JSONParser.parse_rest_event(response);
                    adapter = new EventRestAdapter(EventRestActivity.this, eventRestClassList);
                    recyclerView.setAdapter(adapter);
                    LinearLayoutManager llm = new LinearLayoutManager(EventRestActivity.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(EventRestActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "resturant_events");
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void serachres(String title) {
        templist = new ArrayList<>();
        if (templist != null)
            for (EventRestClass list : eventRestClassList) {
                if (list != null)
                    if (list.getEvt_title().toLowerCase().contains(title) || list.getEvt_cat_name().toLowerCase().contains(title)) {
                        templist.add(list);
                    }
            }
        adapter.searchedList(templist);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(EventRestActivity.this);
    }
}
