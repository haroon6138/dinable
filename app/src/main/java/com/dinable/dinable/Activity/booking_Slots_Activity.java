package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.SlotListAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.slot;
import com.google.firebase.internal.InternalTokenResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class booking_Slots_Activity extends AppCompatActivity {

    private RecyclerView recyclerView_slots;
    private List<slot> slotList;
    private SlotListAdapter slotListAdapter;
    private SweetAlertDialog pd;

    String rest_Name, res_id, bookingDate;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking__slots_);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            rest_Name = intent.getStringExtra("restName");
            res_id = intent.getStringExtra("restId");
            bookingDate = intent.getStringExtra("date");
        }

        pd = Utils.showProgress(booking_Slots_Activity.this);
        pd.show();

        recyclerView_slots = (RecyclerView) findViewById(R.id.recyclerview);
        slotList = new ArrayList<>();
        slotListAdapter = new SlotListAdapter(this, slotList, booking_Slots_Activity.this);

        recyclerView_slots.setAdapter(slotListAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView_slots.setLayoutManager(llm);

        if (Utils.isOnline(booking_Slots_Activity.this)) {
            StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Booking Slots", response);
                    pd.dismiss();
                    if (response.contains("null")) {
                        Toasty.warning(booking_Slots_Activity.this, "We could not find any booking slots.", Toast.LENGTH_LONG).show();
                    } else {
//                    theme_name = JSONParser.parse_theme(response);
//                    book_category.setAdapter(new ArrayAdapter<String>(RestaurantBooking.this, android.R.layout.simple_spinner_dropdown_item, theme_name));
                        try {
                            JSONObject obj = new JSONObject(response);

                            String openingTime = obj.getString("open");
                            String closingTime = obj.getString("close");

                            JSONArray slotsArray = obj.getJSONArray("slots");

                            TextView restName = (TextView) findViewById(R.id.restaurantName);
                            restName.setText(rest_Name);
                            TextView openTime = (TextView) findViewById(R.id.openTime);
                            openTime.setText("Opens At: " + openingTime);
                            TextView closeTime = (TextView) findViewById(R.id.closeTime);
                            closeTime.setText("Closes At: " + closingTime);
                            TextView bookingTime = (TextView) findViewById(R.id.bookingDate);
                            bookingTime.setText("Booking Date: " + bookingDate);

                            JSONObject jObj;
                            JSONArray arr = null;
                            try {
                                jObj = new JSONObject(response);
                                arr = jObj.getJSONArray("slots");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            for (int i = 0; i < arr.length(); i++) {
                                try {
                                    JSONObject jObj2 = arr.getJSONObject(i);
                                    slot mSlot = new slot();
                                    mSlot.setTime(jObj2.getString("time"));
                                    mSlot.setBooked("Seats Booked: " + jObj2.getString("booked"));
                                    mSlot.setNo_of_people_remaining("Seats Remaining: " + jObj2.getString("no_of_people_remaining"));
                                    slotList.add(mSlot);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

//                        // adding recipes to cart list
//                        slotList.clear();
//                        slotList.addAll(slots);

                            // refreshing recycler view
                            slotListAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pd.dismiss();
                            new SweetAlertDialog(booking_Slots_Activity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("Unable to Load Booking Slots!")
                                    .show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("req_key", "slot_available");
                    params.put("res_id", res_id);
                    params.put("date", bookingDate);
                    Log.e("booking_params", params.toString());
                    return params;
                }
            };
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            new SweetAlertDialog(booking_Slots_Activity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error!")
                    .setContentText("No Internet Connection.")
                    .show();
        }
    }
}
