package com.dinable.dinable.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dinable.dinable.Adapter.formenuadapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Booking_status;
import com.dinable.dinable.classes.CusineMenuClass;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.cuisineclass;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RestaurantPage extends AppCompatActivity {
    private String rest_id, rest_self_id;
    Double rest_lat, rest_long;
    private TabLayout tabLayout;
    private ImageView forrestimage, add_circle;
    Button res_self, bookingBtn, deliveryBtn;
    private List<cuisineclass> cuisineclassList;
    private RecyclerView recyclerViewformenu;
    private List<CusineMenuClass> cusineMenuClassList;
    private formenuadapter formenuadapter;
    private SweetAlertDialog pd;
    private ImageView add_to_card;
    private String show_to_add = null, For_showing_checking = null;
    CounterFab floatingActionButton;

    private Database_Helper database_helper;
    private List<Integer> forcheckinglist;
    private List<Booking_status> bookingStatusList;
    final String id = Prefs.getString("user_id", "0");
    String rest_type = null;
    private MenuItem callWaiterButton, currentOrderButton;
    private TextView cuisineMenuNotAvailbleText;

    Location userLocation, restLocation;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("rest_name"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cuisineMenuNotAvailbleText = findViewById(R.id.cuisineText);

        rest_id = getIntent().getStringExtra("rest_id");
        rest_lat = getIntent().getDoubleExtra("rest_lat", 0.0d);
        rest_long = getIntent().getDoubleExtra("rest_long", 0.0d);

        userLocation = new Location("");
        userLocation.setLatitude(Prefs.getDouble("lat", 0.0d));
        userLocation.setLongitude(Prefs.getDouble("long", 0.0d));

        restLocation = new Location("");
        restLocation.setLatitude(Double.valueOf(rest_lat));
        restLocation.setLongitude(Double.valueOf(rest_long));

        tabLayout = findViewById(R.id.tabLayout);
        bookingBtn = findViewById(R.id.Booking_btn);
        deliveryBtn = findViewById(R.id.delivery_btn);
        deliveryBtn.setVisibility(View.VISIBLE);
        deliveryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDeliveryOptions();
            }
        });

        //initialize database
        database_helper = new Database_Helper(this);

        //initialize add cart button and list for cart
        add_to_card = (ImageView) findViewById(R.id.add_to_card);

        //View view = View.inflate(R.layout.cusine_detail_card, )
        add_circle = (ImageView) findViewById(R.id.add_to_card);
        rest_self_id = getIntent().getStringExtra("rest_self");
        res_self = (Button) findViewById(R.id.BC_btn);

        //sending to cart page
        floatingActionButton = (CounterFab) findViewById(R.id.cart_float);
        Log.e("Count", database_helper.getcartsCount() + "");
        Log.e("Count", database_helper.getcheckingCount() + "");
        floatingActionButton.setCount(database_helper.getcartsCount());
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (database_helper.getcartsCount() > 0) {
                    Intent in = new Intent(RestaurantPage.this, Restaurant_add_to_cart.class);
                    in.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
                    in.putExtra("rest_type", rest_type);
                    startActivity(in);
                    Animation.slideUp(RestaurantPage.this);
                } else {
                    Toasty.error(RestaurantPage.this, "Nothing added in cart", Toast.LENGTH_SHORT).show();
                    floatingActionButton.setCount(0);
                }
            }
        });

        try {
            if (database_helper.getcheckingCount() > 0) {
                forcheckinglist = database_helper.getAllcheckings();
            }
        } catch (Exception e) {

        }

        if (Utils.isOnline(RestaurantPage.this)) {
            try {
                //calling booking status method
                if (database_helper.getcheckingCount() > 0) {
                    if (database_helper.getcheckingCount() > 0) {
                        res_self.setText(" Check In");
                        bookingBtn.setVisibility(View.GONE);
                        for (int i = 0; i < forcheckinglist.size(); i++) {
                            if (forcheckinglist.get(i) == Integer.parseInt(rest_id)) {
                                if (Utils.isOnline(RestaurantPage.this)) {
                                    // Call the Restaurant CheckINS Methode
                                    res_check_ins();
                                } else {
                                    new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Oops...")
                                            .setContentText("Internet Not Found!")
                                            .show();
                                }
                            } else {
                                res_self.setText(" Check In");
                                bookingBtn.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        res_self.setText(" Check In");
                        bookingBtn.setVisibility(View.GONE);
                    }
                } else {
                    requestbookingStatus(Endpoints.ip_server, rest_id, id);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Something went wrong!")
                        .show();
            }
        } else {
            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
        //adding checking data


        if (rest_self_id.contains("0")) {
            rest_type = "self";
            bookingBtn.setVisibility(View.GONE);
            //checking process from sqlite
            if (database_helper.getcheckingCount() > 0) {
                res_self.setText(" Check In");

                if (callWaiterButton != null)
                    callWaiterButton.setVisible(true);

                if (currentOrderButton != null)
                    currentOrderButton.setVisible(true);

                for (int i = 0; i < forcheckinglist.size(); i++) {
                    if (forcheckinglist.get(i) == Integer.parseInt(rest_id)) {
                        if (Utils.isOnline(RestaurantPage.this)) {
                            // Call the Restaurant CheckINS Method
                            res_check_ins();
                        } else {
                            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("Internet Not Found!")
                                    .show();
                        }
                    } else {
                        res_self.setText(" Check In");
                    }
                }
            } else {
                res_self.setText(" Check In");
            }
        } else {
            res_self.setText(" Booking");
            rest_type = "booking";
            bookingBtn.setVisibility(View.VISIBLE);
            bookingBtn.setText(" Check In");
        }

        bookingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bookingBtn.getText().toString().equals(" Check In")) {
                    //open Checkins Activity
                    if (Utils.isOnline(RestaurantPage.this)) {
                        // Call the Restaurant CheckINS Methode
                        res_check_ins();
                    } else {
                        new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Internet Not Found!")
                                .show();
                    }
                }
            }
        });

        //Set On click Listener FOr Booking AND Checkins
        res_self.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (res_self.getText().toString().equals(" Check In")) {
                    //open Checkins Activity
                    if (Utils.isOnline(RestaurantPage.this)) {
                        // Call the Restaurant CheckINS Methode
                        res_check_ins();
                    } else {
                        new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Internet Not Found!")
                                .show();
                    }
                } else if (res_self.getText().toString().equals(" You can order now.")) {
                    //add_circle.setVisibility(View.VISIBLE);
                    new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText("Start ordering with adding items to cart.")
                            .show();
                    callWaiterButton.setVisible(true);
                    currentOrderButton.setVisible(true);
                } else if (res_self.getText().toString().equals(" Pending")) {
                    new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.WARNING_TYPE)
                            .setContentText("Your booking is still pending.")
                            .show();
                } else {
                    // Open Booking Activity
                    Intent i = new Intent(RestaurantPage.this, RestaurantBooking.class);
                    i.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
                    i.putExtra("rest_id", rest_id);
                    i.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
                    i.putExtra("rest_id", getIntent().getStringExtra("rest_id"));
                    i.putExtra("rest_image", getIntent().getStringExtra("rest_image"));
                    i.putExtra("rest_self", getIntent().getStringExtra("rest_self"));
                    startActivity(i);
                    Animation.windmill(RestaurantPage.this);
                    finish();
                }
            }
        });

        forrestimage = (ImageView) findViewById(R.id.restaurantsbanners);
        Glide.with(this)
                .load(getIntent().getStringExtra("rest_image"))
                .into(forrestimage);

        recyclerViewformenu = (RecyclerView) findViewById(R.id.recyclerviewformenu);

        if (Utils.isOnline(RestaurantPage.this)) {
            requestData(Endpoints.ip_server, rest_id);
        } else {
            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }


        tabLayout.setTabTextColors(ContextCompat.getColor(this, android.R.color.white),
                ContextCompat.getColor(this, R.color.black));
        tabLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.colorwhitebackground));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (Utils.isOnline(RestaurantPage.this)) {
                    //assigning cusine ids and names to tablayout
                    for (int i = 0; i < cuisineclassList.size(); i++) {
                        if (cuisineclassList.get(i).getCuisine_name().equals(tab.getText())) {
                            requestDataforcusinemenu(Endpoints.ip_server, rest_id, String.valueOf(cuisineclassList.get(i).getCuisine_id()));
                        }
                    }
                } else {
                    new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Internet Not Found!")
                            .show();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.restaurantsmenu, menu);
        callWaiterButton = menu.findItem(R.id.waiter_call);
        callWaiterButton.setVisible(false);
        currentOrderButton = menu.findItem(R.id.fordeals);
        currentOrderButton.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_detail) {
            Intent i = new Intent(RestaurantPage.this, AboutandReviews.class);
            i.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
            i.putExtra("rest_id", rest_id);
            this.startActivity(i);
            Animation.card(this);
        } else if (id == R.id.fordeals) {
            Intent i = new Intent(RestaurantPage.this, My_orders.class);
            i.putExtra("rest_id", rest_id);
            i.putExtra("isComingFromRestaurantPage", true);
            i.putExtra("restName", getIntent().getStringExtra("rest_name"));
            this.startActivity(i);
            Animation.card(this);
        } else if (id == R.id.waiter_call) {
            callWaiter(rest_id);
        }


        return super.onOptionsItemSelected(item);
    }

    public void callWaiter(String rest_id) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams params = new RequestParams();
        params.put("req_key", "need_waiter");
        params.put("restaurant_id", rest_id);
        params.put("user_id", id);
        params.put("order_id", "0");

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(RestaurantPage.this);
                pd.show();
                Log.e("need waiter params", params.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(RestaurantPage.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good Job!")
                                    .setContentText(object.getString("message"))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();

                        } else {
                            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(RestaurantPage.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }

    //for requesting fetching booking status data
    public void requestbookingStatus(String uri, final String rest_ids, final String User_id) {
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RestaurantBooking", response);
                if (response.contains("null")) {
                    //Toasty.warning(RestaurantPage.this, "your booking is not available.", Toast.LENGTH_LONG).show();
                } else {
                    bookingStatusList = JSONParser.parse_rest_booking_status(response);
                    for (int in = 0; in < bookingStatusList.size(); in++) {
                        if (bookingStatusList.get(in).getUsr_id().equals(id) && bookingStatusList.get(in).getRest_id().equals(rest_id)) {
                            if (bookingStatusList.get(in).getRejected().equals("0")) {
                                if (bookingStatusList.get(in).getApprove_pending().equals("0")) {
                                    res_self.setText(" Pending");
                                    bookingBtn.setVisibility(View.GONE);
                                    deliveryBtn.setVisibility(View.GONE);
                                }

//                                else if(bookingStatusList.get(in).getApprove_pending().equals("2")){
//                                    res_self.setText("Booking");
//                                }
//
                                else if (bookingStatusList.get(in).getApprove_pending().equals("1")) {
                                    if (database_helper.getcheckingCount() > 0) {
                                        res_self.setText(" Check In");
                                        bookingBtn.setVisibility(View.GONE);
                                        for (int i = 0; i < forcheckinglist.size(); i++) {
                                            if (forcheckinglist.get(i) == Integer.parseInt(rest_id)) {
                                                if (Utils.isOnline(RestaurantPage.this)) {
                                                    // Call the Restaurant CheckINS Methode
                                                    res_check_ins();
                                                } else {
                                                    new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                                            .setTitleText("Oops...")
                                                            .setContentText("Internet Not Found!")
                                                            .show();
                                                }
                                            } else {
                                                res_self.setText(" Check In");
                                                bookingBtn.setVisibility(View.GONE);
                                            }
                                        }
                                    } else {
                                        res_self.setText(" Check In");
                                        bookingBtn.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                res_self.setText(" Booking");
                                deliveryBtn.setVisibility(View.VISIBLE);
                            }
                        }
                        //Toasty.success(RestaurantPage.this, ""+bookingStatusList.get(i).getUsr_id()+"  "+bookingStatusList.get(i).getTheme(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "resturant_booking_status");
                params.put("res_id", rest_ids);
                params.put("usr_id", User_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


    //first time requesting data for initializing
    public void requestData(String uri, final String rest_ids) {
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("null")) {
                    Toasty.warning(RestaurantPage.this, "Cuisines are not available.", Toast.LENGTH_LONG).show();

                } else {
                    cuisineclassList = JSONParser.parse_cuisine(response);
                    for (int i = 0; i < cuisineclassList.size(); i++) {
                        tabLayout.addTab(tabLayout.newTab().setText(cuisineclassList.get(i).getCuisine_name()));
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "res_cusine");
                params.put("res_id", rest_ids);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


    public void requestDataforcusinemenu(String uri, final String reid, final String cusid) {
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("null")) {
                    Toasty.warning(RestaurantPage.this, "Cuisine menu not available.", Toast.LENGTH_LONG).show();
                    try {

                        cuisineMenuNotAvailbleText.setVisibility(View.VISIBLE);
                        recyclerViewformenu.setVisibility(View.GONE);

                        cusineMenuClassList.clear();
                        formenuadapter = new formenuadapter(RestaurantPage.this, cusineMenuClassList, show_to_add, rest_id, getIntent().getStringExtra("rest_name"), For_showing_checking, floatingActionButton);
                        recyclerViewformenu.setAdapter(formenuadapter);
                        LinearLayoutManager llm = new LinearLayoutManager(RestaurantPage.this);
                        llm.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerViewformenu.setLayoutManager(llm);
                    } catch (Exception e) {

                    }
                } else {
                    cuisineMenuNotAvailbleText.setVisibility(View.GONE);
                    recyclerViewformenu.setVisibility(View.VISIBLE);

                    cusineMenuClassList = JSONParser.parse_cusine_details(response);
                    formenuadapter = new formenuadapter(RestaurantPage.this, cusineMenuClassList, show_to_add, rest_id, getIntent().getStringExtra("rest_name"), For_showing_checking, floatingActionButton);
                    recyclerViewformenu.setAdapter(formenuadapter);
                    LinearLayoutManager llm = new LinearLayoutManager(RestaurantPage.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerViewformenu.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "cusine_menu");
                params.put("res_id", reid);
                params.put("cs_id", cusid);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    // Declare the Restaurant CheckINS Function
    private void res_check_ins() {
        final String[] tableNo = {""};
        Log.e("distance", userLocation.distanceTo(restLocation) + "");
        if (userLocation.distanceTo(restLocation) <= 40000.0f) { // If Check-In Restaurant is less than or equal to 4000 meters from user current location
            final Dialog dialog = new Dialog(RestaurantPage.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_table_no);
            dialog.setTitle("Restaurant Check-In");

            final EditText u_c = dialog.findViewById(R.id.tableNo);
            TextView cancel = dialog.findViewById(R.id.cancel);
            TextView submit = dialog.findViewById(R.id.submit);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tableNo[0] = u_c.getText().toString();
                    if (tableNo[0].equalsIgnoreCase("")) {
                        Toasty.error(RestaurantPage.this, "Table No. Cannot Be Empty!").show();
                    } else {
                        doRestaurantCheckIn(dialog, tableNo[0]);
                    }
                }
            });

            JSONArray jsonArray = null;
            JSONObject jsonObject = null;
            boolean hasInputTableNo = false;

            if (rest_self_id.contains("0")) {
                doRestaurantCheckIn(dialog, "0");
            } else {
            try {
                jsonArray = new JSONArray(Prefs.getString("CheckInData", "[]"));

                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    if (rest_id.equalsIgnoreCase(jsonObject.getString("restaurantID")))
                        hasInputTableNo = true;
                    else
                        hasInputTableNo = false;
                }

                if (hasInputTableNo) {
                    doRestaurantCheckIn(dialog, tableNo[0]);
                } else {
                    dialog.show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        } else {
            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error!")
                    .setContentText("You haven't arrived at the restaurant yet.")
                    .show();
        }
    }

    private void setDeliveryOptions() {
        res_self.setText(" You can order now.");
        bookingBtn.setVisibility(View.GONE);
        deliveryBtn.setVisibility(View.GONE);
        callWaiterButton.setVisible(false);
        currentOrderButton.setVisible(true);
        show_to_add = "show";
        For_showing_checking = "show";
        Prefs.putString("orderType", "Delivery");
        floatingActionButton.setVisibility(View.VISIBLE);
        if (Utils.isOnline(RestaurantPage.this)) {
            for (int i = 0; i < cuisineclassList.size(); i++) {
                if (cuisineclassList.get(i).getCuisine_name().equals(tabLayout.getTabAt(0).getText())) {
                    requestDataforcusinemenu(Endpoints.ip_server, rest_id, String.valueOf(cuisineclassList.get(i).getCuisine_id()));
                }
            }
        } else {
            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }


    private void doRestaurantCheckIn(Dialog dialog, String tableNo) {
        dialog.dismiss();
        final String id = Prefs.getString("user_id", "0");
        final String finalTableNo = tableNo;
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "resturant_cheackins");
        params.put("res_id", rest_id);
        params.put("usr_id", id);
        params.put("table_no", finalTableNo);
        Log.e("Params", params.toString());

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(RestaurantPage.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                Log.e("checkIn", response);
                if (response.equals("null")) {
                    Toasty.warning(RestaurantPage.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(Prefs.getString("CheckInData", ""));

                        jsonObject.put("tableNo", finalTableNo);
                        jsonObject.put("restaurantID", rest_id);
                        jsonObject.put("hasInputTableNo", true);

                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.e("checkIn", jsonArray.toString());

                    Prefs.putString("CheckInData", jsonArray.toString());

                    Prefs.putString("tableNo", finalTableNo);
                    Prefs.putBoolean("hasInputTableNo", true);
                    Prefs.putString("orderType", "Dine-In");
                    try {
                        JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();

                            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good Job!")
                                    .setContentText("Check In Successful. Now you can order.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            //Toasty.success(RestaurantPage.this,"Great OK Button Cliked",Toast.LENGTH_LONG).show();
                                            if (database_helper.getcheckingCount() > 0) {
                                                for (int i = 0; i < forcheckinglist.size(); i++) {
                                                    if (forcheckinglist.get(i) == Integer.parseInt(rest_id)) {

                                                    } else {
                                                        try {
                                                            if (database_helper.getcartsCount() > 0) {
                                                                database_helper.deleteall();
                                                            }
                                                            if (database_helper.getcheckingCount() > 0) {
                                                                database_helper.deleteallchecking();
                                                            }
                                                        } catch (Exception e) {

                                                        }
                                                    }
                                                }
                                            }

                                            res_self.setText(" You can order now.");
                                            bookingBtn.setVisibility(View.GONE);
                                            deliveryBtn.setVisibility(View.GONE);
                                            callWaiterButton.setVisible(true);
                                            currentOrderButton.setVisible(true);
                                            show_to_add = "show";
                                            For_showing_checking = "show";
                                            floatingActionButton.setVisibility(View.VISIBLE);
                                            try {
                                                database_helper.insert_checking(Integer.parseInt(rest_id));
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }

                                            if (Utils.isOnline(RestaurantPage.this)) {
                                                for (int i = 0; i < cuisineclassList.size(); i++) {
                                                    if (cuisineclassList.get(i).getCuisine_name().equals(tabLayout.getTabAt(0).getText())) {
                                                        requestDataforcusinemenu(Endpoints.ip_server, rest_id, String.valueOf(cuisineclassList.get(i).getCuisine_id()));
                                                    }
                                                }
                                            } else {
                                                new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText("Oops...")
                                                        .setContentText("Internet Not Found!")
                                                        .show();
                                            }
                                            sweetAlertDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        } else {
                            new SweetAlertDialog(RestaurantPage.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(RestaurantPage.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
                Log.d("response", "FINISHED");
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(RestaurantPage.this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            // requestbookingStatus(Endpoints.ip_server,rest_id,id);
            floatingActionButton.setCount(database_helper.getcartsCount());
        } catch (Exception e) {

        }
    }
}

