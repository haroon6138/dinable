package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.restaurantsadapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.CusineForSearchClass;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.ThemeForSearchClass;
import com.dinable.dinable.classes.restaurantsclass;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Search_activity extends AppCompatActivity {

    Spinner cusine_spiner, theme_spinner;
    List<CusineForSearchClass> cusineForSearchClassList;
    List<ThemeForSearchClass> themeForSearchClassList;
    List<String> SearchClassList;
    List<String> themeList;
    Button btn_search, onedollar, twodollar, threedollar;
    EditText tv_search;
    RatingBar searchrating;

    String keyword_search = "", cs_id = "", th_id = "";
    String price_dollar = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SearchClassList = new ArrayList<>();
        SearchClassList.add(0, "Choose Cusine");
        themeList = new ArrayList<>();
        themeList.add(0, "Choose Theme");
        cusine_spiner = findViewById(R.id.cus_search);
        theme_spinner = findViewById(R.id.category_theme);
        btn_search = findViewById(R.id.btn_search);
        tv_search = findViewById(R.id.tv_search);

        //price buttons
        onedollar = findViewById(R.id.onedollar);
        twodollar = findViewById(R.id.twodollar);
        threedollar = findViewById(R.id.threedollar);

        onedollar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onedollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.for_price_select));
                onedollar.setTextColor(getResources().getColor(R.color.ef_white));
                twodollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.shape_for_comment));
                twodollar.setTextColor(getResources().getColor(R.color.black2));
                threedollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.shape_for_comment));
                threedollar.setTextColor(getResources().getColor(R.color.black2));
                price_dollar = "$";
            }
        });
        twodollar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onedollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.shape_for_comment));
                onedollar.setTextColor(getResources().getColor(R.color.black2));
                twodollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.for_price_select));
                twodollar.setTextColor(getResources().getColor(R.color.ef_white));
                threedollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.shape_for_comment));
                threedollar.setTextColor(getResources().getColor(R.color.black2));
                price_dollar = "$$";
            }
        });
        threedollar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onedollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.shape_for_comment));
                onedollar.setTextColor(getResources().getColor(R.color.black2));
                twodollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.shape_for_comment));
                twodollar.setTextColor(getResources().getColor(R.color.black2));
                threedollar.setBackgroundDrawable(ContextCompat.getDrawable(Search_activity.this, R.drawable.for_price_select));
                threedollar.setTextColor(getResources().getColor(R.color.ef_white));
                price_dollar = "$$$";
            }
        });

        //rating
        searchrating = findViewById(R.id.search_ratings);


        load_cusines_for_spiner();
        load_theme_for_spiner();

        theme_spinner.setSelection(0);

        // Set Cusine Spinner Item Selected Event
        cusine_spiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (int io = 0; io < cusineForSearchClassList.size(); io++) {
                    if (cusineForSearchClassList.get(io).getC_name().equals(SearchClassList.get(position))) {
                        cs_id = cusineForSearchClassList.get(io).getC_id();
                    } else if (SearchClassList.get(position).equals("Choose Cuisine")) {
                        cs_id = "";
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Set Theme Spinner Item Selected Event
        theme_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (int io = 0; io < themeForSearchClassList.size(); io++) {
                    if (themeForSearchClassList.get(io).getTheme_name().equals(themeList.get(position))) {
                        th_id = themeForSearchClassList.get(io).getTheme_id();
                    } else if (themeList.get(position).equals("Choose Theme")) {
                        th_id = "";
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Set Button CLick Listner
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keyword_search = tv_search.getText().toString();
                Intent in = new Intent(Search_activity.this, search_result_activity.class);
                in.putExtra("keyword_search", keyword_search);
                in.putExtra("cusine_id", cs_id);
                in.putExtra("theme_id", th_id);
                if (searchrating.getRating() == 0.0) {
                    in.putExtra("ratting", "");
                } else {
                    in.putExtra("ratting", searchrating.getRating() + "");
                }
                //in.putExtra("ratting",searchrating.getRating()+"");
                in.putExtra("dollar", price_dollar);
                startActivity(in);

            }
        });
    }

    //Declear the function for Spiner for data
    private void load_cusines_for_spiner() {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("null")) {
                    Toasty.warning(Search_activity.this, "We could not find Cusines.", Toast.LENGTH_LONG).show();
                } else {
                    cusineForSearchClassList = JSONParser.parse_cusine_For_search(response);
                    //Toast.makeText(Search_activity.this,""+cusineForSearchClassList.size(),Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < cusineForSearchClassList.size(); i++) {
                        SearchClassList.add(cusineForSearchClassList.get(i).getC_name());
                    }
                    cusine_spiner.setAdapter(new ArrayAdapter<>(Search_activity.this, android.R.layout.simple_spinner_dropdown_item, SearchClassList));
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(Search_activity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Unable to Load Category!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "get_all_csn");
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void load_theme_for_spiner() {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("null")) {
                    Toasty.warning(Search_activity.this, "We could not find Theme.", Toast.LENGTH_LONG).show();
                } else {
                    themeForSearchClassList = JSONParser.parse_theme_For_search(response);
                    for (int i = 0; i < themeForSearchClassList.size(); i++) {
                        themeList.add(themeForSearchClassList.get(i).getTheme_name());
                    }
                    theme_spinner.setAdapter(new ArrayAdapter<>(Search_activity.this, android.R.layout.simple_spinner_dropdown_item, themeList));
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(Search_activity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Unable to Load Category!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "get_all_theme");
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
    }

}
