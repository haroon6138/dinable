package com.dinable.dinable.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dinable.dinable.Adapter.SlotListAdapter;
import com.dinable.dinable.Adapter.inviteFriendsListAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.slot;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RestaurantBooking extends AppCompatActivity {
//ev_time_from
    EditText ev_num_people, ev_book_date,  ev_time_to, ev_booking_detail,get_emails;
    TextView show_emails;
    Spinner book_category;
    Calendar myCalendar;
    String res_id, rest_Name;
    Button Book_btn,insert_emails, btn_inviteFriends;
    LinearLayout email_member_layout;
    CheckBox checkBox1;
    private SweetAlertDialog pd;
    ArrayList<String> theme_name,emails,emails_ids,emails_address;
    String noOfPeopleForBooking = "";
    ArrayList<String> invitedFriendsList;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_booking);
        rest_Name = getIntent().getStringExtra("rest_name");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Book " + rest_Name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myCalendar = Calendar.getInstance();
        ev_num_people = (EditText) findViewById(R.id.tv_num_people);
        ev_book_date = (EditText) findViewById(R.id.tv_book_date);
        //ev_time_from = (EditText) findViewById(R.id.tv_time_from);
        ev_time_to = (EditText) findViewById(R.id.tv_time_to);
        book_category = (Spinner) findViewById(R.id.book_catg);
        res_id = getIntent().getStringExtra("rest_id");
        Book_btn = (Button) findViewById(R.id.btn_book);
        btn_inviteFriends = (Button) findViewById(R.id.btn_InviteFriends);
        ev_booking_detail = (EditText) findViewById(R.id.evt_detail);
        insert_emails=findViewById(R.id.add_emails);
        get_emails=(EditText) findViewById(R.id.get_email);
        show_emails=findViewById(R.id.show_emails);
        email_member_layout=findViewById(R.id.email_member_layout);
        checkBox1=findViewById(R.id.checkBox1);
        checkBox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox1.isChecked())
                {
                    email_member_layout.setVisibility(View.VISIBLE);
                }
                else
                {
                    email_member_layout.setVisibility(View.GONE);
                }
            }
        });

        //for emails
        emails=new ArrayList<>();
        emails_ids=new ArrayList<>();
        emails_address=new ArrayList<>();

        btn_inviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                noOfPeopleForBooking = ev_num_people.getText().toString();

                if(noOfPeopleForBooking.equalsIgnoreCase("")) {
                    Toasty.error(RestaurantBooking.this, "Please Enter No Of People For Booking...").show();
                } else {
                    Intent i = new Intent(RestaurantBooking.this, inviteFriendsActivity.class);
                    i.putExtra("noOfPeopleForBooking", noOfPeopleForBooking);
                    startActivityForResult(i, 2);
                }

            }
        });

        //adding emails
        insert_emails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String  check=get_emails.getText().toString();
//                if(check.isEmpty()){
//                    get_emails.setFocusable(true);
//                    get_emails.setError("Insert Emails.");
//                }else {
//                    sending_emails(check);
//
//                }
            }
        });

        //Load Data in Spinner
        load_theme_for_spiner();

        //Set On Click Listener on Booking Button
        Book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isOnline(RestaurantBooking.this))
                {
                    // || ev_time_from.getText().toString().isEmpty()

                    if(ev_num_people.getText().toString().isEmpty() || ev_book_date.getText().toString().isEmpty()|| ev_time_to.getText().toString().isEmpty())
                    {
                        new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error!")
                                .setContentText("Fields are Required!")
                                .show();

                    }
                    else
                    {
                        //call the methode
                        book_restaurant();
                    }
                }
                else
                {
                    new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!")
                            .setContentText("Internet Not Found!")
                            .show();
                }
            }
        });

        // Set Date Picker Listner
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        //Set Click Listner When User Click on Booking Date
        ev_book_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog mdatePickerDialog = new DatePickerDialog(RestaurantBooking.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                mdatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mdatePickerDialog.show();
            }
        });

        // Set on Click Listner On Time Form
//        ev_time_from.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(RestaurantBooking.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        ev_time_from.setText( selectedHour + ":" + selectedMinute);
//                    }
//                }, hour, minute, false);//Yes 24 hour time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//            }
//        });

        // Set on Click Listner On Time To
        ev_time_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(RestaurantBooking.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        ev_time_to.setText( selectedHour + ":" + selectedMinute);
//                    }
//                }, hour, minute, false);//Yes 24 hour time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();

                Intent slotsIntent = new Intent(RestaurantBooking.this, booking_Slots_Activity.class);
                slotsIntent.putExtra("date", ev_book_date.getText().toString());
                slotsIntent.putExtra("restName", rest_Name);
                slotsIntent.putExtra("restId", res_id);
                startActivityForResult(slotsIntent, 1);
            }
        });
    }

    // Declare the function for set Date on Booking Edit Text
    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ev_book_date.setText(sdf.format(myCalendar.getTime()));
        getAvailableBookingSlots();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String strEditText = data.getStringExtra("selectedTimeSlot");
                ev_time_to.setText(strEditText);
            }
        }

        if (requestCode == 2) {
            invitedFriendsList = new ArrayList<>();
            if(data != null) {
                invitedFriendsList = data.getExtras().getStringArrayList("inviteesArray");
                Log.e("InviteesBooking", Arrays.deepToString(invitedFriendsList.toArray()));

                String friendsInvited = "Friends Invited: \n";

                for(int i = 0; i < invitedFriendsList.size(); i++) {
                    Log.e("List", "" + invitedFriendsList.get(i).split("-")[1]);
                    friendsInvited += invitedFriendsList.get(i).split("-")[1] + "\n";
                }
                show_emails.setText(friendsInvited);
            }
        }
    }

    // Get All Available Booking Slots for the selected date
    private void getAvailableBookingSlots() {
        Intent slotsIntent = new Intent(this, booking_Slots_Activity.class);
        slotsIntent.putExtra("date", ev_book_date.getText().toString());
        slotsIntent.putExtra("restName", rest_Name);
        slotsIntent.putExtra("restId", res_id);
        startActivityForResult(slotsIntent, 1);
    }

//    private void book_restaurant() {
//
//        pd = Utils.showProgress(RestaurantBooking.this);
//        pd.show();
//
//        final JSONArray jsonArray = new JSONArray();
//        for(int i = 0; i < invitedFriendsList.size(); i++) {
//            jsonArray.put(invitedFriendsList.get(i).split("-")[0]);
//        }
//        Log.e("jArray", "" + jsonArray);
//
//        final SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
//        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
//        Date date = null;
//        try {
//            date = parseFormat.parse(ev_time_to.getText().toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
//
//        final String id = Prefs.getString("user_id", "0");
//
//        final Date finalDate = date;
//        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                pd.dismiss();
//                Log.e("bookingResponse", response);
//                if(response.equals("null")) {
//                    Toasty.warning(RestaurantBooking.this, "Response is null", Toast.LENGTH_SHORT).show();
//                }else {
//                    try {
//                        JSONObject object  = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
//                        if(object.getBoolean("success")) {
//                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
//                            //finish();
//                            new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.SUCCESS_TYPE)
//                                    .setTitleText("Good Job!")
//                                    .setContentText(object.getString("message"))
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                            Intent forcl=new Intent(RestaurantBooking.this, RestaurantPage.class);
//                                            forcl.putExtra("rest_name",getIntent().getStringExtra("rest_name"));
//                                            forcl.putExtra("rest_id",getIntent().getStringExtra("rest_id"));
//                                            forcl.putExtra("rest_image",getIntent().getStringExtra("rest_image"));
//                                            forcl.putExtra("rest_self",getIntent().getStringExtra("rest_self"));
//                                            startActivity(forcl);
//                                            Animation.split(RestaurantBooking.this);
//                                            finish();
//                                        }
//                                    })
//                                    .show();
//
//                        }else {
//                            new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
//                                    .setTitleText("Oops...")
//                                    .setContentText(object.getString("message"))
//                                    .show();
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    Log.d("response",response);
//                }
//
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        pd.dismiss();
//                        new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
//                                .setTitleText("Oops...")
//                                .setContentText("Some thing went wrong!")
//                                .show();
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//
//                params.put("req_key","resturant_booking");
//                params.put("res_id",res_id);
//                params.put("usr_id", id);
//                params.put("parrent_id",id);
//                params.put("req_user_id", String.valueOf(jsonArray));
//                params.put("u_email", String.valueOf(emails_address));
//                params.put("id_count", String.valueOf(emails_ids.size()));
//                params.put("no_people",ev_num_people.getText().toString());
//                params.put("time_to", displayFormat.format(finalDate));
//
//                params.put("date_req",ev_book_date.getText().toString());
//                params.put("theme_cat",book_category.getSelectedItem().toString());
//                params.put("details",ev_booking_detail.getText().toString());
//
//                Log.e("bookingParams", params.toString());
//
//                return params;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(RestaurantBooking.this);
//        queue.add(request);
//    }


    // Declare the Restaurant Booking Function
    private void book_restaurant()
    {
        JSONArray jsonArray = new JSONArray();
        if(invitedFriendsList != null) {
            for(int i = 0; i < invitedFriendsList.size(); i++) {
                jsonArray.put(invitedFriendsList.get(i).split("-")[0]);
            }
            Log.e("jArray", "" + jsonArray);
        }

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(ev_time_to.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));

        final String id = Prefs.getString("user_id", "0");
        emails_ids.add(id);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key","resturant_booking");
        params.put("res_id",res_id);
        params.put("usr_id",emails_ids);
        params.put("parrent_id",id);
        params.put("req_user_id", jsonArray);
        params.put("u_email",emails_address);
        params.put("id_count",emails_ids.size());
        params.put("no_people",ev_num_people.getText().toString());
      //  params.put("time_from",ev_time_from.getText().toString());
//        params.put("time_to",ev_time_to.getText().toString());
        params.put("time_to", displayFormat.format(date));

        params.put("date_req",ev_book_date.getText().toString());
        params.put("theme_cat",book_category.getSelectedItem().toString());
        params.put("details",ev_booking_detail.getText().toString());

        Log.e("bookingParams", params.toString());

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onStart()
            {
                super.onStart();
                pd = Utils.showProgress(RestaurantBooking.this);
                pd.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                Log.e("bookingResponse", response);
                if(response.equals("null")) {
                    Toasty.warning(RestaurantBooking.this, "Response is null", Toast.LENGTH_SHORT).show();
                }else {
                    try {
                        JSONObject object  = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if(object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Your Booking request has been sent. Please wait while it is being reviewed.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            Intent forcl=new Intent(RestaurantBooking.this, RestaurantPage.class);
                                            forcl.putExtra("rest_name",getIntent().getStringExtra("rest_name"));
                                            forcl.putExtra("rest_id",getIntent().getStringExtra("rest_id"));
                                            forcl.putExtra("rest_image",getIntent().getStringExtra("rest_image"));
                                            forcl.putExtra("rest_self",getIntent().getStringExtra("rest_self"));
                                            startActivity(forcl);
                                            Animation.split(RestaurantBooking.this);
                                            finish();
                                        }
                                    })
                                    .show();

                        }else {
                            new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response",response);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String  response  = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(RestaurantBooking.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                }else {

                    Log.d("response",response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }


    //Declare the function for Spiner for data
    private void load_theme_for_spiner()
    {
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.ip_server , new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                if (response.contains("null"))
                {
                    Toasty.warning(RestaurantBooking.this, "We could not find Category.", Toast.LENGTH_LONG).show();
                }
                else
                {
                    theme_name = JSONParser.parse_theme(response);
                    book_category.setAdapter(new ArrayAdapter<String>(RestaurantBooking.this, android.R.layout.simple_spinner_dropdown_item, theme_name));
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error!")
                                .setContentText("Unable to Load Category!")
                                .show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "resturant_booking_theme");
                params.put("res_id", res_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent forcl=new Intent(RestaurantBooking.this, RestaurantPage.class);
        forcl.putExtra("rest_name",getIntent().getStringExtra("rest_name"));
        forcl.putExtra("rest_id",getIntent().getStringExtra("rest_id"));
        forcl.putExtra("rest_image",getIntent().getStringExtra("rest_image"));
        forcl.putExtra("rest_self",getIntent().getStringExtra("rest_self"));
        startActivity(forcl);
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
    }

    //sending emails
    private void sending_emails(String email_add)
    {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key","check_user_email_for_booking");
        params.put("u_email",email_add);
        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onStart()
            {
                super.onStart();
                pd = Utils.showProgress(RestaurantBooking.this);
                pd.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(RestaurantBooking.this, "Response is null", Toast.LENGTH_SHORT).show();
                }else {

                    try {
                        JSONObject object  = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if(object.getBoolean("success")) {
                            emails_ids.add(object.getString("user_id"));
                            emails_address.add(get_emails.getText().toString());
                            //Toast.makeText(RestaurantBooking.this,object.getString("user_id")+" "+get_emails.getText().toString(),Toast.LENGTH_LONG).show();
                            emails.add(get_emails.getText().toString()+",");
                            StringBuilder builder = new StringBuilder();
                            for (String details : emails) {
                                builder.append(details);
                            }
                            show_emails.setText(builder.toString());
                            get_emails.setText("");

                        }else {
                            new SweetAlertDialog(RestaurantBooking.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response",response);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String  response  = Utils.getResponse(responseBody);
                if(response.equals("null")) {
                    Toasty.warning(RestaurantBooking.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                }else {

                    Log.d("response",response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }
}
