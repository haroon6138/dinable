package com.dinable.dinable.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.dinable.dinable.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.dinable.dinable.AppUtils.Utils.isLocationEnabled;

public class Splash extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private static int SPLASH_TIME_OUT = 3000;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    ImageView iv_banner;
    double lati = 0.0d, longi = 0.0d;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private GoogleApiClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        iv_banner = (ImageView) findViewById(R.id.splash_banner);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                checkIfLocationEnabled();
//                }
            }
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();

        Picasso.get().load(R.drawable.splash)
                .resize(1080, 1920)
                .onlyScaleDown().
                into(iv_banner);
       // Picasso.get().load(R.drawable.r1).into(iv_banner);
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Splash.this) == ConnectionResult.SUCCESS) {

            googleApiClient = new GoogleApiClient.Builder(Splash.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(Splash.this)
                    .addOnConnectionFailedListener(Splash.this)
                    .build();

            if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        } else {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkIfLocationEnabled();
    }

    private void checkIfLocationEnabled() {
        if(!isLocationEnabled(getApplicationContext())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Your Location Isn't Turned On. Please Turn On Your Location To Continue.");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    String GpsProvider = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

                    if(GpsProvider.equals("")){
                        //GPS Disabled
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }else{
                        if (ActivityCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(Splash.this, new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations this can be null.
                                        if (location != null) {

                                            Log.e("Location Changed", location.toString());

                                            lati = location.getLatitude();
                                            longi = location.getLongitude();

                                            Intent i = new Intent(Splash.this, MainActivity.class);
                                            Prefs.putDouble("lat", lati);
                                            Prefs.putDouble("long", longi);
                                            startActivity(i);
                                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                            finish();
                                        }
                                    }
                                });
                        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Splash.this) == ConnectionResult.SUCCESS) {

                            googleApiClient = new GoogleApiClient.Builder(Splash.this)
                                    .addApi(LocationServices.API)
                                    .addConnectionCallbacks(Splash.this)
                                    .addOnConnectionFailedListener(Splash.this)
                                    .build();

                            if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {
                                googleApiClient.connect();
                            }
                        } else {
                        }

//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                // Start your application main_activity
//                                Intent i = new Intent(Splash.this, MainActivity.class);
//                                startActivity(i);
//                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                                finish();
//
//                            }
//                        }, SPLASH_TIME_OUT);
                    }
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1 * 2000); // milliseconds
        locationRequest.setFastestInterval(1 * 1000); // the fastest rate in milliseconds at which your app can handle location updates
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("Loc", location.toString());
        lati = location.getLatitude();
        longi = location.getLongitude();
        Prefs.putDouble("lat", lati);
        Prefs.putDouble("long", longi);
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        Intent i = new Intent(Splash.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }
}
