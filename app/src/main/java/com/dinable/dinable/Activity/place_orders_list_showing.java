package com.dinable.dinable.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.Booking_Reciept_adaptor;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.dinable.dinable.classes.Database_Helper;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.order_menu_class;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*
 * watch restaurant page and home page
 * self check
 * get image restaurant
 */

public class place_orders_list_showing extends AppCompatActivity implements RatingDialogListener {

    private List<order_menu_class> order_menu_classList;
    String o_id, check_self, rest_id, rest_image, o_status, invoice, rest_PlayerId, orderType;
    private TextView rest_name, datetime, totalprice;
    private View recipt;
    private Button btnCheckout;

    private SweetAlertDialog pd;
    private Database_Helper database_helper;
    String yes, show_waiter = "no", checked_dialog = "no", show_checkout = "no", for_rating_show_self = "no";

    private Booking_Reciept_adaptor booking_reciept_adaptor;
    private RecyclerView recyclerView;

    JSONObject jsonObject;
    JSONArray jsonArray;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_orders_list_showing);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Order");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        o_id = getIntent().getStringExtra("o_id");
        Log.e("OrderID", o_id);
        check_self = getIntent().getStringExtra("selfcheck");
        rest_id = getIntent().getStringExtra("rest_id");
        rest_image = getIntent().getStringExtra("rest_image");
        o_status = getIntent().getStringExtra("o_status");
        invoice = getIntent().getStringExtra("invoice_status");
        rest_PlayerId = getIntent().getStringExtra("rest_PlayerId");
        orderType = getIntent().getStringExtra("orderType");

        database_helper = new Database_Helper(this);

        rest_name = findViewById(R.id.restaurant_name);
        datetime = findViewById(R.id.datetime);
        totalprice = findViewById(R.id.totalprice);
        recipt = findViewById(R.id.receipts);
        btnCheckout = findViewById(R.id.checkoutBtn);

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestDatatocheckout(Endpoints.ip_server, o_id);
            }
        });

        recyclerView = findViewById(R.id.recyclerview);

        if (check_self.equals("1")) {
            if (invoice.equals("0")) {
                show_waiter = "yes";
                if (o_status.equals("Approve / Unpaid")) {
                    show_checkout = "yes";
                }
            }
        } else if (check_self.equals("0")) {
            if (o_status.equals("Successfully Paid")) {
                for_rating_show_self = "yes";
            }
        }


        if (Utils.isOnline(place_orders_list_showing.this)) {
            try {
                //Toast.makeText(this,o_id,Toast.LENGTH_SHORT).show();
                requestData(Endpoints.ip_server, o_id);
            } catch (Exception ex) {
                new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Something went wrong!")
                        .show();
            }

        } else {
            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
    }

    //sending
    public void requestData(String uri, final String o_id) {
        final String id = Prefs.getString("user_id", "0");
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response);
                if (response.contains("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Orders not available.", Toast.LENGTH_LONG).show();
                } else {
                    order_menu_classList = JSONParser.parse_Orders_menu(response);
                    rest_name.setText(getIntent().getStringExtra("rest_name") + "");
                    datetime.setText(getIntent().getStringExtra("datetime") + "");
                    totalprice.setText("RS." + getIntent().getStringExtra("overprice") + "/. ");
//                    for(int i=0;i<order_menu_classList.size();i++){
//                        TextView valueTV = new TextView(place_orders_list_showing.this);
//                        valueTV.setText(order_menu_classList.get(i).getMn_nm()+"      Qty."+(Integer.parseInt(order_menu_classList.get(i).getTotal_price())/(Integer.parseInt(order_menu_classList.get(i).getMenu_qty())))+"*"+order_menu_classList.get(i).getMenu_qty()+"  "+order_menu_classList.get(i).getTotal_price());
//                        valueTV.setLayoutParams(new LinearLayout.LayoutParams(
//                                LinearLayout.LayoutParams.FILL_PARENT,
//                                LinearLayout.LayoutParams.WRAP_CONTENT));
//                        ((LinearLayout) recipt).addView(valueTV);
//                    }
                    booking_reciept_adaptor = new Booking_Reciept_adaptor(place_orders_list_showing.this, order_menu_classList);
                    recyclerView.setAdapter(booking_reciept_adaptor);


                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Something went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "user_order_Menu");
                params.put("o_id", o_id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void sendOneSignalPush() {
        String app_Id = "2dd5277e-ad36-4fd9-8431-732a94c477b2";
        final String authToken = "Basic NThhZmFiNGMtYjVhMC00YjJmLWE2N2MtM2MxMzkyMzU2NGNi";

        JSONObject objData = null;
        JSONObject contentObj = null;

        try {
            objData = new JSONObject();
            contentObj = new JSONObject();
            jsonArray = new JSONArray();

            contentObj.put("en", Prefs.getString("user_name", "") + " Wants to checkout, Please settle the bill & mark the order as 'Successfully Paid'.");
            jsonArray.put(rest_PlayerId);

            objData.put("app_id", app_Id);
            objData.put("contents", contentObj);
            objData.put("include_player_ids", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, Endpoints.OneSignal_PUSH_URL, objData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("!_@@_SUCESS", response + "");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("!_@@_Errors--", error + "");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", authToken);
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 1000 * 60;// 60 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);
    }


    //checkout
    public void requestDatatocheckout(String uri, final String o_id) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "order_to_checkout");
        params.put("o_id", o_id);
        params.put("u_id", id);
        params.put("r_id", rest_id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(place_orders_list_showing.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {

                        JSONArray jsonArray = null;
                        JSONObject jsonObject = null;

                        try {
                            jsonArray = new JSONArray(Prefs.getString("CheckInData", "[]"));

                            for(int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                if(rest_id.equalsIgnoreCase(jsonObject.getString("restaurantID")))
                                    jsonArray.remove(i);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        Prefs.putString("tableNo", "");
                        Prefs.putBoolean("hasInputTableNo", false);
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();

                            // Send Notification To Restaurant that user wants to checkout
                            sendOneSignalPush();

                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Your checkout has been processed successfully.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            if (database_helper.getcheckingCount() > 0) {
                                                try {
                                                    database_helper.deleteallchecking();
                                                    database_helper.deleteall();
                                                } catch (Exception c) {

                                                }
                                            }
                                            Prefs.putString("order_num", "null");
                                            Prefs.putString("order_rest_id", "null");
                                            showDialog();
                                            yes = "yes";
                                        }
                                    })
                                    .show();

                        } else {
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure!")
                                    .setContentText("Error while processing your checkout. Please try again.")
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }

    public void callWaiter(String rest_id) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams params = new RequestParams();
        params.put("req_key", "need_waiter");
        params.put("restaurant_id", rest_id);
        params.put("user_id", id);
        params.put("order_id", o_id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(place_orders_list_showing.this);
                pd.show();
                Log.e("need waiter params", params.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success")
                                    .setContentText("Your request has been queued. A Waiter will arrive shortly.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();

                        } else {
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure!")
                                    .setContentText("Error processing your request. Please try again.")
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }


    //for waiter calling
    public void requestwaiter(String uri, final String o_id) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "need_a_waiter");
        params.put("order_id", o_id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(place_orders_list_showing.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toasty.success(RestaurantBooking.this,object.getString("message"),Toast.LENGTH_LONG).show();
                            //finish();
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good Job!")
                                    .setContentText(object.getString("message"))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();

                        } else {
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText(object.getString("message"))
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }

    //for rating
    private void showDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(2)
                .setTitle("Rate this Restaurant")
                .setDescription("How was your experience with us?")
                .setCommentInputEnabled(true)
                .setStarColor(R.color.yellow)
                .setNoteDescriptionTextColor(R.color.black2)
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.colorPrimary)
                .setCommentTextColor(R.color.black2)
                .setCommentBackgroundColor(R.color.white)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(this) // only if listener is implemented by fragment
                .show();
    }

    @Override
    public void onPositiveButtonClicked(int rate, String comment) {
        requestRating(Endpoints.ip_server, rest_id, rate, comment, yes);

        //Toast.makeText(this, ""+for_add.get(0).getRest_id()+"  "+  rate+"  "+ comment, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNegativeButtonClicked() {
        finish();

    }

    @Override
    public void onNeutralButtonClicked() {
        // finish();

    }

    //rating request
    public void requestRating(String uri, final String rest_id, final int rate, final String comment, final String yes) {
        final String id = Prefs.getString("user_id", "0");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "user_review_for_res");
        params.put("res_id", rest_id);
        params.put("rating", rate);
        params.put("review", comment);
        params.put("u_id", id);

        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        final JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!")
                                    .setContentText("Thank you for your kind feedback")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            if (yes.equals("yes")) {
                                                finish();
                                            }
                                        }
                                    })
                                    .show();
                        } else {
                            new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Failure!")
                                    .setContentText("Error processing your request. Please try again.")
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                if (response.equals("null")) {
                    Toasty.warning(place_orders_list_showing.this, "Unable to Connect Server", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                try {
                    pd.dismiss();
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        Animation.swipeLeft(this);
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.for_waiter_call, menu);
        MenuItem item = menu.findItem(R.id.waiter_call);
        MenuItem item1 = menu.findItem(R.id.add_more_menu);
        MenuItem item2 = menu.findItem(R.id.proceed);

        if (orderType.equalsIgnoreCase("Delivery"))
            item.setVisible(false);

        //hiding checkout button in toolbar
        item2.setVisible(false);

        MenuItem item3 = menu.findItem(R.id.rating);
        if (check_self.equals("1")) {
            if (show_waiter.equals("yes")) {
                item.setVisible(true);
                item1.setVisible(true);
                item3.setVisible(false);
                if (show_checkout.equals("yes")) {
                    if (orderType.equalsIgnoreCase("Delivery"))
                        item2.setVisible(false);
                    else
                        item2.setVisible(true);
                } else {
                    item2.setVisible(false);
                    btnCheckout.setVisibility(View.GONE);
                }
            } else {
                item.setVisible(false);
                item1.setVisible(false);
                item2.setVisible(false);
                btnCheckout.setVisibility(View.GONE);
                item3.setVisible(false);
            }
        } else if (for_rating_show_self.equals("yes")) {
            item.setVisible(false);
            item1.setVisible(false);
            item2.setVisible(false);
            btnCheckout.setVisibility(View.GONE);
            item3.setVisible(true);
        } else {
            item.setVisible(false);
            item1.setVisible(false);
            item2.setVisible(false);
            btnCheckout.setVisibility(View.GONE);
            item3.setVisible(false);
        }
        //hiding checkout button in toolbar
        item2.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.waiter_call) {
//            requestwaiter("",o_id);
            callWaiter(rest_id);
        } else if (id == R.id.add_more_menu) {
            if (Prefs.getString("order_num", "").equals(o_id)) {
                Intent forcl = new Intent(place_orders_list_showing.this, RestaurantPage.class);
                forcl.putExtra("rest_name", getIntent().getStringExtra("rest_name"));
                forcl.putExtra("rest_id", rest_id);
                forcl.putExtra("rest_image", rest_image);
                forcl.putExtra("rest_self", check_self);
                startActivity(forcl);
                Animation.split(place_orders_list_showing.this);
                finish();
            } else {
                Toasty.error(place_orders_list_showing.this, "Something Wrong", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.proceed) {
            if (Utils.isOnline(place_orders_list_showing.this)) {
                try {
                    requestDatatocheckout(Endpoints.ip_server, o_id);
                } catch (Exception ex) {
                    new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Something went wrong!")
                            .show();
                }

            } else {
                new SweetAlertDialog(place_orders_list_showing.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Internet Not Found!")
                        .show();
            }
        } else if (id == R.id.rating) {
            showDialog();
            yes = "yes";
        }
        return super.onOptionsItemSelected(item);
    }
}


