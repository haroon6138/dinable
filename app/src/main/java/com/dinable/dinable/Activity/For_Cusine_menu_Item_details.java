package com.dinable.dinable.Activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinable.dinable.Fragments.Cusine_detail_Fragment;
import com.dinable.dinable.Fragments.Cusine_reviews_Fragment;
import com.dinable.dinable.Fragments.Deals_fragment;
import com.dinable.dinable.Fragments.Events_fragment;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;

import java.util.ArrayList;
import java.util.List;

import cn.hugeterry.coordinatortablayout.CoordinatorTabLayout;
import cn.hugeterry.coordinatortablayout.listener.LoadHeaderImagesListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class For_Cusine_menu_Item_details extends AppCompatActivity {
    private CoordinatorTabLayout mCoordinatorTabLayout;
    String cusine_image, cusine_name, cusine_price, cusine_rating, cusine_desc, cusine_id, cusine_rest_id, show_cart, rest_name;
    private TabLayout tabLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for__cusine_menu__item_details);
        cusine_name = getIntent().getStringExtra("cusine_name");
        cusine_image = getIntent().getStringExtra("cusine_image");
        cusine_price = getIntent().getStringExtra("cusine_price");
        cusine_rating = getIntent().getStringExtra("cusine_rating");
        cusine_desc = getIntent().getStringExtra("cusine_desc");
        cusine_id = getIntent().getStringExtra("cusine_idxyz");
        cusine_rest_id = getIntent().getStringExtra("cusine_rest_id");
        show_cart = getIntent().getStringExtra("show_cart");
        rest_name = getIntent().getStringExtra("rest_name");

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        mCoordinatorTabLayout = (CoordinatorTabLayout) findViewById(R.id.coordinatortablayout);
        mCoordinatorTabLayout.setTitle(cusine_name)
                .setBackEnable(true)
                .setLoadHeaderImagesListener(new LoadHeaderImagesListener() {
                    @Override
                    public void loadHeaderImages(ImageView imageView, TabLayout.Tab tab) {
                        Glide.with(For_Cusine_menu_Item_details.this)
                                .load(cusine_image)
                                .into(imageView);
                    }
                }).setupWithViewPager(viewPager);

        tabLayout = mCoordinatorTabLayout.getTabLayout();
        tabLayout.setTabTextColors(ContextCompat.getColor(this, android.R.color.white),
                ContextCompat.getColor(this, android.R.color.white));
        tabLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.black));

    }

    private void setupViewPager(ViewPager viewPager) {
        AdapterforCusine_details adapter = new AdapterforCusine_details(getSupportFragmentManager());
        adapter.addFragment(new Cusine_detail_Fragment(), "Cusine", cusine_rest_id, cusine_name, cusine_price, cusine_id, cusine_rating, cusine_image, cusine_desc, show_cart);
        adapter.addFragment(new Cusine_reviews_Fragment(), "Reviews", cusine_rest_id, cusine_name, cusine_price, cusine_id, cusine_rating, cusine_image, cusine_desc, show_cart);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animation.windmill(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.windmill(this);
        return super.onSupportNavigateUp();
    }

    static class AdapterforCusine_details extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public AdapterforCusine_details(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title, String rest_id, String cusine_name, String cusine_price, String cusine_id, String cusine_rating, String cusine_image, String cusine_desc, String show_cart) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
            Bundle data = new Bundle();//create bundle instance
            data.putString("rest_id", rest_id);//put string to pass with a key value
            data.putString("cusine_name", cusine_name);//put string to pass with a key value
            data.putString("cusine_price", cusine_price);//put string to pass with a key value
            data.putString("cusine_id", cusine_id);//put string to pass with a key value
            data.putString("cusine_rating", cusine_rating);//put string to pass with a key value
            data.putString("cusine_image", cusine_image);//put string to pass with a key value
            data.putString("cusine_desc", cusine_desc);//put string to pass with a key value
            data.putString("show_cart", show_cart);//put string to pass with a key value
            fragment.setArguments(data);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }
}
