package com.dinable.dinable.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.FCM.SharedPrefManager;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.Animation;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    EditText email, password;
    private SweetAlertDialog pd;
    ImageView iv_main_logo;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setBackgroundDrawableResource(R.drawable.login);
        disableSSLCertificateChecking();

//        GPSTracker gps = new GPSTracker(this);
//        if(gps.canGetLocation()){
//            lati = gps.getLatitude(); // returns latitude
//            longi = gps.getLongitude(); // returns longtiude
//            Log.e("Location", gps.getLocation().toString());
//        }

        iv_main_logo = (ImageView) findViewById(R.id.main_logo);
        //To check if user logged in
        Boolean isLoginSucces = Prefs.getBoolean("loginSuccess", false);
        if (isLoginSucces) {
            //start activity..
            Intent intent = new Intent(MainActivity.this, Home.class);
            startActivity(intent);
        }

        // Click Listener for Sign Up Activity
        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Signup.class));
                Animation.split(MainActivity.this);
            }
        });

        //Take Email and Pass
        email = findViewById(R.id.emailtxt);
        password = findViewById(R.id.passtxt);

        // Click Listner for Login Button
        findViewById(R.id.loginbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isOnline(MainActivity.this)) {
                    if (email.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Fields are Required!")
                                .show();

                    } else {
                        //call the method
                        login_methode(email.getText().toString(), password.getText().toString());
                    }
                } else {
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Internet Not Found!")
                            .show();
                }
            }
        });

    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }
        } };

        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() { @Override public boolean verify(String hostname, SSLSession session) { return true; } });
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    // Declare the login Method
    private void login_methode(final String email, final String pass) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("req_key", "user_login");
        params.put("u_email", email);
        params.put("u_pass", pass);
        params.put("token",
                SharedPrefManager.getInstance(MainActivity.this).getDeviceToken().toString());
        client.post(Endpoints.ip_server, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                pd = Utils.showProgress(MainActivity.this);
                pd.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = Utils.getResponse(responseBody);
                Log.e("Login", response);
                if (response.equals("null")) {
                    Toasty.warning(MainActivity.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        JSONObject object = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                        if (object.getBoolean("success")) {
                            //Toast.makeText(MainActivity.this,"Login Successful",Toast.LENGTH_LONG).show();
                            Toasty.success(MainActivity.this, "Login Successful", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(MainActivity.this, Home.class);
                            Prefs.putString("user_name", object.getString("message"));
                            Prefs.putString("user_id", object.getString("id"));
                            Prefs.putString("user_phone", object.getString("phone"));
                            Prefs.putString("user_image", object.getString("u_image"));
                            Prefs.putString("user_email", email);
                            Prefs.putString("user_password", pass);
                            Prefs.putBoolean("loginSuccess", true); // change this value on logout
                            startActivity(intent);
                            Animation.fade(MainActivity.this);
                            //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        } else {
                            Toasty.error(MainActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("response", response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String response = Utils.getResponse(responseBody);
                Log.e("Login", response + " - " + statusCode);
                if (response.equals("null")) {
                    Toasty.error(MainActivity.this, "Unable to connect server", Toast.LENGTH_SHORT).show();
                } else {

                    Log.d("response", response);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pd.dismiss();
            }
        });
    }
}
