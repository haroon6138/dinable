package com.dinable.dinable.Fragments;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dinable.dinable.R;

public class Cusine_detail_Fragment extends Fragment {

    private TextView desr,price;


    String cusine_image,cusine_name,cusine_price,cusine_rating,cusine_desc,cusine_id,cusine_rest_id,show_cart,rest_name;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.activity_cusine_detail__fragment, container, false);
        //getting data from arguments
        cusine_rest_id= getArguments().getString("rest_id");
        cusine_name= getArguments().getString("cusine_name");
        cusine_price=getArguments().getString("cusine_price");
        cusine_id= getArguments().getString("cusine_id");
        cusine_rating= getArguments().getString("cusine_rating");
        cusine_image=  getArguments().getString("cusine_image");
        cusine_desc=  getArguments().getString("cusine_desc");
        show_cart=  getArguments().getString("show_cart");
        rest_name=  getArguments().getString("rest_name");




        //intializing
        desr=(TextView) rv.findViewById(R.id.cusine_des);
        price=(TextView) rv.findViewById(R.id.cusine_pri);

        //setting data
        if(cusine_price !=null){
            price.setText(cusine_price+"/.");
        }
        if(cusine_desc !=null){
            desr.setText(cusine_desc);
        }
        return rv;
    }

}
