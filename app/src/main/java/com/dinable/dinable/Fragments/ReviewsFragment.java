package com.dinable.dinable.Fragments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.ReviewRestuarntAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.ReviewRestuarantClass;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class ReviewsFragment extends Fragment {

    List<ReviewRestuarantClass> reviewRestuarantClassList;
    RecyclerView recyclerView;
    private ReviewRestuarntAdapter adapter;
    String getArgument;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rv =  inflater.inflate(
                R.layout.activity_reviews_restuarant_fragment, container, false);
        getArgument = getArguments().getString("rest_id");
        recyclerView=(RecyclerView) rv.findViewById(R.id.recylcerView);
        if(Utils.isOnline(getContext()))
        {
            requestData(Endpoints.ip_server,getArgument);
        }
        else
        {
            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
        return rv;
    }

    private void requestData(String uri,final String id)
    {
        StringRequest request = new StringRequest(Request.Method.POST,uri, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                if (response.contains("null"))
                {
                    Toasty.warning(getContext(), "No Review Available.", Toast.LENGTH_LONG).show();
                }
                else
                {
                    reviewRestuarantClassList = JSONParser.parse_res_review(response);
                    adapter = new ReviewRestuarntAdapter(getContext(), reviewRestuarantClassList);
                    recyclerView.setAdapter(adapter);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "review_list_for_restuarant");
                params.put("res_id", id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(request);
    }
}
