package com.dinable.dinable.Fragments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Activity.EventRestActivity;
import com.dinable.dinable.Adapter.EventRestAdapter;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.EventRestClass;
import com.dinable.dinable.classes.JSONParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class Events_fragment extends Fragment {

    private List<EventRestClass> eventRestClassList;
    RecyclerView recyclerView;
    private EventRestAdapter adapter;
    String getArgument;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rv =  inflater.inflate(
                R.layout.activity_events_fragment, container, false);
        recyclerView=(RecyclerView) rv.findViewById(R.id.recylcerView);
        getArgument = getArguments().getString("rest_id");
        if(Utils.isOnline(getContext()))
        {
            requestData(Endpoints.ip_server,getArgument);
        }
        else
        {
            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }
        return rv;
    }

    private void requestData(String uri,final String id)
    {
        StringRequest request = new StringRequest(Request.Method.POST,uri, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                if (response.contains("null"))
                {
                    Toasty.warning(getContext(), "No Event Available.", Toast.LENGTH_LONG).show();
                }
                else
                {
                    eventRestClassList = JSONParser.parse_rest_event(response);
                    adapter = new EventRestAdapter(getContext(), eventRestClassList);
                    recyclerView.setAdapter(adapter);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "resturant_events_by_id");
                params.put("res_id", id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(request);
    }
}
