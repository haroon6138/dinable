package com.dinable.dinable.Fragments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dinable.dinable.Adapter.for_menu_reviews_adaptor;
import com.dinable.dinable.AppUtils.Endpoints;
import com.dinable.dinable.AppUtils.Utils;
import com.dinable.dinable.R;
import com.dinable.dinable.classes.JSONParser;
import com.dinable.dinable.classes.Menu_reviews_class;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class Cusine_reviews_Fragment extends Fragment {
    String cusine_image, cusine_name, cusine_price, cusine_rating, cusine_desc, cusine_id, cusine_rest_id, show_cart, rest_name;
    RecyclerView recyclerView;
    List<Menu_reviews_class> menuReviewsClassList;
    for_menu_reviews_adaptor for_menu_reviews_adaptor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.activity_cusine_reviews__fragment, container, false);
        //getting data from arguments
        cusine_rest_id = getArguments().getString("rest_id");
        cusine_name = getArguments().getString("cusine_name");
        cusine_price = getArguments().getString("cusine_price");
        cusine_id = getArguments().getString("cusine_id");
        cusine_rating = getArguments().getString("cusine_rating");
        cusine_image = getArguments().getString("cusine_image");
        cusine_desc = getArguments().getString("cusine_desc");
        show_cart = getArguments().getString("show_cart");
        rest_name = getArguments().getString("rest_name");


        recyclerView = (RecyclerView) rv.findViewById(R.id.recylcerView);
        if (Utils.isOnline(getActivity())) {
            requestData(Endpoints.ip_server, cusine_id);
            //Toast.makeText(getActivity(),rest_name+"",Toast.LENGTH_SHORT).show();
        } else {
            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Internet Not Found!")
                    .show();
        }

        return rv;
    }

    private void requestData(String uri, final String id) {
        StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonArray.length() == 0) {
                    Toasty.warning(getContext(), "No Review Available.", Toast.LENGTH_LONG).show();
                } else {
                    menuReviewsClassList = JSONParser.parse_menu_review(response);
                    for_menu_reviews_adaptor = new for_menu_reviews_adaptor(getContext(), menuReviewsClassList);
                    recyclerView.setAdapter(for_menu_reviews_adaptor);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Some thing went wrong!")
                                .show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("req_key", "list_menu_ratting_review");
                params.put("mn_id", id);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(request);
    }
}
